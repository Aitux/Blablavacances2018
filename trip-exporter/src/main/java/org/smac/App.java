package org.smac;

import com.opencsv.CSVReader;

import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main(String[] args) throws InvalidAlgorithmParameterException
    {
        if (args.length != 3)
            throw new InvalidAlgorithmParameterException("\nUsage: java -jar trip-exporter-X.Y.jar-with-dependencies.jar <input.csv> <login> <password>");

        File f = new File(args[0]);
        String prefix = "trip_";
        List<Integer> idTourList = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();

        try
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
            StringBuilder builder = new StringBuilder();
            String s;
            while ((s = in.readLine()) != null)
                builder.append(s).append('\n');

            CSVReader reader = new CSVReader(new StringReader(builder.toString()), ';');
            List<String[]> array = reader.readAll();
            for (int i = 0; i < array.get(0).length; i++)
            {
                String s1 = array.get(0)[i];
                if (s1.startsWith(prefix))
                {
                    s1 = s1.substring(prefix.length()).toLowerCase();
                    integerList.add(i);
                    stringList.add(s1);
                }

            }
            array.remove(0);

            Connection c = null;

            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/senioreva", args[1], args[2]);
            Statement stmt = c.createStatement();

            PreparedStatement insertTourId = c.prepareStatement("INSERT INTO Tour values (?) RETURNING idTour");
            PreparedStatement insertLikesTourFeature = c.prepareStatement("INSERT INTO describesTour(featureTName, tourfeatureValue,idTour) VALUES (?, ?, ?)");

            int idMax;

            ResultSet rs = stmt.executeQuery("SELECT max(idTour) FROM tour");
            if (rs.next())
                idMax = rs.getInt(1);
            else
                idMax = 0;
            for (int i = 0; i < array.size(); i++)
            {
                insertTourId.setInt(1, ++idMax);
                insertTourId.execute();
                ResultSet id = insertTourId.getResultSet();
                if (id.next())
                    idTourList.add(id.getInt(1));
            }

            int j = 0;
            for (String[] strings
                    : array)
            {
                int k = 0;
                for (int i :
                        integerList)
                {
                    insertLikesTourFeature.setString(1, stringList.get(k++));
                    insertLikesTourFeature.setFloat(2, Float.parseFloat(strings[i]));
                    insertLikesTourFeature.setInt(3, idTourList.get(j));
                    insertLikesTourFeature.executeUpdate();
                    System.out.println("INSERT 0 1");
                }
                j++;
            }


        } catch (IOException | SQLException e)
        {
            System.err.println("[ERROR] Fatal");
            e.printStackTrace();
            System.exit(-1);
        }


    }
}
