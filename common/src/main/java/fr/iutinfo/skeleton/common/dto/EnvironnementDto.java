package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class EnvironnementDto
{
    final static Logger logger = LoggerFactory.getLogger(EnvironnementDto.class);
    private int id_environnement = 0;
    private String libelle;

    public int getId_environnement()
    {
        return id_environnement;
    }

    public void setId_environnement(int id_environnement)
    {
        this.id_environnement = id_environnement;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
