package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class DestinationDto
{
    final static Logger logger = LoggerFactory.getLogger(DestinationDto.class);
    private int id_destination = 0;
    private String libelle;

    public int getId_destination()
    {
        return id_destination;
    }

    public void setId_destination(int id_destination)
    {
        this.id_destination = id_destination;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
