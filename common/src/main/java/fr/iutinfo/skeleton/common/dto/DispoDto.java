package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class DispoDto
{
    final static Logger logger = LoggerFactory.getLogger(DispoDto.class);
    private int id_dispo = 0;
    private String libelle;

    public int getId_dispo()
    {
        return id_dispo;
    }

    public void setId_dispo(int id_dispo)
    {
        this.id_dispo = id_dispo;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }    
}
