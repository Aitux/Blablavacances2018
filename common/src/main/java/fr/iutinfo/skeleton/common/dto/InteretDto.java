package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class InteretDto
{
    final static Logger logger = LoggerFactory.getLogger(InteretDto.class);
    private int id_interet = 0;
    private String libelle;

    public int getId_interet()
    {
        return id_interet;
    }

    public void setId_interet(int id_interet)
    {
        this.id_interet = id_interet;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
