package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class ActivitesDto
{
    final static Logger logger = LoggerFactory.getLogger(ActivitesDto.class);
    private int id_activites = 0;
    private String libelle;

    public int getId_activites()
    {
        return id_activites;
    }

    public void setId_activites(int id_activites)
    {
        this.id_activites = id_activites;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
