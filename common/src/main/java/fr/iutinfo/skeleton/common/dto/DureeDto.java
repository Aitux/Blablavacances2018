package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class DureeDto
{
    final static Logger logger = LoggerFactory.getLogger(DureeDto.class);
    private int id_duree = 0;
    private String libelle;

    public int getId_duree()
    {
        return id_duree;
    }

    public void setId_duree(int id_duree)
    {
        this.id_duree = id_duree;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
