package fr.iutinfo.skeleton.common.dto;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VoyageDto
{
    final static Logger logger = LoggerFactory.getLogger(VoyageDto.class);

    private int id_voyage = 0;
    private int budget;
    private int route_seul;
    private int id_createur;
    private String nb_co_vac;
    private int sexe_co_vac;
    private String age_co_vac;
    private String etude_co_vac;
    private int fumeur_co_vac;
    private int animal_de_compagnie_co_vac;
    private String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getNb_co_vac()
    {
        return nb_co_vac;
    }

    public void setNb_co_vac(String nb_co_vac)
    {
        this.nb_co_vac = nb_co_vac;
    }

    public int getSexe_co_vac()
    {
        return sexe_co_vac;
    }

    public void setSexe_co_vac(int sexe_co_vac)
    {
        this.sexe_co_vac = sexe_co_vac;
    }

    public String getAge_co_vac()
    {
        return age_co_vac;
    }

    public void setAge_co_vac(String age_co_vac)
    {
        this.age_co_vac = age_co_vac;
    }

    public String getEtude_co_vac()
    {
        return etude_co_vac;
    }

    public void setEtude_co_vac(String etude_co_vac)
    {
        this.etude_co_vac = etude_co_vac;
    }

    public int getFumeur_co_vac()
    {
        return fumeur_co_vac;
    }

    public void setFumeur_co_vac(int fumeur_co_vac)
    {
        this.fumeur_co_vac = fumeur_co_vac;
    }

    public int getAnimal_de_compagnie_co_vac()
    {
        return animal_de_compagnie_co_vac;
    }

    public void setAnimal_de_compagnie_co_vac(int animal_de_compagnie_co_vac)
    {
        this.animal_de_compagnie_co_vac = animal_de_compagnie_co_vac;
    }

    public int getId_createur()
    {
        return id_createur;
    }

    public void setId_createur(int id_createur)
    {
        this.id_createur = id_createur;
    }

    public int getId_voyage()
    {
        return id_voyage;
    }

    public void setId_voyage(int id_voyage)
    {
        this.id_voyage = id_voyage;
    }

    public int getRoute_seul()
    {
        return route_seul;
    }

    public void setRoute_seul(int route_seul)
    {
        this.route_seul = route_seul;
    }

    public int getBudget()
    {
        return budget;
    }

    public void setBudget(int budget)
    {
        this.budget = budget;
    }


}
