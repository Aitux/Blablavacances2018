package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;

public class UserDto {
    final static Logger logger = LoggerFactory.getLogger(UserDto.class);
    private String name;
    private String alias;
    private int id = 0;
    private String email;
    private String password;
	private String adresse;
    private String tel;
	private String prenom;
	private String anneenaissance;
	private String sexe;
	private int niveauEtude;
	private String profession;
	private int fumeur;

    public int getFumeur()
    {
        return fumeur;
    }

    public void setFumeur(int fumeur)
    {
        this.fumeur = fumeur;
    }

    public String getProfession()
    {
        return profession;
    }

    public void setProfession(String profession)
    {
        this.profession = profession;
    }


    public int getNiveauEtude()
    {
        return niveauEtude;
    }

    public void setNiveauEtude(int niveauEtude)
    {
        this.niveauEtude = niveauEtude;
    }

    public String getAnneenaissance() {
		return anneenaissance;
	}

	public void setAnneenaissance(String anneenaissance) {
		this.anneenaissance = anneenaissance;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
    public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
    
    public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

}
