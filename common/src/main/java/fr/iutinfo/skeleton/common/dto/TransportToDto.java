package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class TransportToDto
{
    final static Logger logger = LoggerFactory.getLogger(TransportToDto.class);
    private int id_transportTo = 0;
    private String libelle;

    public int getId_transportTo()
    {
        return id_transportTo;
    }

    public void setId_transportTo(int id_transportTo)
    {
        this.id_transportTo = id_transportTo;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
