package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class HebergementDto
{
    final static Logger logger = LoggerFactory.getLogger(HebergementDto.class);
    private int id_hebergement = 0;
    private String libelle;

    public int getId_hebergement()
    {
        return id_hebergement;
    }

    public void setId_hebergement(int id_hebergement)
    {
        this.id_hebergement = id_hebergement;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
