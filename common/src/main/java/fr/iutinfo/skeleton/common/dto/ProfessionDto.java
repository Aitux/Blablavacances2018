package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class ProfessionDto
{
    final static Logger logger = LoggerFactory.getLogger(ProfessionDto.class);
    private int id_profession = 0;
    private String libelle;

    public int getId_profession()
    {
        return id_profession;
    }

    public void setId_profession(int id_profession)
    {
        this.id_profession = id_profession;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
