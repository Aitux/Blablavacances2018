package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class TransportDto
{
    final static Logger logger = LoggerFactory.getLogger(TransportDto.class);
    private int id_transport = 0;
    private String libelle;

    public int getId_transport()
    {
        return id_transport;
    }

    public void setId_transport(int id_transport)
    {
        this.id_transport = id_transport;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
