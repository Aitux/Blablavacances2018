package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class ExpeDto
{
    final static Logger logger = LoggerFactory.getLogger(ExpeDto.class);
    private int id_expe = 0;
    private String libelle;

    public int getId_expe()
    {
        return id_expe;
    }

    public void setId_expe(int id_expe)
    {
        this.id_expe = id_expe;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
