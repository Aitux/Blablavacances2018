package fr.iutinfo.skeleton.common.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.common.dto
 * Created by aitux on 18/04/18.
 */
public class InteretSelfDto
{
    final static Logger logger = LoggerFactory.getLogger(InteretSelfDto.class);
    private int id_interetSelf = 0;
    private String libelle;

    public int getId_interetSelf()
    {
        return id_interetSelf;
    }

    public void setId_interetSelf(int id_interetSelf)
    {
        this.id_interetSelf = id_interetSelf;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }
}
