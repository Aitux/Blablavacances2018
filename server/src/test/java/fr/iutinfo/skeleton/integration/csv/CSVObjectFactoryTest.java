package fr.iutinfo.skeleton.integration.csv;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.csv.CSVObjectFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration
 * Created by aitux on 26/06/18.
 */
public class CSVObjectFactoryTest
{
    private File temp = null;
    private char separator = ';';


    @Before
    public void setUp()
    {
        try
        {
            // On crée un fichier temporaire pour les tests.
            temp = File.createTempFile("pattern", ".csv");
            // On précise que le fichier doit être supprimer une fois les tests terminés.
            temp.deleteOnExit();
            // On écrit à l'intérieur sous le même format que pour un csv.
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write("Test1;Test2\rTest3;Test4");
            out.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void createCSVObjectTest()
    {
        CSVObject expectedObject = new CSVObject(new String[]{"Test1", "Test2"}, new String[][]{{"Test3", "Test4"}});
        CSVObject actualObject = CSVObjectFactory.createCSVObject(temp, separator);
        Assert.assertEquals(expectedObject, actualObject);
        expectedObject = new CSVObject(new String[]{"Test4", "Test3"}, new String[][]{{"Test2", "Test1"}});
        Assert.assertNotEquals(expectedObject, actualObject);
    }
}
