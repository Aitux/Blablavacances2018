package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.db.dbObject.DBObject;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 02/07/18.
 */
public class DBFillerTest
{

    @Test
    public void createDBObjectTest()
    {
        CSVObject object = new CSVObject(new String[]{"preference1", "value", "preference1", "value", "preference2", "value"}, new String[][]{{"risotto", "1", "riz", "3", "boxe", "1"}, {"risotto", "2", "riz", "1", "boxe", "1"}});
        List<DBObject> expected = new ArrayList<>();

        DBObject object1 = new DBObject();
        object1.initFromKeyAndValue("risotto", "1");
        object1.initFromKeyAndValue("riz", "3");
        object1.initFromKeyAndValue("boxe", "1");
        expected.add(object1);
        object1 = new DBObject();
        object1.initFromKeyAndValue("risotto", "2");
        object1.initFromKeyAndValue("riz", "1");
        object1.initFromKeyAndValue("boxe", "1");
        expected.add(object1);
        List<DBObject> actual = new DBFillerMock(object, null).createDBOjects();
        Assert.assertEquals(expected, actual);
    }

    class DBFillerMock extends DBFiller
    {

        public DBFillerMock(CSVObject object, Connection c)
        {
            super(object, c);
        }

        @Override
        public String rowName()
        {
            return null;
        }

        @Override
        public void inject()
        {

        }

        @Override
        public String getIdentifier()
        {
            return "preference";
        }

        @Override
        public int startingPoint(String identifier)
        {
            String[] titles = csvObject.getTitles();
            for (int i = 0; i < titles.length; i++)
            {

                if (titles[i].contains(identifier))
                {
                    return i;
                }
            }
            return -1;

        }
    }
}
