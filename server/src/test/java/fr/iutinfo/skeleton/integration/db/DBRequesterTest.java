package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.csv.CSVObjectFactory;
import fr.iutinfo.skeleton.integration.db.dbObject.JobLabel;
import org.junit.*;

import javax.swing.filechooser.FileSystemView;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 26/06/18.
 */
public class DBRequesterTest
{
    private static Connection c = null;
    private File temp = null;
    private char separator = ';';

    @BeforeClass
    public static void setUpForClass()
    {
        try
        {

            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "test.db");
        } catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown()
    {
        try
        {
            c.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp()
    {
        try
        {
            // On crée un fichier temporaire pour les tests.
            temp = java.io.File.createTempFile("pattern", ".csv");
            // On précise que le fichier doit être supprimé une fois les tests terminés.
            temp.deleteOnExit();
            // On écrit à l'intérieur sous le même format que pour un csv.
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write("annee_naissance;id_preferenceTest;value;id_preferenceTest;value\r1997;1;3;2;1\r1996;1;1;2;2");
            out.close();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void requestIdAsNameTest()
    {
        CSVObject actuals = new CSVObject(new String[]{"annee_naissance", "id_preferenceTest", "value", "id_preferenceTest", "value","id_preferenceTest2","value","id_preferenceTest2","value"}, new String[][]{{"1997","1","3","2","1","1","3","2","1"},{"1996","1","1","2","2","1","2","2","2"}});
        DBRequester requester = new DBRequesterMock(actuals, c);
        actuals = requester.requestIdAsName();
        CSVObject expected = new CSVObject(new String[]{"annee_naissance", "id_preferenceTest", "value", "id_preferenceTest", "value", "id_preferenceTest2","value","id_preferenceTest2","value"}, new String[][]{{"1997", "toto", "3", "titi", "1","exemple","3","sample","1"}, {"1996", "toto", "1", "titi", "2","exemple","2","sample","2"}});
        Assert.assertEquals(expected, actuals);
        expected = new CSVObject(new String[]{"annee_naissance", "id_preferenceTest", "value", "id_preferenceTest", "value","id_preferenceTest2","value","id_preferenceTest2","value"}, new String[][]{{"1", "titi", "3", "toto", "1997","sample","1","exemple","2"}, {"1996", "titi", "1", "toto", "2","sample","exemple","1","2"}});
        Assert.assertNotEquals(expected, actuals);
    }

    @Test
    public void transformJobLabelAndSexToIntegerTest()
    {
        CSVObject object = new CSVObject(new String[]{"sexe","sexe","profession","profession","id","test","separator","profession","profession"}, new String[][]{{"Homme","Femme","Sport","Sciences - Informatiques", "3","test","sep","Droit - Sciences politiques", "Défense publique - Sécurité"}});
        CSVObject expected = new CSVObject(new String[]{"sexe","sexe","profession","profession","id","test","separator","profession","profession"}, new String[][]{{"0","1","16","15", "3","test","sep","7", "6"}});
        DBRequester requester = new DBRequesterMock(object, null);
        CSVObject actual = requester.transformJobLabelAndSexToInteger();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void preferenceStartingPointTest()
    {
        CSVObject csvObject = new CSVObject(new String[]{"toto", "titi", "id_preferenceTest"}, new String[][]{{}, {}});
        int expected = 2;
        DBRequester requester = new DBRequesterMock(csvObject, null);
        int actual = requester.startingPoint("preference");
        Assert.assertSame(expected, actual);
    }

    @Test
    public void convertTitleToRequestArgumentTest()
    {
        CSVObject csvObject = new CSVObject(new String[]{"toto", "titi", "id_preferenceTest", "value", "id_preferenceTeest", "value", "id_preferenceTeeest", "value"}, new String[][]{{}, {}});
        String[] expected = {"test", "teest", "teeest"};
        DBRequester requester = new DBRequesterMock(csvObject, null);
        String[] actual = requester.convertTitleToRequestArgument();
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void associateIdToFullNameTest()
    {
        CSVObject csvObject = new CSVObject(new String[]{"toto", "titi", "id_preferenceTest", "value", "id_preferenceTest", "value", "id_preferenceTest2", "value", "id_preferenceTest2", "value"},
                new String[][]{{"1", "2", "1", "2", "2", "2", "1", "3", "2", "1"},
                        {"2", "1", "1", "1", "2", "3", "1", "2", "2", "1"}});

        DBRequester mock = new DBRequesterMock(csvObject, c);
        List<Map<String, String>> actual = mock.associateIdToFullName();
        List<Map<String, String>> expected = new ArrayList<>();
        Map<String, String> inside = new LinkedHashMap<>();
        inside.put("1", "toto");
        inside.put("2", "titi");
        expected.add(inside);
        Map<String, String> second = new LinkedHashMap<>();
        second.put("1", "exemple");
        second.put("2", "sample");
        expected.add(second);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void splitStringArrayDuplicatesToMapTest()
    {
        DBRequester requester = new DBRequesterMock(null, null);
        String[] array = new String[]{"test", "test", "test", "boo", "boo", "hey", "hey", "hey", "hey", "LetsGo", "ThereWeAre", "ThereWeAre", "Ouch"};
        Map<String, Integer> actual = requester.splitStringArrayDuplicatesToMap(array);
        Map<String, Integer> expected = new HashMap<>();
        expected.put("test", 3);
        expected.put("boo", 2);
        expected.put("hey", 4);
        expected.put("LetsGo", 1);
        expected.put("ThereWeAre", 2);
        expected.put("Ouch", 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void requestIdToDBTest()
    {
        DBRequester mock = new DBRequesterMock(null, c);
        String expected = "toto";
        String actual = mock.requestIdToDB(mock.setUpPreparedStatement("test", "1"));
        Assert.assertEquals(expected, actual);
    }

    class DBRequesterMock extends DBRequester
    {

        public DBRequesterMock(CSVObject object, Connection connection)
        {
            super(object, connection);
        }

        @Override
        public PreparedStatement setUpPreparedStatement(String param1, String param2)
        {
            try
            {
                PreparedStatement stmt = c.prepareStatement("SELECT " + rowName() + " FROM " + param1 + " WHERE id=?");

                stmt.setString(1, param2);
                return stmt;
            } catch (SQLException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public String rowName()
        {
            return "libelle";
        }

        @Override
        public String getIdentifier()
        {
            return "preference";
        }

        @Override
        public int getSubstringStart()
        {
            return 13;
        }
    }
}
