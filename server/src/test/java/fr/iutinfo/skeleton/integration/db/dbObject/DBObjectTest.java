package fr.iutinfo.skeleton.integration.db.dbObject;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db.dbObject
 * Created by aitux on 02/07/18.
 */
public class DBObjectTest
{
    @Test
    public void initFromKeyAndValueTest()
    {
        Map<String, String> expected = new HashMap<>();
        expected.put("toto","3");
        expected.put("titi","2");
        expected.put("tata","1");
        expected.put("tyty","1997");
        DBObject mock = new DBObject();
        mock.initFromKeyAndValue("toto","3");
        mock.initFromKeyAndValue("titi","2");
        mock.initFromKeyAndValue("tata","1");
        mock.initFromKeyAndValue("tyty","1997");
        Assert.assertEquals(expected,mock.getLabelsAndAssociatedValues());
    }

}
