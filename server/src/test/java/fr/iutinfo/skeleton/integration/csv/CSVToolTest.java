package fr.iutinfo.skeleton.integration.csv;

import fr.iutinfo.skeleton.integration.csv.CSVTool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration
 * Created by aitux on 25/06/18.
 */
public class CSVToolTest
{

    private CSVTool tool = CSVTool.getInstance();
    private File temp = null;
    private char separator = ';';


    @Before
    public void setUp()
    {
        try
        {
            // On crée un fichier temporaire pour les tests.
            temp = File.createTempFile("pattern", ".csv");
            // On précise que le fichier doit être supprimer une fois les tests terminés.
            temp.deleteOnExit();
            // On écrit à l'intérieur sous le même format que pour un csv.
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write("Test1;Test2\rTest3;Test4");
            out.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void readCSVTest()
    {
        String[][] expectedOutput = new String[][]{{"Test1", "Test2"}, {"Test3", "Test4"}};
        String[][] actualOutput = tool.readCSV(temp, separator);
        Assert.assertArrayEquals(expectedOutput, actualOutput);
    }

    @Test
    public void getAllPossibleValueTest()
    {
        List<String> expectedOutput = new ArrayList<>();
        expectedOutput.add("Test1");
        expectedOutput.add("Test2");
        expectedOutput.add("Test3");
        expectedOutput.add("Test4");
        Collections.sort(expectedOutput);
        List<String> actualOutput = tool.getAllPossibleValue(new String[][]{{"Test1", "Test2"}, {"Test3", "Test4"}});
        Assert.assertEquals(expectedOutput, actualOutput);
        actualOutput = tool.getAllPossibleValue(new String[][]{{"Toto", "Titi"}, {"Test1", "Test4"}});
        Assert.assertNotEquals(expectedOutput, actualOutput);
    }





}
