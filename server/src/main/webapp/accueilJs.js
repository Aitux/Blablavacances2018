    $(document).ready(function () {
        $("#profil").hide();
        $("#creeVoyage").hide();
        $("#mesVoyages").hide();
        $("#toutVoyage").hide();
        $("#deco").hide();
        $(".accueilConnec").hide();
        $(".pageProfil").hide();
        $(".recherche").hide();

        $(".connect").hide();
        $(".tuto").hide();
        $(".charte").hide();
        $(".inscription").hide();
        $(".contact").hide();
        $(".connexion").hide();
        $(".pageVoyage").hide();
        $(".profil").hide();
        $("#deletence").hide();

        $('#deco').click(() => {
            location.reload(true);
        });

//        $('.suiteQuiz').click((event) => {
              //
              //            $('.' + event.target.id).show();
              //            $(event.target).parent().hide();
              //        });

        $("#charte").click(function () {

            $(".charte").show();
            $(".accueil").hide();
            $(".tuto").hide();
            $(".inscription").hide();
            $("#inscription").show();
            $("#charte").classList.add("#menu li:hover");
            $(".contact").hide();
            $(".connexion").hide();
            $(".pageVoyage").hide();
            $(".profil").hide();
            $("#deletence").hide();
        });
        $("#accueil").click(function () {

            $(".charte").hide();
            $(".accueil").show();
            $(".tuto").hide();
            $(".inscription").hide();
            $("#inscription").show();
            $(".contact").hide();
            $(".connexion").hide();
            $(".pageVoyage").hide();
            $(".profil").hide();
            $("#deletence").hide();


        });
        $("#tuto").click(function () {


            $(".charte").hide();
            $(".accueil").hide();
            $(".inscription").hide();
            $(".tuto").show();
            $("#inscription").show();
            $(".contact").hide();
            $(".connexion").hide();
            $(".pageVoyage").hide();
            $(".profil").hide();
            $("#deletence").hide();


        });
        $("#inscription").click(function () {

            $(".charte").hide();
            $(".accueil").hide();
            $(".inscription").show();
            $("#inscription").hide();
            $(".tuto").hide();
            $(".contact").hide();
            $(".connexion").hide();
            $(".pageVoyage").hide();
            $(".profil").hide();
            $("#deletence").hide();



        });

        $("#contact").click(function () {

            $(".charte").hide();
            $(".accueil").hide();
            $(".inscription").hide();
            $("#inscription").show();
            $(".tuto").hide();
            $(".contact").show();
            $(".connexion").hide();
            $(".pageVoyage").hide();
            $(".profil").hide();
            $("#deletence").hide();


        });

        $("#connexion").click(function () {
            $(".charte").hide();
            $(".accueil").hide();
            $(".inscription").hide();
            $("#inscription").hide();
            $(".tuto").hide();
            $(".contact").hide();
            $(".connexion").show();
            $(".pageVoyage").hide();
            $("#connexion").hide();
            $(".profil").hide();
            $(".pageProfil").hide();
            $("#deletence").hide();

        });


        $("#submit").click(function () {
            login();
            $("connection").hide();
            $("inscription").hide();
            $(".pageProfil").hide();
            $(".accueilConnec").show();
            $(".recherche").hide();

            $(".pageVoyage").hide();
            $(".accueil").hide();
            $(".charte").hide();
            $(".contact").hide();
            $(".tuto").hide();
            $("#tbody").empty();
            listerVoyage();
            $("#deletence").show();
        });

        $("#creeVoyage").click(function () {
            $(".profil").hide();
            $(".pageVoyage").show();
//            $(".avecqui").hide();
 //            $(".vousmeme").hide();
            $(".recherche").hide();
            $(".accueilConnec").hide();
            $(".pageProfil").hide();
            $("#deletence").hide();


        });
        $("#profil").click(function () {
            $(".pageProfil").hide();
            $(".accueilConnec").show();
            $(".recherche").hide();

            $(".pageVoyage").hide();
            $(".accueil").hide();
            $(".charte").hide();
            $(".contact").hide();
            $(".tuto").hide();
            $("#tbody").empty();
            listerVoyage();
            $("#deletence").show();

        });

        //  $("#addVoyage").click(function(){

        // });
        $("#modifier").click(function () {
            $(".accueilConnec").hide();
            $(".pageProfil").show();
            $(".recherche").hide();
            $(".pageVoyage").hide();

            $("#deletence").hide();

        });

        /*    $("#addPreference").click(function () {
                let flag = false;
                if ($('#nomVoyage').val() === "") {
                    flag = true;
                    $('#tripName').css('color', 'red');
                    window.location.hash = '#tripName';
                    alert("Le nom du voyage ne peut être vide.");
                }
                $.ajax({
                    type: 'GET',
                    contentType: 'application/json',
                    url: "v1/voyages",
                    success: function (data, textStatus, jqXHR) {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            console.log(data[i].name + " -> " + $('#nomVoyage').val());
                            console.log($('#nomVoyage').val() == data[i].name)
                            if ($('#nomVoyage').val() == data[i].name) {
                                $('#tripName').css('color', 'red');
                                window.location.hash = '#tripName';
                                alert("Nom de voyage déjà utilisé");
                                flag = true;
                            }
                        }
                    },
                    async: false
                });

                if (!flag) {
                    if (confirm("Voulez-vous vraiment ajouter ce voyage?")) {
                        // Action à entreprendre.
                        // Code ou appel de fonction
                        postVoyage(
                            $('#nomVoyage').val(),
                            $('#SelectRegion').val(),
                            $('#descrVoyage').val(),
                            $('#voyageDep').val(),
                            $('#voyageRet').val(),
                            $('#CapaciteVoyage').val(),
                            $('#budgetInput').html(),
                            "v1/voyages/"
                        );

                        var str1 = "";
                        var str2 = "";
                        var str3 = "";
                        var t = [];

                        var getAllPrefUser = () => {
                            let tab = [];
                            $('.prefUser:checked').each((idx, ele) => {
                                tab.push((ele.value));
                            });
                            console.log(tab);
                            return tab;
                        }
                        t = getAllPrefUser();
                        var i = 0;
                        for (i; i < t.length; i++) {
                            if (t[i].charAt(t[i].length - 1) === "1") {
                                str1 += t[i] + ",";
                            } else if (t[i].charAt(t[i].length - 1) === "2") {
                                str2 += t[i] + ",";
                            } else {
                                str3 += t[i] + ",";
                            }
                        }
                        postPreference(str1, str2, str3);

                    }
                }
            });*/
        $("#toutVoyage").click(function () {
            $(".recherche").show();
            $(".pageProfil").hide();
            $(".accueilConnec").hide();
            $(".pageVoyage").hide();
            $("#deletence").hide();

        });


        $("#modifProfil").click(function () {
            var str1 = "";
            var str2 = "";
            var str3 = "";
            var t = [];
            var getAllProfilPref = () => {
                let tab = [];
                $('.profilPref:checked').each((idx, ele) => {
                    tab.push((ele.value));
                });
                console.log(tab);
                return tab;
            }
            t = getAllProfilPref();
            var i = 0;
            console.log(t[0]);
            for (i; i < t.length; i++) {
                console.log(t[i]);
                if (t[i].charAt(t[i].length - 1) === "1") {
                    str1 += t[i].substring(0, t[i].length - 1) + ",";
                } else if (t[i].charAt(t[i].length - 1) === "2") {
                    str2 += t[i].substring(0, t[i].length - 1) + ",";
                } else {
                    str3 += t[i].substring(0, t[i].length - 1) + ",";
                }
            }
            //    getUserPref();
            postPreferenceUser(str1, str2, str3);
            putUser(str1, str2, str3);



        });



        $("#delete").click(function () {
            console.log($('#saisieDelete').val());
            if ($('#saisieDelete').val() === "") {
                alert("Veuillez entrer un voyage valide");
            } else {
                var bcl = document.querySelectorAll('#tabVoyages td');
                for (var i = 0; i < bcl.length; i++) {
                    if ($("#saisieDelete").val() === bcl[i].innerText) {
                        /****/
                        getVoyage($("#saisieDelete").val());
                    }
                }
            }
        });



    });
