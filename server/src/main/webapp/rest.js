$(document).ready(() => {
    let voy;
    $('#loading').hide();

    $("#addPreference").click(() =>  {
        let flagLimite;
        // APARITION ECRAN CHARGEMENT
        $('.page').hide();
        $('#loading').show();
        /////////////////////////////

        // On commence par vérifier que l'utilisateur peut bien créer un voyage.
        $.ajax({
            url: "v1/voyage/limite/" + idUser,
            type: "GET",
            async: false,
            success: () => {
                flagLimite = true;
            },
            error: () => {
                flagLimite = false;
            }
        });
        if (flagLimite === true) {
            // On stock les valeurs du questionnaire dans 2 objets.
            getQuestionnaire();
            console.log(resultat);
            // On crée un voyage en BDD
            $.ajax({
                url: "/v1/voyage",
                type: "POST",
                async: false,
                contentType: 'application/json',
                data: JSON.stringify({
                    "id_createur": idUser,
                    "budget": resultat.budget,
                    "route_seul": resultat.routeSeul,
                    "nb_co_vac": resultat.nbrCoVac,
                    "sexe_co_vac": resultat.sexeCoVac,
                    "age_co_vac": resultat.ageCoVac,
                    "etude_co_vac": resultat.etudeCoVac,
                    "fumeur_co_vac": resultat.fumeurCoVac,
                    "animal_de_compagnie_co_vac": resultat.pet,
                    "description": resultat.description
                }),
                success: (data, textStatus, request) => {
                    let str = request.getResponseHeader("Location");
                    str = str.substr(32, str.length);
                    voy = str;
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    console.log(textStatus);
                    console.log(jqXHR.status);
                    if (jqXHR.status != 201) {
                        console.log("Error while creating voyage");
                        flagLimite = false;
                    }
                }
            });

            if (flagLimite === true) {
                // ON REMPLIT LA BDD AVEC TOUS LES DOMAINES PRESENTS DANS LE QUESTIONNAIRE//
                // FUTURE Il y a probablement mieux et plus efficace comme code.
                let tab;
                for (let k in question) {
                    tab = Object.keys(question[k]);
                    for (let i = 0; i < tab.length; i++) {
                        $.ajax({
                            url: "v1/" + k + "/libelle/" + tab[i],
                            type: "GET",
                            async: false,
                            success: () => {},
                            error: () => {
                                $.ajax({
                                    url: "/v1/" + k,
                                    type: "POST",
                                    contentType: 'application/json',
                                    async: false,
                                    data: JSON.stringify({
                                        "libelle": tab[i]
                                    }),
                                    success: () => {},
                                    error: () => {}
                                });
                            }
                        });
                    }
                }
                // Cette partie peut être un peu longue car les requêtes sont synchrones pour des raisons 
                // de cohérence au sein de la base de données.
                ///////////////////////////FIN DU REMPLISSAGE/////////////////////////////////////////////

                for (let k in question) {
                    tab = Object.entries(question[k]);
                    let refId = "id_" + k;
                    let pos = "preference" + capitalizeFirstLetter(k);
                    for (let i = 0; i < tab.length; i++) {
                        $.ajax({
                            async: false,
                            url: "/v1/" + k + "/libelle/" + tab[i][0],
                            type: "GET",
                            dataType: "json",
                            success: (json) => {
                                let id = json["id_" + k];
                                $.ajax({
                                    async: false,
                                    url: "/v1/" + pos,
                                    type: "POST",
                                    data: JSON.stringify({
                                        "id_user": idUser,
                                        "id_voyage": voy,
                                        "id": id,
                                        "value": tab[i][1]
                                    }),
                                    contentType: 'application/json'
                                });
                            },
                            error: (error, textStatus, errorThrown) => {
                                console.log(error.status);
                                console.log(textStatus);
                            }
                        });

                    }
                }
                tab = Object.entries(interetSelf["interetSelf"]);
                for (let i = 0; i < tab.length; i++) {
                    $.ajax({
                        async: false,
                        url: "/v1/interetSelf/libelle/" + tab[i][0],
                        type: "GET",
                        dataType: "json",
                        success: (json) => {
                            let id = json["id_interetSelf"];
                            $.ajax({
                                async: false,
                                url: "/v1/userInteretSelf",
                                type: "POST",
                                data: JSON.stringify({
                                    "id_user": idUser,
                                    "id": id,
                                    "value": tab[i][1]
                                }),
                                contentType: 'application/json'
                            });
                        },
                        error: (error, textStatus, errorThrown) => {
                            console.log(error.status);
                            console.log(textStatus);
                        }
                    });
                }
                console.log("fumeur ? " + resultat.fumeur);
                $.ajax({
                    url: "v1/user/etuProf/" + idUser,
                    type: "PUT",
                    contentType: 'application/json',
                    data: JSON.stringify({
                        "niveauEtude": resultat.etudeSelf,
                        "profession": resultat.professionSelf,
                        "fumeur": resultat.fumeur
                    })
                });


            } else
                alert("Erreur serveur");
        } else
            alert("Vous avez déjà crée un projet de vacances.");

        // DISPARITION ECRAN CHARGEMENT
        $('#loading').hide();
        $('.page').show();
        /////////////////////////////

    });
});


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
