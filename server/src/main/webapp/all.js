var log;
var password;
var name;
var mail;
var idUser;
var idVoyage = new Object();

// id des tables présentes dans le questionnaire "Créer un projet de vacances"
var idTable = [];

// Toutes les classes que contiennent les tables
var tableClass = [];

// l'objet contenant tous les résultats qui ne viennent pas d'une architecture en 3 réponses de: créer voyage
var resultat = {};

var question = {};

var interetSelf = {};

// Toutes les balises des listes à choix multiples
var balisesSelectMultiples = [];

// Checkbox et radio sauf les questionnaires "centres d'intérêt" (self & co-vac)
var balisesCheckRadio = [];

function mouseoverPass(obj) {
    var obj = document.getElementById('myPassword');
    obj.type = "text";
}

function mouseoutPass(obj) {
    var obj = document.getElementById('myPassword');
    obj.type = "password";
}


var res = {
    motive: [],
    neutre: [],
    pasEnvie: []
};

function getUser(name) {
    getUserGeneric(name, "v1/user/");
}

function getUserGeneric(name, url) {
    $.getJSON(url + name, function (data) {
        afficheUser(data);
    });
}

function suiteQuestionnaire(index) {

}

function login() {
    getWithAuthorizationHeader("v1/login", function (data) {
        $("#formConnec").hide();
        //afficheUser(data);
        if (data.alias === ($("#loginConnect")).val()) {

            log = data.alias;
            name = data.name;
            mail = data.email;
            idUser = data.id;
            console.log("idUser qd il se co " + idUser);
            $(".connexion").hide();
            $(".connect").show();
            //$(".pageVoyage").show();
            $("#profil").show();
            $(".accueilConnec").show();
            $("#creeVoyage").show();
            $("#mesVoyages").show();
            $("#toutVoyage").show();
            $('#deco').show();
            $("#accueil").hide();
            $("#charte").hide();
            $("#tuto").hide();
            $("#contact").hide();


            $(".accueilConnec #monProfil").append(log);
            $(".accueilConnec #monLogin").append(name);
            $(".accueilConnec #monEmail").append(mail);
            listerVoyage();
        } else {
            alert("Mot de passe ou login Incorrect");

        }
    });
}

var getClassFromTable = (str) => {
    $('#' + str).find('input').each(function (idx, ele) {
        tableClass.push(ele.className);
    });

    tableClass = removeDuplicatesFromArray(tableClass);
}

var getTableId = () => {

    $('.pageVoyage table').each((idx, ele) => {
        idTable.push(ele.id);
    })
    idTable = removeDuplicatesFromArray(idTable);
}

var getResultatFromTable = () => {
    getTableId();
    for (let k = 0; k < idTable.length; k++) {
        getClassFromTable(idTable[k]);
    }
    for (let k = 0; k < tableClass.length; k++) {
        getTableChecked(tableClass[k]);
    }
}

var getTableChecked = (string) => {
    let tmp = {};
    $('.' + string + ':checked').each((idx, ele) => {
        tmp["" + ele.name] = ele.value;
    });
    if (string === "interetSelf") {
        interetSelf["" + string] = tmp;
        question["" + string] = tmp;
    } else {
        question["" + string] = tmp;
    }
}


var getDescription = () => {
    resultat["description"] = $('#descrVoyage').val();
}

var getAllMultId = () => {
    $('.mult').each(function (idx, ele) {
        balisesSelectMultiples.push(ele.id);
    });
}

var getBoxGeneric = (string) => {
    var str = "";
    $('.' + string + ':checked').each(function (idx, ele) {
        str += ele.value + ',';
    });
    str = str.substr(0, str.length - 1);
    resultat["" + string] = str;
}

var getAllBoxRadio = () => {
    balisesCheckRadio = [];
    getAllCheckRadioName();
    for (let i = 0; i < balisesCheckRadio.length; i++) {
        getBoxGeneric(balisesCheckRadio[i]);
    }

}

var getAllCheckRadioName = () =>  {
    $('.check').each(function (idx, ele) {
        balisesCheckRadio.push(ele.name);
    });
    balisesCheckRadio = removeDuplicatesFromArray(balisesCheckRadio);
}


var removeDuplicatesFromArray = (array) => {
    let uniqueArray = array.filter(function (item, pos) {
        return array.indexOf(item) == pos;
    });
    return uniqueArray;
}


var getMultiSelect = (integer) => {
    let tab = $('#' + balisesSelectMultiples[integer]).val();
    let strRes = "";
    for (let i = 0; i < tab.length; i++) {
        strRes += tab[i] + ',';
    }
    strRes = strRes.substr(0, (strRes.length - 1));
    return strRes;
}

var getAllMultiSelect = () => {
    balisesSelectMultiples = [];
    getAllMultId();
    for (let i = 0; i < balisesSelectMultiples.length; i++) {
        resultat["" + balisesSelectMultiples[i]] = (getMultiSelect(i));
    }
}

function profile() {
    getWithAuthorizationHeader("v1/profile", function (data) {
        afficheUser(data);
    });
}

var getQuestionnaire = () =>  {
    getSingleValue();
    getAllBoxRadio();
    getAllMultiSelect();
    getDescription();
    getResultatFromTable()
    console.log(resultat);
}

function getWithAuthorizationHeader(url, callback) {
    if ($("#loginConnect").val() != "") {
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            beforeSend: function (req) {
                req.setRequestHeader("Authorization", "Basic " + btoa($("#loginConnect").val() + ":" + $("#passwordConnect").val()));
            },
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                alert('error: ' + textStatus);
            }
        });
    } else {
        $.getJSON(url, function (data) {
            afficheUser(data);
        });
    }
}

/*
$(function () {
    $('#formInscr').submit(function () {
        console.log("Salut :)");
        let nom = $(this).find('input[name=nom]').val();
        let prenom = $(this).find('input[name=prenom]').val();
        let login = $(this).find('input[name=login]').val();
        let password = $(this).find('input[name=password]').val();
        console.log($(this).find('input[name=sexe]').val());
        let annee = $(this).find('inut[name=YoB]').val();

        postUserInscr(nom, prenom, login, password, annee, sexe);
    });

});
*/



function postUser(name, alias, email, pwd) {
    postUserGeneric(name, alias, email, pwd, 'v1/user/');
}

var postUserInscr = (alias, name, email, pwd, annee, sexe, prenom) => {
    $.ajax({
        url: 'v1/user/',
        type: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify({
            "name": name,
            "alias": alias,
            "email": email,
            "prenom": prenom,
            "anneenaissance": annee,
            "sexe": sexe,
            "password": pwd,
            "id": 0
        })
    });
}


function postUserGeneric(name, alias, email, pwd, url) {
    console.log("postUserGeneric " + url);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: "json",
        data: JSON.stringify({
            "name": name,
            "alias": alias,
            "email": email,
            "password": pwd,
            "id": 0
        }),
        success: function (data, textStatus, jqXHR) {
            afficheUser(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postUser error: ' + textStatus);
        }
    });
}

var getSingleValue = () => {
    resultat["precision"] = $('#precision').val();
    resultat["professionSelf"] = $('#professionSelf').val();

}


function listUsers() {
    listUsersGeneric("v1/user/");
}

function listUsersGeneric(url) {
    $.getJSON(url, function (data) {
        afficheListUsers(data)
    });
}

function afficheUser(data) {
    //console.log(data);
    $("#reponse").html(userStringify(data));
}

var displayUserPref = (data) => {
    $("#reponse").html(userPrefStringify(data));
}

var userPrefStringify = (pref) => {
    return "motive: " + pref.motive + ", neutre: " + pref.neutre + ", pasEnvie: " + pref.pasEnvie;
}

function afficheListUsers(data) {
    var ul = document.createElement('ul');
    ul.className = "list-group";
    var index = 0;
    for (index = 0; index < data.length; ++index) {
        var li = document.createElement('li');
        li.className = "list-group-item";
        li.innerHTML = userStringify(data[index]);
        ul.appendChild(li);
    }
    $("#reponse").html(ul);
}



function userStringify(user) {
    return user.id + ". " + user.name + " &lt;" + user.email + "&gt;" + " (" + user.alias + ")";
}

function postVoyage(nomVoyage, ville, description, depart, retour, voyageurs, budget, url) {
    console.log("postVoyage" + url)
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: "json",
        data: JSON.stringify({
            "id": 0,
            "idUser": idUser,
            "name": nomVoyage.trim(),
            "ville": ville,
            "description": description.trim(),
            "depart": depart,
            "retour": retour,
            "budget": budget,
            "capacite": voyageurs
        }),
        success: function (data, textStatus, jqXHR) {
            console.log('postVoyage : success', data.id, data);
            idVoyage = data.id;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postVoyage error: ', textStatus);
        }
    });
}

var deleteBtn = (event) => {
    console.log("deleteBtn: ");
    console.log(event.target);
    getVoyage(event.target.id);
}

function listerVoyage() {
    $.ajax({
        url: "v1/voyage/limite/" + idUser,
        type: "GET",
        success: () => {
            $('.connect').empty();
            $('.connect').append("<center><h3>Vous n'avez pas encore créé de projet de vacances.</h3></center>");
        },
        error: () => {
            $('.connect').empty();
            $('.connect').append("<center><h3>Félicitations, votre projet de vacances a bien été créé.</h3></center>")
        }
    });

}

var isEmpty = (el) => {
    return !$.trim(el.html());
}


function postPreference(aime, moyen, aimePas) {
    console.log("postPreference" + "/v1/preference/");
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "v1/preference/",
        dataType: "json",
        data: JSON.stringify({
            "idVoyage": idVoyage,
            "motive": aime,
            "neutre": moyen,
            "pasEnvie": aimePas
        }),
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            //displayUser(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postPreference error: ' + textStatus);
        }
    });

}

function postPreferenceUser(aime, moyen, aimePas) {
    console.log(idUser + "=iduser");
    console.log("postPreferenceUser" + "/v1/PreferenceUser/");
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: "v1/PreferenceUser/",
        dataType: "json",
        data: JSON.stringify({
            "idUser": idUser,
            "motive": aime,
            "neutre": moyen,
            "pasEnvie": aimePas
        }),
        success: function (data, textStatus, jqXHR) {
            displayUserPref(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postPreference error: ' + textStatus);
        }

    });
}

function putUser(aime, moyen, aimePas) {
    console.log("postPreferenceUser" + "/v1/PreferenceUser/" + idUser);
    $.ajax({
        type: 'PUT',
        contentType: 'application/json',
        url: "v1/PreferenceUser/" + idUser,
        dataType: "json",
        data: JSON.stringify({
            "motive": aime,
            "neutre": moyen,
            "pasEnvie": aimePas
        }),
        success: function (data, textStatus, jqXHR) {
            console.log("success ! To update PreferenceUser !")
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postPreference error: ' + textStatus);


        }
    });
}

var handlerDeparture = (event) => {
    let dep = event.target.value;
    let now = new Date();
    var dd = now.getDate();
    var mm = now.getMonth() + 1;
    var yyyy = now.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    now = yyyy + '-' + mm + '-' + dd;

    if (dep < now) {
        alert("Les voyages dans le temps sont actuellement indisponibles.");
        $('#departureDate').css('color', 'red');
        $('#voyageDep').val(now);
    } else {
        $('#departureDate').css('color', 'black');
    }
}


var handlerReturn = (event) => {
    let dep = $('#voyageDep').val();
    let ret = event.target.value;
    if (dep > ret) {
        $("#returnDate").css('color', 'red');
        alert("Attention ! La date de retour ne peut être antérieur à la date de départ !");
        $('#voyageRet').val(dep);
    } else {
        $("#returnDate").css('color', 'black');
    }
}

function deleteVoyage(id) {
    console.log("deleteVoyage" + "/v1/voyages/");
    console.log(id);
    $.ajax({
        type: 'DELETE',
        url: "/v1/voyages/" + id,

        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            $("#tbody").empty();
            listerVoyage();
            alert("Voyage supprimé");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('postPreference error: ' + textStatus);
            alert("Pas de Voyages correspondants");
        }
    });
}

function getVoyage(nom) {
    $.getJSON("v1/voyages/" + nom, function (data) {
        console.log("should work");
        deleteVoyage(data.id);
    });

}
