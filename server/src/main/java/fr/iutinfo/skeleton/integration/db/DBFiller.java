package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.db.dbObject.DBObject;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 26/06/18.
 */
public abstract class DBFiller implements DB
{
    protected CSVObject csvObject;
    protected Connection c;

    public DBFiller(CSVObject object, Connection c)
    {
        this.csvObject = object;
        this.c = c;
    }

    public Connection getConnection()
    {
        return c;
    }

    public void setConnection(Connection c)
    {
        this.c = c;
    }

    public abstract void inject();

    protected List<DBObject> createDBOjects()
    {
        List<DBObject> dbObjectList = new ArrayList<>();
        String[] titles = csvObject.getTitles();
        String[][] values = csvObject.getValues();

        // FINIR CA. Puis après tant pis on va la remplir comme un gros sale. Parce qu'aux travers d'un csv il n'y a pas moyen de vraiment les différencier donc on va faire ça salement :'(
        // FUTURE séparer en liste d'objets
        for (String[] value : values)
        {
            DBObject obj = new DBObject();
            for (int k = 0; k < values[0].length; k++)
            {
                if (k < startingPoint(getIdentifier()))
                {
                    obj.initFromKeyAndValue(titles[k], value[k]);
                } else if (titles[k].contains(getIdentifier()) && k >= startingPoint(getIdentifier()))
                {
                    obj.initFromKeyAndValue(value[k], value[k + 1]);
                }
            }
            dbObjectList.add(obj);
        }

        return dbObjectList;
    }

    @Override
    public int startingPoint(String identifier)
    {
        String[] titles = csvObject.getTitles();
        for (int i = 0; i < titles.length; i++)
        {

            if (titles[i].contains(identifier))
            {
                return i;
            }
        }
        return -1;
    }

}
