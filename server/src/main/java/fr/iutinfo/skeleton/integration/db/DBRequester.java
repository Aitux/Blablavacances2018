package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.db.dbObject.JobLabel;
import fr.iutinfo.skeleton.integration.db.dbObject.Sex;

import javax.validation.constraints.NotNull;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 26/06/18.
 */
public abstract class DBRequester implements DB
{
    protected CSVObject csvObject;
    protected Connection c;

    public DBRequester(CSVObject object, Connection connection)
    {
        this.csvObject = object;
        this.c = connection;
        transformJobLabelAndSexToInteger();
    }

    public CSVObject requestIdAsName()
    {
        List<Map<String, String>> fullName = associateIdToFullName();
        String[][] values = csvObject.getValues();
        int flag = 0, cpt = 0;

        for (int xs = 0; xs < values.length; xs++)
        {
            Map<String, String> map = fullName.get(cpt++);
            Iterator<String> iterator = map.keySet().iterator();
            for (int i = startingPoint(getIdentifier()); i < values[0].length; i++)
            {
                if (flag == 0)
                {
                    if (iterator.hasNext())
                        values[xs][i] = map.get(iterator.next());
                    else if (cpt < fullName.size())
                    {
                        map = fullName.get(cpt++);
                        iterator = map.keySet().iterator();
                        values[xs][i] = map.get(iterator.next());

                    }
                }
                flag = 1 - flag;
            }
            cpt = 0;
        }
        return new CSVObject(csvObject.getTitles(), values);
    }

    /**
     * Return the starting point from a CSVObject that matches the identifier.
     *
     * @param identifier is a String
     * @return an integer or -1 if no match with the identifier was found.
     */
    public int startingPoint(String identifier)
    {
        String[] titles = csvObject.getTitles();
        for (int i = 0; i < titles.length; i++)
        {

            if (titles[i].contains(identifier))
            {
                return i;
            }
        }
        return -1;
    }

    public List<Map<String, String>> associateIdToFullName()
    {
        List<Map<String, String>> hashMapList = new ArrayList<>();
        Map<String, Integer> stringIntegerMap = splitStringArrayDuplicatesToMap(convertTitleToRequestArgument());
        for (String str :
                stringIntegerMap.keySet())
        {
            Map<String, String> map = new LinkedHashMap<>();
            for (int i = 1; i <= stringIntegerMap.get(str); i++)
            {
                String tmp = requestIdToDB(setUpPreparedStatement(str, "" + i));
                map.put("" + i, tmp);

            }
            hashMapList.add(map);
        }

        return hashMapList;
    }

    public CSVObject transformJobLabelAndSexToInteger()
    {
        String[] titles = csvObject.getTitles();
        String[][] values = csvObject.getValues();
        for (int i = 0; i < values.length; i++)
        {
            for (int k = 0; k < values[0].length; k++)
            {
                if (titles[k].equals("profession"))
                {
                    values[i][k] = "" + JobLabel.getJobLabel(values[i][k]).getValue();
                }
                if (titles[k].equals("sexe"))
                {
                    values[i][k] = "" + Sex.getSexFromString(values[i][k]).getValue();
                }
            }
        }
        return new CSVObject(titles, values);
    }

    public Map<String, Integer> splitStringArrayDuplicatesToMap(String[] array)
    {
        int cpt = 0;
        String previous = "", current;
        Map<String, Integer> res = new LinkedHashMap<>();
        previous = array[0];
        for (int i = 0; i < array.length; i++)
        {
            current = array[i];
            if (current.equals(previous))
                cpt++;
            else
            {
                res.put(previous, cpt);
                cpt = 1;
            }
            if (i == array.length - 1)
                res.put(current, cpt);

            previous = current;
        }

        return res;
    }

    public Connection getConnection()
    {
        return c;
    }

    public String[] convertTitleToRequestArgument()
    {
        int size = startingPoint(getIdentifier());
        String[] tab = csvObject.getTitles();
        String[] res = new String[(tab.length - size) / 2];
        int k = 0;
        int flag = 0;
        for (int i = size; i < tab.length; i++)
        {
            if (flag == 0)
            {
                res[k] = tab[i].substring(getSubstringStart(), tab[i].length()).toLowerCase();
                k++;
            }
            flag = 1 - flag;
        }
        return res;
    }

    public String requestIdToDB(@NotNull PreparedStatement stmt)
    {
        try
        {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            String retVal = rs.getString(rowName());
            rs.close();
            stmt.close();
            return retVal;

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * First parameter should be the table name and second the id that you want to transform to a label.
     *
     * @param param1 table name.
     * @param param2 id.
     * @return a ready to use PreparedStatement.
     */
    public abstract PreparedStatement setUpPreparedStatement(String param1, String param2);

    public abstract int getSubstringStart();

}
