package fr.iutinfo.skeleton.integration.csv;

import java.util.Arrays;
import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration
 * Created by aitux on 25/06/18.
 */
public class CSVObject
{
    private String[] titles;
    private String[][] values;

    public CSVObject(String[] titles, String[][] values)
    {
        this.titles = titles;
        this.values = values;
    }

    public String[] getTitles()
    {
        return titles;
    }

    public List<String> getTitlesAsList()
    {
        return Arrays.asList(titles);
    }

    public String[][] getValues()
    {
        return values;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CSVObject csvObject = (CSVObject) o;
        if (!Arrays.equals(titles, csvObject.titles))
            return false;
        if (!checkArraysEquals(values, csvObject.getValues()))
            return false;
        return true;
    }

    private boolean checkArraysEquals(String[][] array1, String[][] array2)
    {
        for (int i = 0; i < array1.length; i++)
        {
            for (int k = 0; k < array1[0].length; k++)
            {
                if(!array1[i][k].equals(array2[i][k]))
                    return false;
            }
        }
        return true;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("CSVObject{").append("titles=").append(Arrays.toString(titles)).append(", values=").append("[");
        for (String[] str :
                values)
        {
            for (String string :
                    str)
            {
                stringBuilder.append(string).append(", ");
            }
            stringBuilder.deleteCharAt(stringBuilder.length()-1);
            stringBuilder.deleteCharAt(stringBuilder.length()-2);
            stringBuilder.append("],[");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        stringBuilder.deleteCharAt(stringBuilder.length()-2);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
