package fr.iutinfo.skeleton.integration.csv;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.Arrays;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration
 * Created by aitux on 26/06/18.
 */
public class CSVObjectFactory
{

        /**
         * Create an object that's easier to use than the basic arrays with the values.
         *
         * @param file
         * @param separator
         * @return
         */
        public static CSVObject createCSVObject(@NotNull File file, char separator)
        {
            String[][] tmpStringArray = CSVTool.getInstance().readCSV(file, separator);
            String [] titles = tmpStringArray[0];
            return new CSVObject(titles, Arrays.copyOfRange(tmpStringArray,1,tmpStringArray.length));
        }

}
