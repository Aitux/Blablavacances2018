package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.api.CsvExporter;
import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.csv.CSVObjectFactory;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.sql.*;
import java.util.Objects;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 26/06/18.
 */
public class DBFillerSMAC extends DBFiller
{
    public DBFillerSMAC(CSVObject object, Connection c)
    {
        super(object, c);
    }

    public static void main(String[] args) throws ClassNotFoundException
    {
        CSVObject object = CSVObjectFactory.createCSVObject(new File("/home/aitux/Téléchargements/data(2).csv"),';');

        Connection c = null;
        Connection co = null;
        try
        {
            c = DriverManager.getConnection("jdbc:sqlite:" + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "data.db");
            Class.forName("org.postgresql.Driver");
            co = DriverManager.getConnection("jdbc:postgresql://localhost:5432/partens", "aitux","e94a0dc724");
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        DBRequester smac = new DBRequesterSMAC(object, c);
        CSVObject object1 = smac.requestIdAsName();
        DBFiller dbFiller = new DBFillerSMAC(object1, co);
        dbFiller.createDBOjects();
        CsvExporter csvExporter = new CsvExporter();

        try
        {
            System.out.println(Objects.requireNonNull(co).isValid(10));
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        System.out.println(object1.toString());
    }

    @Override
    public String rowName()
    {
        return "libelle";
    }

    @Override
    public void inject()
    {

    }

    @Override
    public String getIdentifier()
    {
        return "preference";
    }

    @Override
    public int startingPoint(String identifier)
    {
        return 0;
    }

}
