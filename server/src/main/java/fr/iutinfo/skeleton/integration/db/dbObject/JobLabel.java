package fr.iutinfo.skeleton.integration.db.dbObject;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db.dbObject
 * Created by aitux on 09/07/18.
 */
public enum JobLabel
{
    AGRICULTURE(1), ART_CULTURE_AUDIOVISUEL(2), ARTISANAT_ART(3), BTP_ARCHITECTURE(4), COMMERCE(5), DEFENSE_PUBLIQUE_SECURITE(6), DROIT_SCIENCES_PO(7), ECO_GESTION_FINANCE(8), HOTELLERIE_RESTAURATION_TOURISME(9),
    INDUSTRIE(10), INFORMATION_COMMUNICATION(11), LETTRES_LANGUES_SCIENCES_HUMAINE(12), MAINTENANCE_ENTRETIEN(13), SANTE_SOCIAL_SOINS_ESTHETIQUES(14), SCIENCES_INFORMATIQUES(15), SPORT(16), TRANSPORT(17), ENSEIGNEMENT(18),
    ADMINISTRATION_SECRETARIAT(19), SANS_PROFESSION(20);

    private final int num;

    JobLabel(int num)
    {
        this.num = num;
    }

    public static JobLabel getJobLabel(String s)
    {
        s = s.toLowerCase();
        switch (s)
        {
            case "agriculture":
                return AGRICULTURE;
            case "art - culture - audiovisuel":
                return ART_CULTURE_AUDIOVISUEL;
            case "artisanat d'art":
                return ARTISANAT_ART;
            case "btp - architecture":
                return BTP_ARCHITECTURE;
            case "commerce":
                return COMMERCE;
            case "défense publique - sécurité":
                return DEFENSE_PUBLIQUE_SECURITE;
            case "droit - sciences politiques":
                return DROIT_SCIENCES_PO;
            case "economie - gestion - finance":
                return ECO_GESTION_FINANCE;
            case "hôtellerie - restauration - tourisme":
                return HOTELLERIE_RESTAURATION_TOURISME;
            case "industrie":
                return INDUSTRIE;
            case "information - communication":
                return INFORMATION_COMMUNICATION;
            case "lettres - langues - sciences humaines":
                return LETTRES_LANGUES_SCIENCES_HUMAINE;
            case "maintenances - entretien":
                return MAINTENANCE_ENTRETIEN;
            case "santé - social - soins esthétiques":
                return SANTE_SOCIAL_SOINS_ESTHETIQUES;
            case "sciences - informatiques":
                return SCIENCES_INFORMATIQUES;
            case "sport":
                return SPORT;
            case "transport":
                return TRANSPORT;
            case "enseignement":
                return ENSEIGNEMENT;
            case "administration - secrétariat":
                return ADMINISTRATION_SECRETARIAT;
            case "sans profession":
                return SANS_PROFESSION;
            default:
                return null;
        }
    }

    public int getValue()
    {
        return this.num;
    }
}
