package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;
import fr.iutinfo.skeleton.integration.csv.CSVObjectFactory;
import fr.iutinfo.skeleton.integration.db.exception.MissingArgumentException;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 09/07/18.
 */
public class App
{
    public static void main(String[] args) throws MissingArgumentException
    {
        File f = new File(args[0]);
        char c = args[1].charAt(0);
        CSVObject csvObject = CSVObjectFactory.createCSVObject(f,c);
        try
        {
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "data.db");
            DBRequester dbRequester = new DBRequesterSMAC(csvObject, connection);
            CSVObject finalObject = dbRequester.requestIdAsName();
            System.out.println(finalObject.toString());

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
