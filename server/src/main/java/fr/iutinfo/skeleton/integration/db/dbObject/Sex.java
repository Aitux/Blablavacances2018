package fr.iutinfo.skeleton.integration.db.dbObject;

import java.lang.reflect.MalformedParametersException;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db.dbObject
 * Created by aitux on 09/07/18.
 */
public enum Sex
{
    HOMME(0), FEMME(1);

    private final int num;

    Sex(int num)
    {
        this.num = num;
    }

    public int getValue()
    {
        return this.num;
    }

    public static Sex getSexFromString(String s)
    {
        if (s.toLowerCase().equals("homme"))
            return HOMME;
        if (s.toLowerCase().equals("femme"))
            return FEMME;
        return null;
    }
}
