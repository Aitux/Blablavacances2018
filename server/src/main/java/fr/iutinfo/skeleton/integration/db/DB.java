package fr.iutinfo.skeleton.integration.db;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 03/07/18.
 */
public interface DB
{
    String rowName();

    String getIdentifier();

    int startingPoint(String identifier);
}
