package fr.iutinfo.skeleton.integration.csv;

import com.opencsv.CSVReader;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutixnfo.skeleton.api
 * Created by aitux on 12/06/18.
 */
public class CSVTool
{
    private static CSVTool ourInstance = new CSVTool();

    private CSVTool()
    {
    }

    public static CSVTool getInstance()
    {
        return ourInstance;
    }

    /**
     * Parse the csv file and returns an Array with all the values.
     *
     * @param file      a csv file
     * @param separator a char
     * @return String[][] a tab containing every single row of the file
     */
    public String[][] readCSV(@NotNull File file, char separator)
    {
        String[][] res = null;
        try
        {
            CSVReader csvReader = new CSVReader(new FileReader(file), separator);
            Iterator<String[]> ite = csvReader.iterator();
            List<String[]> listString = new ArrayList<>();
            while (ite.hasNext())
            {
                listString.add(ite.next());
            }
            res = listString.toArray(new String[listString.size()][listString.get(0).length]);

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return res;
    }


    private Set<String> removeDuplicatesFromStringArray(@NotNull String[][] tab)
    {

        Set<String> stringSet = new HashSet<>();

        for (int i = 0; i < tab.length; i++)
        {
            for (int k = 0; k < tab[1].length; k++)
            {
                if (tab[i][k].contains(","))
                {
                    String[] tmpTab = tab[i][k].split(",");
                    for (String s :
                            tmpTab)
                    {
                        stringSet.add(s);
                    }
                } else
                    stringSet.add(tab[i][k]);
            }
        }
        return stringSet;
    }

    /**
     * Return a list with all the possible unique value in the csv i.e. if some values are found multiple times, they only appear once.
     *
     * @param tab, which is a String[][] every single row that can be found in the csv file.
     * @return a string list
     */
    public List<String> getAllPossibleValue(@NotNull String[][] tab)
    {
        Set<String> stringSet = this.removeDuplicatesFromStringArray(tab);
        String[] strings = stringSet.toArray(new String[stringSet.size()]);

        List<String> str = new ArrayList<>(Arrays.asList(strings));
        Collections.sort(str);
        return str;
    }


}


