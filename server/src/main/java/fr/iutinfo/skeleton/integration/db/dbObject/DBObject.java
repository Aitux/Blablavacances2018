package fr.iutinfo.skeleton.integration.db.dbObject;

import fr.iutinfo.skeleton.integration.csv.CSVObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db.dbObject
 * Created by aitux on 02/07/18.
 */
public class DBObject
{
    private Map<String, String> labelsAndAssociatedValues;

    public DBObject()
    {
        this.labelsAndAssociatedValues = new HashMap<>();
    }

    public void initFromKeyAndValue(String key, String value)
    {
        labelsAndAssociatedValues.put(key, value);
    }

    public Map<String, String> getLabelsAndAssociatedValues()
    {
        return labelsAndAssociatedValues;
    }

    @Override
    public String toString()
    {
        return this.labelsAndAssociatedValues.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBObject object = (DBObject) o;
        return Objects.equals(labelsAndAssociatedValues, object.labelsAndAssociatedValues);
    }

    @Override
    public int hashCode()
    {

        return Objects.hash(labelsAndAssociatedValues);
    }
}
