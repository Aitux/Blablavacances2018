package fr.iutinfo.skeleton.integration.db;

import fr.iutinfo.skeleton.integration.csv.CSVObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db
 * Created by aitux on 27/06/18.
 */
public class DBRequesterSMAC extends DBRequester
{
    public DBRequesterSMAC(CSVObject object, Connection connection)
    {
        super(object, connection);
    }

    @Override
    public PreparedStatement setUpPreparedStatement(String param1, String param2)
    {
        PreparedStatement stmt;
        try
        {
            System.out.println(param1 + " / " + param2);
            stmt = getConnection().prepareStatement("SELECT " + rowName() + " FROM " + param1 + " WHERE id_" + param1 + " = ?");
            stmt.setInt(1, Integer.parseInt(param2));
            return stmt;
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String rowName()
    {
        return "libelle";
    }

    @Override
    public String getIdentifier()
    {
        return "preference";
    }

    @Override
    public int getSubstringStart()
    {
        return 13;
    }


}
