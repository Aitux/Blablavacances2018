package fr.iutinfo.skeleton.integration.db.exception;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.integration.db.exception
 * Created by aitux on 09/07/18.
 */
public class MissingArgumentException extends Exception
{
}
