package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface HolidayMakerFeatureValueDao
{

    @SqlUpdate("create table HolidayMakerFeatureValue(" +
            "holidayMakerFeatureValue text PRIMARY KEY)")
    void createTable();

    @SqlUpdate("insert into HolidayMakerFeatureValue(holidayMakerFeatureValue) values (:holidayMakerFeatureValue)")
    int insert(@BindBean() HolidayMakerFeatureValue holidayMakerFeatureValue);

    @SqlQuery("select * from HolidayMakerFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HolidayMakerFeatureValue> all();

    @SqlQuery("select * from HolidayMakerFeatureValue where holidayMakerFeatureValue = :holidayMakerFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    HolidayMakerFeatureValue findByValue(@Bind("holidayMakerFeatureValue") String holidayMakerFeatureValue);

    void close();
}
