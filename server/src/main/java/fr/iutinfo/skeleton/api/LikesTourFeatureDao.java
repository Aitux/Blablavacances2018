package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.beans.beancontext.BeanContext;
import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface LikesTourFeatureDao
{
    @SqlUpdate("create table likesTourFeature(" +
            "attractionTourFeature integer," +
            "featureTName text ON DELETE CASCADE" +
            "tourFeatureValue text ON DELETE CASCADE," +
            "idHolidayMaker integer ON DELETE CASCADE," +
            "constraint fk_featureTNameLikesTourFeature foreign key(featureTName) references TourFeature(featureTName)," +
            "constraint fk_tourFeatureValueLikesTourFeature foreign key(tourFeatureValue) references TourFeatureValue(tourFeatureValue)," +
            "constraint fk_idHolidayMakerLikesTourFeature foreign key(idHolidayMaker) references HolidayMaker(idHolidayMaker)," +
            "constraint pk_likesTourFeature PRIMARY KEY(featureTName, tourFeatureValue, idHolidayMaker)" +
            ")")
    void createTable();

    @SqlUpdate("insert into likesTourFeature(attractionTourFeature, featureTName, tourFeatureValue, idHoliday) values (:attractionTourFeature, :featureTName, :tourFeatureValue, :idHolidayMaker))")
    int insert(@BindBean() LikesTourFeature likesTourFeature);

    @SqlQuery("select * from likesTourFeature")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesTourFeature> all();

    @SqlQuery("select * from likesTourFeature where idHolidayMaker = :idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesTourFeature> findByHolidayMakerId(@Bind("idHolidayMaker") int idHolidayMaker);

    @SqlQuery("select * from likesTourFeature where featureTName = :featureTName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesTourFeature> findByFeatureTName(@Bind("featureTName") String featureTName);

    @SqlQuery("select * from likesTourFeature where tourFeatureValue = :tourFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesTourFeature> findByTourFeatureValue(@Bind("tourFeatureValue") String tourFeatureValue);

    
}
