package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DestinationDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Destination
{
    private int id_destination;
    private String libelle;

    public Destination(){
    }

    public Destination(String libelle){
        this.libelle = libelle;
    }


    public int getId_destination()
    {
        return id_destination;
    }

    public void setId_destination(int id_destination)
    {
        this.id_destination = id_destination;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(DestinationDto dto){
        this.setId_destination(dto.getId_destination());
        this.setLibelle(dto.getLibelle());
    }

    public DestinationDto convertToDto(){
        DestinationDto dto = new DestinationDto();
        dto.setId_destination(this.getId_destination());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
