package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface HolidayMakerFeatureDao
{
    @SqlUpdate("create table HolidayMakerFeature(" +
            "featureHName text PRIMARY KEY)")
    void createTable();

    @SqlUpdate("insert into HolidayMakerFeature(featureHName) values (:featureHName)")
    int insert(@BindBean() HolidayMakerFeature holidayMakerFeature);

    @SqlQuery("select * from HolidayMakerFeature")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HolidayMakerFeature> all();

    @SqlQuery("select * from HolidayMakerFeature where featureHName = :featureHName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    HolidayMakerFeature findByFeatureHName(@Bind("featureHName") String featureHName);

    void close();
}
