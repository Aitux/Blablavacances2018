package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 07/05/18.
 */
public class ParticipeResource
{

    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(ParticipeResource.class);
    private static ParticipeDao dao = getDbi().open(ParticipeDao.class);

    public ParticipeResource() throws SQLException
    {
        if(!tableExist("participe")){
            logger.debug("Create table participe");
            dao.createTablePartcipe();
        }
    }

    @POST
    public Response insert(Participe participe){
        int s = dao.insert(participe);
        if(s == 0)
        return Response.notModified().build();
       else
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<Participe> participes = dao.all();
        GenericEntity<List<Participe>> entity = new GenericEntity<List<Participe>>(participes){};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response getParticipeByUserId(@PathParam("id_user") int id_user){
        List<Participe> participes = dao.getByUserId(id_user);
        GenericEntity<List<Participe>> entity = new GenericEntity<List<Participe>>(participes){};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response getParticipeByVoyageId(@PathParam("id_voyage") int id_voyage){
        List<Participe> participes = dao.getByVoyageId(id_voyage);
        GenericEntity<List<Participe>> entity = new GenericEntity<List<Participe>>(participes){};
        return Response.ok(entity).build();
    }

    @DELETE
    public Response delete(Participe participe){
        int s = dao.delete(participe);
        if(s == 0){
            return Response.notModified().build();
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("/delete/user/{id_user}")
    public Response deleteByUserId(@PathParam("id_user") int id_user){
        int s = dao.deleteUser(id_user);
        if(s == 0){
            return Response.notModified().build();
        }
        return Response.ok().build();
    }

    @DELETE
    @Path("/delete/voyage/{id_voyage}")
    public Response deleteByVoyageId(@PathParam("id_voyage") int id_voyage){
        int s = dao.deleteVoyage(id_voyage);
        if(s == 0){
            return Response.notModified().build();
        }
        return Response.ok().build();
    }
}
