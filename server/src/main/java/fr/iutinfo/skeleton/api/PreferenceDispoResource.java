package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceDispo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceDispoResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceDispoResource.class);
    private static PreferenceDispoDao dao = getDbi().open(PreferenceDispoDao.class);

    public PreferenceDispoResource() throws SQLException
    {
        if (!tableExist("preferenceDispo"))
        {
            logger.debug("Create table preferenceDispo");
            dao.createPreferenceDispo();
        }
    }

    @POST
    public Response insert(PreferenceDispo preferenceDispo){
        int s =  dao.insert(preferenceDispo);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceDispo> preferenceDispoList = dao.all();
        GenericEntity<List<PreferenceDispo>> entity = new GenericEntity<List<PreferenceDispo>>(preferenceDispoList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/dispo/{id}")
    public Response findByIdDispo(@PathParam("id") int id){
        return Response.ok(dao.findByIdDispo(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceDispo preferenceDispo){
        preferenceDispo.setId_user(id_user);
        dao.update(preferenceDispo);
        return Response.ok().build();
    }

}
