package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface LikesPartnerDao
{
    @SqlUpdate("create table likesPartner(" +
            "attractionPartnerFeature float," +
            "holidayMakerFeatureValue text ON DELETE CASCADE," +
            "idHolidayMaker integer ON DELETE CASCADE," +
            "featureHName text ON DELETE CASCADE," +
            "constraint fk_holidayMakerFeatureValueLikesPartner(holidayMakerFeatureValue) references HolidayMakerFeatureValue(holidayMakerFeatureValue)," +
            "constraint fk_idHolidayMakerLikesPartner(idHolidayMaker) references HolidayMaker(idHolidayMaker)," +
            "constraint fk_featureHNameLikesPartner(featureHName) references HolidayMakerFeature(featureHName)," +
            "constraint pk_likesPartner PRIMARY KEY(holidayMakerFeatureValue, idHolidayMaker, featureHName))")
    void createTable();

    @SqlUpdate("insert into likesPartner(attractionPartnerFeature, holidayMakerFeature, idHolidayMaker, featureHName) values(:attractionPartnerFeature, :holidayMakerFeature, :idHolidayMaker, :featureHName)")
    int insert(@BindBean() LikesPartner likesPartner);

    @SqlQuery("select * from likesPartner")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesPartner> all();

    @SqlQuery("select * from likesPartner where idHolidayMaker=:idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesPartner> findByIdHolidayMaker(@Bind("idHolidayMaker") int idHolidayMaker);

    @SqlQuery("select * from likesPartner where featureHName=:featureHName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesPartner> findByFeature(@Bind("featureHName") String featureHName);

    @SqlQuery("select * from likesPartner where holidayMakerFeatureValue=:holidayMakerFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<LikesPartner> findByHolidayMakerFeatureValue(@Bind("holidayMakerFeatureValue") String holidayMakerFeatureValue);

    void close();
}
