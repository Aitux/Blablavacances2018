package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/describesTour")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DescribesTourResource
{

    final static Logger logger = LoggerFactory.getLogger(DescribesTourResource.class);
    private static DescribesTourDao dao = getDbi().open(DescribesTourDao.class);

    @Context
    UriInfo uriInfo;

    public DescribesTourResource() throws SQLException
    {
        if (!tableExist("describesTour"))
        {
            logger.debug("Create table describesTour");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<DescribesTour> list = dao.all();
        GenericEntity<List<DescribesTour>> entity = new GenericEntity<List<DescribesTour>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(DescribesTour describesTour)
    {
        int i = dao.insert(describesTour);
        if (i == 0)
            return Response.notModified().build();
        else
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{idTour}")
    public Response findById(@PathParam("idTour") int idTour){
        List<DescribesTour> list = dao.findById(idTour);
        GenericEntity<List<DescribesTour>> entity = new GenericEntity<List<DescribesTour>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/feature/{featureTName}")
    public Response findByFeatureTName(@PathParam("featureTName") String featureTName){
        List<DescribesTour> list = dao.findByFeatureName(featureTName);
        GenericEntity<List<DescribesTour>> entity = new GenericEntity<List<DescribesTour>>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
