package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.VoyageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/voyage")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class VoyageResource
{
    final static Logger logger = LoggerFactory.getLogger(VoyageResource.class);
    private static VoyageDao dao = getDbi().open(VoyageDao.class);

    // LE NOMBRE DE VOYAGE QUE PEUT CREER UN UTILISATEUR
    private final int LIMITE = 1;
    ////////////////////////////////////////////////////

    @Context
    public UriInfo uriInfo;

    public VoyageResource() throws SQLException
    {
        if (!tableExist("voyage"))
        {
            logger.debug("Create table voyage");
            dao.createTableVoyage();
        }
    }

    @GET
    public Response getAllVoyage()
    {
        List<Voyage> voyageList;
        voyageList = dao.all();
        GenericEntity<List<Voyage>> genericEntity = new GenericEntity<List<Voyage>>(voyageList){};
        return Response.ok(genericEntity).build();
    }

    @POST
    public Response insert(Voyage voyage)
    {
        int id = dao.insert(voyage);
        voyage.setId_voyage(id);
        URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + voyage.getId_voyage()).build();
        return Response.created(instanceURI).build();
    }

    @GET
    @Path("/{id_createur}")
    public Response findByCreateur(@PathParam("id_createur") int id_createur)
    {
        List<Voyage> a = dao.findByCreateur(id_createur);
        GenericEntity<List<Voyage>> b = new GenericEntity<List<Voyage>>(a){};
        if(a.isEmpty())
            return Response.noContent().build();
         else
             return Response.ok(b).build();
    }

    @DELETE
    @Path("/{id_voyage}")
    public Response delete(@PathParam("id_voyage") int id_voyage){
        int nb = dao.delete(id_voyage);
        if(nb != 0)
            return Response.ok().build();
        else
            return Response.status(Response.Status.NOT_MODIFIED).build();
    }

    @PUT
    @Path("/{id_voyage}")
    public Response update(@PathParam("id_voyage") int id,Voyage voyage){
        voyage.setId_voyage(id);
        int nb = dao.update(voyage);
        if(nb != 0)
            return Response.ok().build();
        else return Response.status(Response.Status.NOT_MODIFIED).build();
    }

    @GET
    @Path("/id/{id_voyage}")
    public Response findById(@PathParam("id_voyage") int id)
    {
        Voyage a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }


    @GET
    @Path("/limite/{id_createur}")
    public Response getNumberVoyageCreated(@PathParam("id_createur") int id_createur)
    {
        List<Voyage> a = dao.findByCreateur(id_createur);
        if (a.size() >= LIMITE)
            return Response.status(Response.Status.CONFLICT).build();
        else
            return Response.ok().build();
    }


}
