package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface TourDao
{
    @SqlUpdate("create table Tour(" +
            "idTour autoincrement integer PRIMARY KEY" +
            ")")
    void createTable();

    @SqlUpdate("insert into Tour(idTour) values ('')")
    @GetGeneratedKeys
    int insert();

    @SqlQuery("select * from Tour")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Tour> all();

    @SqlQuery("select * from Tour where idTour = :idTour")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Tour findByTourId(@Bind("idTour") int idTour);


}
