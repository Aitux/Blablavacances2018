package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/likesPartner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LikesPartnerResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.LikesPartnerResource.class);
    private static LikesPartnerDao dao = getDbi().open(LikesPartnerDao.class);


    @Context
    UriInfo uriInfo;

    public LikesPartnerResource() throws SQLException
    {
        if (!tableExist("likesPartner"))
        {
            logger.debug("Create LikesPartner table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<LikesPartner> list = dao.all();
        GenericEntity<List<LikesPartner>> entity = new GenericEntity<List<LikesPartner>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(LikesPartner likesPartner)
    {
        int i = dao.insert(likesPartner);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{idHolidayMaker}")
    public Response findByIdHolidayMakerId(@PathParam("idHolidayMaker") int idHolidayMaker)
    {
        List<LikesPartner> i = dao.findByIdHolidayMaker(idHolidayMaker);
        GenericEntity<List<LikesPartner>> entity = new GenericEntity<List<LikesPartner>>(i){};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/feature/{featureHName}")
    public Response findByFeatureHName(@PathParam("featureHName") String featureHName){
        List<LikesPartner> i = dao.findByFeature(featureHName);
        GenericEntity<List<LikesPartner>> entity = new GenericEntity<List<LikesPartner>>(i){};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/holidayMakerFeatureValue/{holidayMakerFeatureValue}")
    public Response findByHolidayMakerFeatureValue(@PathParam("holidayMakerFeatureValue") String holidayMakerFeatureValue){
        List<LikesPartner> i = dao.findByHolidayMakerFeatureValue(holidayMakerFeatureValue);
        GenericEntity<List<LikesPartner>> entity = new GenericEntity<List<LikesPartner>>(i){};
        return Response.ok(entity).build();
    }
}
