package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class ComputedPreferenceTour
{
    private int score;
    private int idTour;
    private int idHolidayMaker;

    public ComputedPreferenceTour(int score, int idTour, int idHolidayMaker)
    {
        this.score = score;
        this.idTour = idTour;
        this.idHolidayMaker = idHolidayMaker;
    }

    public ComputedPreferenceTour(){
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    public int getIdTour()
    {
        return idTour;
    }

    public void setIdTour(int idTour)
    {
        this.idTour = idTour;
    }

    public int getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }
}
