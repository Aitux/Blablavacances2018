package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceProfessionDao
{


    @SqlUpdate("create table preferenceProfession(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_profession foreign key(id) references profession(id_profession)," +
            "constraint pk_profession primary key (id_user, id_voyage, id))")
    void createPreferenceProfession();

    @SqlUpdate("insert into preferenceProfession(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceProfession preferenceProfession);

    @SqlQuery("select * from preferenceProfession")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceProfession> all();

    @SqlQuery("select * from preferenceProfession where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceProfession findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceProfession where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceProfession findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceProfession where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceProfession findByIdProfession(@Bind("id") int id);

    @SqlUpdate("update preferenceProfession set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceProfession preferenceProfession);

    @SqlUpdate("delete from preferenceProfession where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
