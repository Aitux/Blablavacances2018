package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface UserDao
{
    @SqlUpdate("create table users (id integer primary key autoincrement, name varchar(100), alias varchar(100), email varchar(100), passwdHash varchar(64)," +
            " salt varchar(64), search varchar(1024), tel varchar(32), adresse varchar(1024), prenom varchar(100), anneenaissance integer, sexe varchar(5), niveauEtude integer, profession varchar(40), fumeur integer)")
    void createUserTable();

    @SqlUpdate("insert into users (name, alias, email, passwdHash, salt, search, tel, adresse, prenom, anneenaissance, sexe, fumeur) "
            + "values (:name, :alias, :email, :passwdHash, :salt, :search, :tel, :adresse, :prenom, :anneenaissance, :sexe, :fumeur)")
    @GetGeneratedKeys
    int insert(@BindBean() User user);

    @SqlQuery("select * from users where name = :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findByName(@Bind("name") String name);

    @SqlQuery("select * from users where alias = :alias")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findByAlias(@Bind("alias") String alias);

    @SqlQuery("select * from users where search like :name")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<User> search(@Bind("name") String name);

    @SqlUpdate("update users set name = :name, alias = :alias, email = :email, passwdHash = :passwdHash, salt = :salt, search = :search, tel = :tel," +
            " adresse = :adresse, prenom = :prenom, anneenaissance = :anneenaissance, sexe = :sexe where id = :id")
    void update(@Bind("id") int id, @BindBean() User user);

    @SqlUpdate("update users set niveauEtude = :niveauEtude, profession = :profession, fumeur=:fumeur where id = :id")
    void updateEtuProf(@BindBean() User user);

    @SqlUpdate("drop table if exists users")
    void dropUserTable();

    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);

    @SqlQuery("select * from users order by id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<User> all();

    @SqlQuery("select * from users where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    User findById(@Bind("id") int id);

    void close();

}
