package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public class UserInteretSelf
{
    private int id_user;
    private int id;
    private int value;

    public UserInteretSelf()
    {
    }

    public UserInteretSelf(int id_user, int id, int value)
    {
        this.id_user = id_user;
        this.id = id;
        this.value = value;
    }


    public int getId_user()
    {
        return id_user;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }


    @Override
    public String toString()
    {
        return "UserInteretSelf{" +
                "id_user=" + id_user +
                ", id=" + id +
                ", value=" + value +
                '}';
    }


    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }


}
