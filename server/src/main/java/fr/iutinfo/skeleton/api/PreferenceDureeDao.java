package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceDureeDao
{


    @SqlUpdate("create table preferenceDuree(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_duree foreign key(id) references duree(id_duree)," +
            "constraint pk_duree primary key (id_user, id_voyage, id))")
    void createPreferenceDuree();

    @SqlUpdate("insert into preferenceDuree(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceDuree preferenceDuree);

    @SqlQuery("select * from preferenceDuree")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceDuree> all();

    @SqlQuery("select * from preferenceDuree where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDuree findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceDuree where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDuree findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceDuree where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDuree findByIdDuree(@Bind("id") int id);

    @SqlUpdate("update preferenceDuree set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceDuree preferenceDuree);

    @SqlUpdate("delete from preferenceDuree where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
