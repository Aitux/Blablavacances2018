package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ExpeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/expe")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ExpeResource
{
    final static Logger logger = LoggerFactory.getLogger(ExpeResource.class);
    private static ExpeDao dao = getDbi().open(ExpeDao.class);

    @Context
    public UriInfo uriInfo;

    public ExpeResource() throws SQLException
    {
        if (!tableExist("expe"))
        {
            logger.debug("Create table expe");
            dao.createExpeTable();
        }
    }

    @GET
    public List<ExpeDto> getAllExpe(){
        List<Expe> expeList;
        expeList = dao.all();
        return expeList.stream().map(Expe::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Expe expe){
        if(dao.all().contains(expe)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(expe);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(expe.getLibelle()).getId_expe()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_expe}")
    public Response findById(@PathParam("id_expe") int id){
        Expe a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Expe a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

