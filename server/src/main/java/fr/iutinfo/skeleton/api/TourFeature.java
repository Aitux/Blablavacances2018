package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class TourFeature
{
    private String featureTName;

    public TourFeature(String featureTName)
    {
        this.featureTName = featureTName;
    }

    public TourFeature(){}

    public String getFeatureTName()
    {
        return featureTName;
    }

    public void setFeatureTName(String featureTName)
    {
        this.featureTName = featureTName;
    }
}
