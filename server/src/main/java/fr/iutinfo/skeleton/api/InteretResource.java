package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.InteretDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/interet")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class InteretResource
{
    final static Logger logger = LoggerFactory.getLogger(InteretResource.class);
    private static InteretDao dao = getDbi().open(InteretDao.class);

    @Context
    public UriInfo uriInfo;

    public InteretResource() throws SQLException
    {
        if (!tableExist("interet"))
        {
            logger.debug("Create table interet");
            dao.createInteretTable();
        }
    }

    @GET
    public List<InteretDto> getAllInteret(){
        List<Interet> interetList;
        interetList = dao.all();
        return interetList.stream().map(Interet::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Interet interet){
        if(dao.all().contains(interet)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(interet);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(interet.getLibelle()).getId_interet()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_interet}")
    public Response findById(@PathParam("id_interet") int id){
        Interet a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Interet a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

