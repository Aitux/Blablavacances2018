package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceTransportToDao
{


    @SqlUpdate("create table preferenceTransportTo(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_transportTo foreign key(id) references transportTo(id_transportTo)," +
            "constraint pk_transportTo primary key (id_user, id_voyage, id))")
    void createPreferenceTransportTo();

    @SqlUpdate("insert into preferenceTransportTo(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceTransportTo preferenceTransportTo);

    @SqlQuery("select * from preferenceTransportTo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceTransportTo> all();

    @SqlQuery("select * from preferenceTransportTo where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransportTo findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceTransportTo where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransportTo findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceTransportTo where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransportTo findByIdTransportTo(@Bind("id") int id);

    @SqlUpdate("update preferenceTransportTo set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceTransportTo preferenceTransportTo);

    @SqlUpdate("delete from preferenceTransportTo where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
