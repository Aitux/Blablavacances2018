package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface ExpeDao
{
    @SqlUpdate("create table expe (id_expe integer primary key autoincrement, libelle varchar(50))")
    void createExpeTable();

    @SqlUpdate("insert into expe (libelle) values (:libelle)")
    int insert (@BindBean Expe expe);

    @SqlQuery("select * from expe")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Expe> all();

    @SqlUpdate("update expe set libelle = :libelle where id_expe = :id_expe")
    void update(@BindBean Expe expe);

    @SqlQuery("select * from expe where id_expe = :id_expe")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Expe findById(@Bind("id_expe") int id_expe);

    @SqlQuery("select * from expe where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Expe findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from expe where id_expe = :id_expe")
    void delete(@Bind("id_expe") int id_expe);

    @SqlUpdate("drop table if exists expe")
    void dropExpeTable();

    void close();
}
