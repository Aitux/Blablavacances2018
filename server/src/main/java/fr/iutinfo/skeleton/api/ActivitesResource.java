package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ActivitesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@Path("/activite")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActivitesResource
{
    final static Logger logger = LoggerFactory.getLogger(ActivitesResource.class);
    private static ActivitesDao dao = getDbi().open(ActivitesDao.class);

    @Context
    public UriInfo uriInfo;

    public ActivitesResource() throws SQLException
    {
        if (!tableExist("activite"))
        {
            logger.debug("Create table activites");
            dao.createActivitesTable();
        }
    }

    @GET
    public List<ActivitesDto> getAllActivites()
    {
        List<Activites> activitesList;
        activitesList = dao.all();
        return activitesList.stream().map(Activites::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Activites activites)
    {
        if (dao.all().contains(activites))
        {
            return Response.status(Response.Status.CONFLICT).build();
        } else
        {
            dao.insert(activites);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(activites.getLibelle()).getId_activite()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_activite}")
    public Response findById(@PathParam("id_activite") int id)
    {
        Activites a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle)
    {
        Activites a = dao.findByLibelle(libelle);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}
