package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceEnvironnementDao
{


    @SqlUpdate("create table preferenceEnvironnement(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_environnement foreign key(id) references environnement(id_environnement)," +
            "constraint pk_environnement primary key (id_user, id_voyage, id))")
    void createPreferenceEnvironnement();

    @SqlUpdate("insert into preferenceEnvironnement(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceEnvironnement preferenceEnvironnement);

    @SqlQuery("select * from preferenceEnvironnement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceEnvironnement> all();

    @SqlQuery("select * from preferenceEnvironnement where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceEnvironnement findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceEnvironnement where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceEnvironnement findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceEnvironnement where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceEnvironnement findByIdEnvironnement(@Bind("id") int id);

    @SqlUpdate("update preferenceEnvironnement set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceEnvironnement preferenceEnvironnement);

    @SqlUpdate("delete from preferenceenvironnement where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
