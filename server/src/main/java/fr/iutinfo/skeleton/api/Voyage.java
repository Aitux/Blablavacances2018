package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.VoyageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Voyage
{
    final static Logger logger = LoggerFactory.getLogger(Voyage.class);

    private int id_voyage;
    private int id_createur;
    private int budget;
    private int route_seul;
    private String nb_co_vac;
    private int sexe_co_vac;
    private String age_co_vac;
    private String etude_co_vac;
    private String description;
    private int fumeur_co_vac;
    private int animal_de_compagnie_co_vac;

    public Voyage()
    {
    }

    public Voyage(int id_voyage, int id_createur, int budget, int route_seul, String nb_co_vac, int sexe_co_vac, String age_co_vac, String etude_co_vac, int fumeur_co_vac, int animal_de_compagnie_co_vac, String description)
    {
        this.description = description;
        this.id_voyage = id_voyage;
        this.id_createur = id_createur;
        this.budget = budget;
        this.route_seul = route_seul;
        this.nb_co_vac = nb_co_vac;
        this.sexe_co_vac = sexe_co_vac;
        this.age_co_vac = age_co_vac;
        this.etude_co_vac = etude_co_vac;
        this.fumeur_co_vac = fumeur_co_vac;
        this.animal_de_compagnie_co_vac = animal_de_compagnie_co_vac;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getNb_co_vac()
    {
        return nb_co_vac;
    }

    public void setNb_co_vac(String nb_co_vac)
    {
        this.nb_co_vac = nb_co_vac;
    }

    public int getSexe_co_vac()
    {
        return sexe_co_vac;
    }

    public void setSexe_co_vac(int sexe_co_vac)
    {
        this.sexe_co_vac = sexe_co_vac;
    }

    public String getAge_co_vac()
    {
        return age_co_vac;
    }

    public void setAge_co_vac(String age_co_vac)
    {
        this.age_co_vac = age_co_vac;
    }

    public String getEtude_co_vac()
    {
        return etude_co_vac;
    }

    public void setEtude_co_vac(String etude_co_vac)
    {
        this.etude_co_vac = etude_co_vac;
    }

    public int getFumeur_co_vac()
    {
        return fumeur_co_vac;
    }

    public void setFumeur_co_vac(int fumeur_co_vac)
    {
        this.fumeur_co_vac = fumeur_co_vac;
    }

    public int getAnimal_de_compagnie_co_vac()
    {
        return animal_de_compagnie_co_vac;
    }

    public void setAnimal_de_compagnie_co_vac(int animal_de_compagnie_co_vac)
    {
        this.animal_de_compagnie_co_vac = animal_de_compagnie_co_vac;
    }

    public int getRoute_seul()
    {
        return route_seul;
    }

    public void setRoute_seul(int route_seul)
    {
        this.route_seul = route_seul;
    }

    public int getId_createur()
    {
        return id_createur;
    }

    public void setId_createur(int id_createur)
    {
        this.id_createur = id_createur;
    }

    public int getId_voyage()
    {
        return id_voyage;
    }

    public void setId_voyage(int id_voyage)
    {
        this.id_voyage = id_voyage;
    }

    public int getBudget()
    {
        return budget;
    }

    public void setBudget(int budget)
    {
        this.budget = budget;
    }

    public void initFromDto(VoyageDto dto)
    {
        this.setDescription(dto.getDescription());
        this.setId_voyage(dto.getId_voyage());
        this.setBudget(dto.getBudget());
        this.setRoute_seul(dto.getRoute_seul());
        this.setAge_co_vac(dto.getAge_co_vac());
        this.setAnimal_de_compagnie_co_vac(dto.getAnimal_de_compagnie_co_vac());
        this.setEtude_co_vac(dto.getEtude_co_vac());
        this.setFumeur_co_vac(dto.getFumeur_co_vac());
        this.setId_createur(dto.getId_createur());
        this.setNb_co_vac(dto.getNb_co_vac());
        this.setSexe_co_vac(dto.getSexe_co_vac());
    }

    public VoyageDto convertToDto()
    {
        VoyageDto dto = new VoyageDto();
        dto.setDescription(getDescription());
        dto.setId_voyage(getId_voyage());
        dto.setBudget(getBudget());
        dto.setRoute_seul(getRoute_seul());
        dto.setId_createur(getId_createur());
        dto.setAge_co_vac(getAge_co_vac());
        dto.setAnimal_de_compagnie_co_vac(getAnimal_de_compagnie_co_vac());
        dto.setEtude_co_vac(getEtude_co_vac());
        dto.setFumeur_co_vac(getFumeur_co_vac());
        dto.setNb_co_vac(getNb_co_vac());
        dto.setSexe_co_vac(getSexe_co_vac());
        return dto;
    }
}
