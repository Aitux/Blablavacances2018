package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/tourFeature")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TourFeatureResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.TourFeatureResource.class);
    private static TourFeatureDao dao = getDbi().open(TourFeatureDao.class);


    @Context
    UriInfo uriInfo;

    public TourFeatureResource() throws SQLException
    {
        if (!tableExist("TourFeature"))
        {
            logger.debug("Create TourFeature table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<TourFeature> list = dao.all();
        GenericEntity<List<TourFeature>> entity = new GenericEntity<List<TourFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(TourFeature featureTName)
    {
        int i = dao.insert(featureTName);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{featureTName}")
    public Response findByHolidayMaker(@PathParam("featureTName") String featureTName)
    {
        TourFeature list = dao.findByFeatureTName(featureTName);
        GenericEntity<TourFeature> entity = new GenericEntity<TourFeature>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
