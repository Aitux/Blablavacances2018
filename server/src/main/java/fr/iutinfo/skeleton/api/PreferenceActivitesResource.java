package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceActivite")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceActivitesResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceActivitesResource.class);
    private static PreferenceActivitesDao dao = getDbi().open(PreferenceActivitesDao.class);

    public PreferenceActivitesResource() throws SQLException
    {
        if (!tableExist("preferenceActivite"))
        {
            logger.debug("Create table preferenceActivite");
            dao.createPreferenceActivite();
        }
    }

    @POST
    public Response insert(PreferenceActivites preferenceActivites){
       int s =  dao.insert(preferenceActivites);
       if(s == 0)
           return Response.notModified().build();
       else
           return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceActivites> preferenceActivitesList = dao.all();
        GenericEntity<List<PreferenceActivites>> entity = new GenericEntity<List<PreferenceActivites>>(preferenceActivitesList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/activite/{id_activite}")
    public Response findByIdActivite(@PathParam("id_activite") int id_activite){
        return Response.ok(dao.findByIdActivite(id_activite)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceActivites preferenceActivites){
        preferenceActivites.setId_user(id_user);
        dao.update(preferenceActivites);
        return Response.ok().build();
    }

}
