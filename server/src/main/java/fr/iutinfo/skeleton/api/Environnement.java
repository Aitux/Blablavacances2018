package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.EnvironnementDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Environnement
{
    private int id_environnement;
    private String libelle;

    public Environnement(){
    }

    public Environnement(String libelle){
        this.libelle = libelle;
    }


    public int getId_environnement()
    {
        return id_environnement;
    }

    public void setId_environnement(int id_environnement)
    {
        this.id_environnement = id_environnement;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(EnvironnementDto dto){
        this.setId_environnement(dto.getId_environnement());
        this.setLibelle(dto.getLibelle());
    }

    public EnvironnementDto convertToDto(){
        EnvironnementDto dto = new EnvironnementDto();
        dto.setId_environnement(this.getId_environnement());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
