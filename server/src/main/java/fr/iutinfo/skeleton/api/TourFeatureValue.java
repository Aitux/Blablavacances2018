package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class TourFeatureValue
{
    private String tourFeatureValue;

    public TourFeatureValue(String tourFeatureValue)
    {
        this.tourFeatureValue = tourFeatureValue;
    }

    public TourFeatureValue(){}

    public String getTourFeatureValue()
    {
        return tourFeatureValue;
    }

    public void setTourFeatureValue(String tourFeatureValue)
    {
        this.tourFeatureValue = tourFeatureValue;
    }
}
