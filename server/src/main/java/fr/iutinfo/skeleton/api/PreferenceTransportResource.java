package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceTransport")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceTransportResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceTransportResource.class);
    private static PreferenceTransportDao dao = getDbi().open(PreferenceTransportDao.class);

    public PreferenceTransportResource() throws SQLException
    {
        if (!tableExist("preferenceTransport"))
        {
            logger.debug("Create table preferenceTransport");
            dao.createPreferenceTransport();
        }
    }

    @POST
    public Response insert(PreferenceTransport preferenceTransport){
        int s =  dao.insert(preferenceTransport);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceTransport> preferenceTransportList = dao.all();
        GenericEntity<List<PreferenceTransport>> entity = new GenericEntity<List<PreferenceTransport>>(preferenceTransportList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/transport/{id}")
    public Response findByIdTransport(@PathParam("id") int id){
        return Response.ok(dao.findByIdTransport(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceTransport preferenceTransport){
        preferenceTransport.setId_user(id_user);
        dao.update(preferenceTransport);
        return Response.ok().build();
    }

}
