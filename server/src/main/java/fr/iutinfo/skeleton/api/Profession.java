package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ProfessionDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Profession
{
    private int id_profession;
    private String libelle;

    public Profession(){
    }

    public Profession(String libelle){
        this.libelle = libelle;
    }


    public int getId_profession()
    {
        return id_profession;
    }

    public void setId_profession(int id_profession)
    {
        this.id_profession = id_profession;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(ProfessionDto dto){
        this.setId_profession(dto.getId_profession());
        this.setLibelle(dto.getLibelle());
    }

    public ProfessionDto convertToDto(){
        ProfessionDto dto = new ProfessionDto();
        dto.setId_profession(this.getId_profession());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
