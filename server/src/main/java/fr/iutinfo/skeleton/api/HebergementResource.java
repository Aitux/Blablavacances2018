package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.HebergementDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/hebergement")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HebergementResource
{
    final static Logger logger = LoggerFactory.getLogger(HebergementResource.class);
    private static HebergementDao dao = getDbi().open(HebergementDao.class);

    @Context
    public UriInfo uriInfo;

    public HebergementResource() throws SQLException
    {
        if (!tableExist("hebergement"))
        {
            logger.debug("Create table hebergement");
            dao.createHebergementTable();
        }
    }

    @GET
    public List<HebergementDto> getAllHebergement(){
        List<Hebergement> hebergementList;
        hebergementList = dao.all();
        return hebergementList.stream().map(Hebergement::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Hebergement hebergement){
        if(dao.all().contains(hebergement)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(hebergement);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(hebergement.getLibelle()).getId_hebergement()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_hebergement}")
    public Response findById(@PathParam("id_hebergement") int id){
        Hebergement a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Hebergement a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

