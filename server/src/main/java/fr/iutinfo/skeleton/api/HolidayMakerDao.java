package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface HolidayMakerDao
{
    @SqlUpdate("create table HolidayMaker(" +
            "idHolidayMaker autoincrement integer PRIMARY KEY)")
    void createTable();

    @SqlUpdate("insert into HolidayMaker(idHolidayMaker) values ('')")
    @GetGeneratedKeys
    int insert();

    @SqlQuery("select * from HolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HolidayMaker> all();

    @SqlQuery("select * from HolidayMaker where idHolidayMaker=:idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    HolidayMaker findById(@Bind("idHolidayMaker") int idHolidayMaker);

    void close();

}
