package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/holidayMakerFeature")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HolidayMakerFeatureResource
{

    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.HolidayMakerFeatureResource.class);
    private static HolidayMakerFeatureDao dao = getDbi().open(HolidayMakerFeatureDao.class);


    @Context
    UriInfo uriInfo;

    public HolidayMakerFeatureResource() throws SQLException
    {
        if (!tableExist("HolidayMakerFeature"))
        {
            logger.debug("Create HolidayMakerFeature table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<HolidayMakerFeature> list = dao.all();
        GenericEntity<List<HolidayMakerFeature>> entity = new GenericEntity<List<HolidayMakerFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(HolidayMakerFeature featureHName)
    {
        int i = dao.insert(featureHName);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{featureHName}")
    public Response findByFeatureHName(@PathParam("featureHName") String featureHName)
    {
        HolidayMakerFeature list = dao.findByFeatureHName(featureHName);
        GenericEntity<HolidayMakerFeature> entity = new GenericEntity<HolidayMakerFeature>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
