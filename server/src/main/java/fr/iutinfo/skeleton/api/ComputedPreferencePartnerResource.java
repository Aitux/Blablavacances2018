package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/computedPreferencePartner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ComputedPreferencePartnerResource
{

    final static Logger logger = LoggerFactory.getLogger(ComputedPreferencePartnerResource.class);
    private static ComputedPreferencePartnerDao dao = getDbi().open(ComputedPreferencePartnerDao.class);


    @Context
    UriInfo uriInfo;

    public ComputedPreferencePartnerResource() throws SQLException
    {
        if (!tableExist("computedPreferencePartner"))
        {
            logger.debug("Create computedPreferencePartner table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<ComputedPreferencePartner> list = dao.all();
        GenericEntity<List<ComputedPreferencePartner>> entity = new GenericEntity<List<ComputedPreferencePartner>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/{idHolidayMaker}")
    public Response findById(@PathParam("idHolidayMaker") int idHolidayMaker)
    {
        List<ComputedPreferencePartner> list = dao.findById(idHolidayMaker);
        GenericEntity<List<ComputedPreferencePartner>> entity = new GenericEntity<List<ComputedPreferencePartner>>(list){};
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(ComputedPreferencePartner computedPreferencePartner)
    {
        dao.insert(computedPreferencePartner);
        return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @PUT
    public Response update(ComputedPreferencePartner computedPreferencePartner){
        int i = dao.update(computedPreferencePartner);
        if(i != 0)
            return Response.ok().build();
        else
            return Response.notModified().build();
    }




}
