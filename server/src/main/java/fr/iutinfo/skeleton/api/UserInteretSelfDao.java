package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface UserInteretSelfDao
{
    @SqlUpdate("create table preferenceInteretSelf(id_user integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_interetSelf foreign key(id) references interetSelf(id_interetSelf)," +
            "constraint pk_interetSelf primary key (id_user, id))")
    void createUserInteretSelf();

    @SqlUpdate("insert into preferenceInteretSelf(id_user, id, value) values (:id_user, :id, :value)")
    int insert(@BindBean() UserInteretSelf preferenceInteretSelf);

    @SqlQuery("select * from preferenceInteretSelf")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<UserInteretSelf> all();

    @SqlQuery("select * from preferenceInteretSelf where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    UserInteretSelf findByIdUser(@Bind("id_user") int id_user);
    
    @SqlQuery("select * from preferenceInteretSelf where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    UserInteretSelf findByIdInteret(@Bind("id") int id);

    @SqlUpdate("update preferenceInteretSelf set value = :value where id_user = :id_user")
    void update(@BindBean() UserInteretSelf preferenceInteretSelf);

    void close();
}
