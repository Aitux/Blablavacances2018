package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/hasTourValue")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HasTourValueResource
{


    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.HasTourValueResource.class);
    private static HasTourValueDao dao = getDbi().open(HasTourValueDao.class);


    @Context
    UriInfo uriInfo;

    public HasTourValueResource() throws SQLException
    {
        if (!tableExist("hasTourValue"))
        {
            logger.debug("Create hasTourValue table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<HasTourValue> list = dao.all();
        GenericEntity<List<HasTourValue>> entity = new GenericEntity<List<HasTourValue>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(HasTourValue hasTourValue)
    {
        int i = dao.insert(hasTourValue);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{tourFeatureValue}")
    public Response findByTourFeature(@PathParam("tourFeatureValue") String tourFeatureValue)
    {
        List<HasTourValue> list = dao.findByTourFeatureValue(tourFeatureValue);
        GenericEntity<List<HasTourValue>> entity = new GenericEntity<List<HasTourValue>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/feature/{featureTName}")
    public Response findByFeatureTName(@PathParam("featureTName") String featureTName){
        List<HasTourValue> list = dao.findByFeatureTName(featureTName);
        GenericEntity<List<HasTourValue>> entity = new GenericEntity<List<HasTourValue>>(list)
        {
        };
        return Response.ok(entity).build();
    }


}
