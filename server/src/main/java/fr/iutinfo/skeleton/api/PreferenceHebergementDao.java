package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceHebergementDao
{


    @SqlUpdate("create table preferenceHebergement(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_hebergement foreign key(id) references hebergement(id_hebergement)," +
            "constraint pk_hebergement primary key (id_user, id_voyage, id))")
    void createPreferenceHebergement();

    @SqlUpdate("insert into preferenceHebergement(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceHebergement preferenceHebergement);

    @SqlQuery("select * from preferenceHebergement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceHebergement> all();

    @SqlQuery("select * from preferenceHebergement where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceHebergement findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceHebergement where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceHebergement findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceHebergement where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceHebergement findByIdHebergement(@Bind("id") int id);

    @SqlUpdate("update preferenceHebergement set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceHebergement preferenceHebergement);

    @SqlUpdate("delete from preferenceHebergement where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
