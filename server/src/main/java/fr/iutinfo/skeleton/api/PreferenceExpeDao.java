package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceExpeDao
{


    @SqlUpdate("create table preferenceExpe(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_expe foreign key(id) references expe(id_expe)," +
            "constraint pk_expe primary key (id_user, id_voyage, id))")
    void createPreferenceExpe();

    @SqlUpdate("insert into preferenceExpe(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceExpe preferenceExpe);

    @SqlQuery("select * from preferenceExpe")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceExpe> all();

    @SqlQuery("select * from preferenceExpe where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceExpe findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceExpe where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceExpe findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceExpe where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceExpe findByIdExpe(@Bind("id") int id);

    @SqlUpdate("update preferenceExpe set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceExpe preferenceExpe);

    @SqlUpdate("delete from preferenceExpe where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
