package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface ComputedPreferenceTourDao
{
    @SqlUpdate("create table computedPreferenceTour(" +
            "score float," +
            "idTour integer ON DELETE CASCADE," +
            "idHolidayMaker integer ON DELETE CASCADE," +
            "constraint fk_computedPreferenceTourIdTour foreign key(idTour) references Tour(idTour)," +
            "constraint fk_computedPreferenceTourIdHolidayMaker foreign key(idHolidayMaker) references HolidayMaker(idHolidayMaker)," +
            "constraint pk_computedPreferenceTour primary key (idTour, idHolidayMaker))")
    void createTable();

    @SqlUpdate("insert into computedPreferenceTour(score, idTour, idHolidayMaker) values(:score, :idTour, :idHolidayMaker)")
    int insert(@BindBean() ComputedPreferenceTour tour);

    @SqlQuery("select * from computedPreferenceTour")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<ComputedPreferenceTour> all();

    @SqlQuery("select * from computedPreferenceTour where idTour=:idTour OR idHolidayMaker=:idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<ComputedPreferenceTour> findById(@Bind("idHolidayMaker") int idHolidayMaker, @Bind("idTour") int idTour);

    @SqlUpdate("update computedPreferenceTour set score = :score where idTour = :idTour AND idHolidayMaker = :idHolidayMaker")
    int update(@BindBean() ComputedPreferenceTour computedPreferenceTour);

    void close();
}
