package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface InteretSelfDao
{
    @SqlUpdate("create table interetSelf (id_interetSelf integer primary key autoincrement, libelle varchar(50))")
    void createInteretSelfTable();

    @SqlUpdate("insert into interetSelf (libelle) values (:libelle)")
    int insert (@BindBean InteretSelf interetSelf);

    @SqlQuery("select * from interetSelf")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<InteretSelf> all();

    @SqlUpdate("update interetSelf set libelle = :libelle where id_interetSelf = :id_interetSelf")
    void update(@BindBean InteretSelf interetSelf);

    @SqlQuery("select * from interetSelf where id_interetSelf = :id_interetSelf")
    @RegisterMapperFactory(BeanMapperFactory.class)
    InteretSelf findById(@Bind("id_interetSelf") int id_interetSelf);

    @SqlQuery("select * from interetSelf where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    InteretSelf findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from interetSelf where id_interetSelf = :id_interetSelf")
    void delete(@Bind("id_interetSelf") int id_interetSelf);

    @SqlUpdate("drop table if exists interetSelf")
    void dropInteretSelfTable();

    void close();
}
