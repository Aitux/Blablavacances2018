package fr.iutinfo.skeleton.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 30/04/18.
 */
@Path("/csv")
public class CsvExporterResource
{
    @GET
    public Response getCsv(){
        CsvExporter csvExporter = new CsvExporter();
        String myCsvText = csvExporter.buildCsvReadable();
        return Response.ok(myCsvText).header("Content-Disposition", "attachment; filename=" + "data.csv").build();
    }

    @GET
    @Path("/algo")
    public Response getCsvAlgo(){
        CsvExporter csvExporter = new CsvExporter();
        String myCsv = csvExporter.buildCsvAlgo();
        return Response.ok(myCsv).header("Content-Disposition", "attachment; filename=dataPartEnS-2018.csv").build();
    }

}
