package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class LikesPartner
{
    private int attractionPartnerFeature;
    private String holidayMakerFeatureValue;
    private int idHolidayMaker;
    private String featureHName;

    public LikesPartner(int attractionPartnerFeature, String holidayMakerFeatureValue, int idHolidayMaker, String featureHName)
    {
        this.attractionPartnerFeature = attractionPartnerFeature;
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
        this.idHolidayMaker = idHolidayMaker;
        this.featureHName = featureHName;
    }

    public LikesPartner(){}

    public int getAttractionPartnerFeature()
    {
        return attractionPartnerFeature;
    }

    public void setAttractionPartnerFeature(int attractionPartnerFeature)
    {
        this.attractionPartnerFeature = attractionPartnerFeature;
    }

    public String getHolidayMakerFeatureValue()
    {
        return holidayMakerFeatureValue;
    }

    public void setHolidayMakerFeatureValue(String holidayMakerFeatureValue)
    {
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

    public int getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }

    public String getFeatureHName()
    {
        return featureHName;
    }

    public void setFeatureHName(String featureHName)
    {
        this.featureHName = featureHName;
    }
}
