package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/describesHolidayMakers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DescribesHolidayMakersResource
{

    final static Logger logger = LoggerFactory.getLogger(DescribesHolidayMakersResource.class);
    private static DescribesHolidayMakersDao dao = getDbi().open(DescribesHolidayMakersDao.class);

    @Context
    UriInfo uriInfo;

    public DescribesHolidayMakersResource() throws SQLException
    {
        if (!tableExist("describesHolidayMakers"))
        {
            logger.debug("Create table describesHolidayMakers");
            dao.createTable();
        }
    }

    @POST
    public Response insert(DescribesHolidayMakers describesHolidayMakers){
        int i = dao.insert(describesHolidayMakers);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all()
    {
        List<DescribesHolidayMakers> list = dao.all();
        GenericEntity<List<DescribesHolidayMakers>> entity = new GenericEntity<List<DescribesHolidayMakers>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/{idHolidayMaker}")
    public Response findById(@PathParam("idHolidayMaker") int idHolidayMaker)
    {
        List<DescribesHolidayMakers> list = dao.findById(idHolidayMaker);
        GenericEntity<List<DescribesHolidayMakers>> entity = new GenericEntity<List<DescribesHolidayMakers>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/featureHName/{featureHName}")
    public Response findByFeatureTName(@PathParam("featureHName") String featureHName){
        List<DescribesHolidayMakers> list = dao.findByFeatureName(featureHName);
        GenericEntity<List<DescribesHolidayMakers>> entity = new GenericEntity<List<DescribesHolidayMakers>>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
