package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface ComputedPreferencePartnerDao
{
    @SqlUpdate("create table computedPreferencePartner(" +
            "score float," +
            "idHolidayMaker1 integer ON DELETE CASCADE," +
            "idHolidayMaker2 integer ON DELETE CASCADE," +
            "constraint fk_computedPreferencePartnerIdHolidayMaker1 foreign key(idHolidayMaker1) references HolidayMaker(idHolidayMaker)," +
            "constraint fk_computedPreferencePartnerIdHolidayMaker2 foreign key(idHolidayMaker2) references HolidayMaker(idHolidayMaker)," +
            "constraint pk_computedPreferencePartner primary key (idHolidayMaker1, idHolidayMaker2))")
    void createTable();

    @SqlUpdate("insert into computedPreferencePartner(score, idHolidayMaker1, idHolidayMaker2) values(:score, :idHolidayMaker1, :idHolidayMaker2)")
    int insert(@BindBean() ComputedPreferencePartner partner);

    @SqlQuery("select * from computedPreferencePartner")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<ComputedPreferencePartner> all();

    @SqlQuery("select * from computedPreferencePartner where idHolidayMaker1=:idHolidayMaker OR idHolidayMaker2=:idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<ComputedPreferencePartner> findById(@Bind("idHolidayMaker") int idHolidayMaker);

    @SqlUpdate("update computedPreferencePartner set score = :score where idHolidayMaker1 = :idHolidayMaker1 AND idHolidayMaker2 = :idHolidayMaker2")
    int update(@BindBean() ComputedPreferencePartner computedPreferencePartner);

    void close();

}
