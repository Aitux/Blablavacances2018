package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 07/05/18.
 */
public interface ParticipeDao
{

    @SqlUpdate("create table participe(" +
            "id_user integer," +
            "id_voyage integer," +
            "constraint fk_user_participe foreign key(id_user) references users(id)," +
            "constraint fk_voyage_participe foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint pk_participe primary key(id_user,id_voyage))")
    void createTablePartcipe();

    @SqlUpdate("insert into participe(id_user, id_voyage) values(:id_user, :id_voyage)")
    int insert(@BindBean() Participe participe);

    @SqlQuery("select * from participe")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Participe> all();

    @SqlQuery("select * from participe where id_user=:id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Participe> getByUserId(@Bind("id_user") int id_user);

    @SqlQuery("select * from participe where id_voyage=:id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Participe> getByVoyageId(@Bind("id_voyage") int id_voyage);

    @SqlUpdate("delete from participe where id_user=:id_user AND id_voyage=:id_voyage")
    int delete(@BindBean() Participe participe);

    @SqlUpdate("delete from participe where id_user=:id_user")
    int deleteUser(@Bind("id_user") int id_user);

    @SqlUpdate("delete from participe where id_voyage=:id_voyage")
    int deleteVoyage(@Bind("id_voyage") int id_voyage);

    void close();
}
