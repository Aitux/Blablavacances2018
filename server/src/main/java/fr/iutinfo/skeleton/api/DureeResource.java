package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DureeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/duree")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DureeResource
{
    final static Logger logger = LoggerFactory.getLogger(DureeResource.class);
    private static DureeDao dao = getDbi().open(DureeDao.class);

    @Context
    public UriInfo uriInfo;

    public DureeResource() throws SQLException
    {
        if (!tableExist("duree"))
        {
            logger.debug("Create table duree");
            dao.createDureeTable();
        }
    }

    @GET
    public List<DureeDto> getAllDuree(){
        List<Duree> dureeList;
        dureeList = dao.all();
        return dureeList.stream().map(Duree::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Duree duree){
        if(dao.all().contains(duree)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(duree);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(duree.getLibelle()).getId_duree()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_duree}")
    public Response findById(@PathParam("id_duree") int id){
        Duree a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Duree a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

