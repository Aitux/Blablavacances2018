package fr.iutinfo.skeleton.api;

import com.google.common.base.Charsets;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import fr.iutinfo.skeleton.common.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.security.SecureRandom;

public class User implements Principal
{
    final static Logger logger = LoggerFactory.getLogger(User.class);
    private static User anonymous = new User(-1, "Anonymous", "anonym");
    private String name;
    private String prenom;
    private String alias;
    private String tel;
    private String anneenaissance;
    private int id = 0;
    private String email;
    private String adresse;
    private String sexe;
    private String profession;
    private int niveauEtude;
    private String password;
    private String passwdHash;
    private String salt;
    private String search;
    private int fumeur;

    public User(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public User(int id, String name, String alias)
    {
        this.id = id;
        this.name = name;
        this.alias = alias;
    }

    public User(String name, String prenom, String alias, String tel, String daten, int id, String email,
                String adresse, int fumeur)
    {
        super();
        this.name = name;
        this.prenom = prenom;
        this.alias = alias;
        this.tel = tel;
        this.anneenaissance = daten;
        this.id = id;
        this.email = email;
        this.adresse = adresse;
        this.fumeur = fumeur;
    }

    public User()
    {
    }

    public static User getAnonymousUser()
    {
        return anonymous;
    }

    public int getFumeur()
    {
        return fumeur;
    }

    public void setFumeur(int fumeur)
    {
        this.fumeur = fumeur;
    }

    public String getProfession()
    {
        return profession;
    }

    public void setProfession(String profession)
    {
        this.profession = profession;
    }

    public int getNiveauEtude()
    {
        return niveauEtude;
    }

    public void setNiveauEtude(int niveauEtude)
    {
        this.niveauEtude = niveauEtude;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        this.tel = tel;
    }


    public String getAdresse()
    {
        return adresse;
    }

    public void setAdresse(String adresse)
    {
        this.adresse = adresse;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        passwdHash = buildHash(password, getSalt());
        this.password = password;
    }

    private String buildHash(String password, String s)
    {
        Hasher hasher = Hashing.sha256().newHasher();
        hasher.putString(password + s, Charsets.UTF_8);
        return hasher.hash().toString();
    }

    public boolean isGoodPassword(String password)
    {
        if (isAnonymous())
        {
            return false;
        }
        String hash = buildHash(password, getSalt());
        return hash.equals(getPasswdHash());
    }

    public String getPasswdHash()
    {
        return passwdHash;
    }

    public void setPasswdHash(String passwdHash)
    {
        this.passwdHash = passwdHash;
    }

    @Override
    public String toString()
    {
        return id + ": " + alias + ", " + name + " <" + email + ">";
    }

    public String getAlias()
    {
        return alias;
    }

    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    public String getSalt()
    {
        if (salt == null)
        {
            salt = generateSalt();
        }
        return salt;
    }

    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    private String generateSalt()
    {
        SecureRandom random = new SecureRandom();
        Hasher hasher = Hashing.sha256().newHasher();
        hasher.putLong(random.nextLong());
        return hasher.hash().toString();
    }

    public String getAnneenaissance()
    {
        return anneenaissance;
    }

    public void setAnneenaissance(String anneenaissance)
    {
        this.anneenaissance = anneenaissance;
    }

    public void resetPasswordHash()
    {
        if (password != null && !password.isEmpty())
        {
            setPassword(getPassword());
        }
    }

    public boolean isInUserGroup()
    {
        return !(id == anonymous.getId());
    }

    public boolean isAnonymous()
    {
        return this.getId() == getAnonymousUser().getId();
    }

    public String getSearch()
    {
        search = name + " " + alias + " " + email;
        return search;
    }

    public void setSearch(String search)
    {
        this.search = search;
    }


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());

        result = prime * result + ((anneenaissance == null) ? 0 : anneenaissance.hashCode());

        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + id;

        result = prime * result + ((name == null) ? 0 : name.hashCode());

        result = prime * result + ((passwdHash == null) ? 0 : passwdHash.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());

        result = prime * result + ((salt == null) ? 0 : salt.hashCode());
        result = prime * result + ((search == null) ? 0 : search.hashCode());
        result = prime * result + ((sexe == null) ? 0 : sexe.hashCode());
        result = prime * result + ((tel == null) ? 0 : tel.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (adresse == null)
        {
            if (other.adresse != null)
                return false;
        } else if (!adresse.equals(other.adresse))
            return false;
        if (alias == null)
        {
            if (other.alias != null)
                return false;
        } else if (!alias.equals(other.alias))
            return false;
        if (anneenaissance == null)
        {
            if (other.anneenaissance != null)
                return false;
        } else if (!anneenaissance.equals(other.anneenaissance))
            return false;
        if (email == null)
        {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (id != other.id)
            return false;
        if (name == null)
        {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (passwdHash == null)
        {
            if (other.passwdHash != null)
                return false;
        } else if (!passwdHash.equals(other.passwdHash))
            return false;
        if (password == null)
        {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (prenom == null)
        {
            if (other.prenom != null)
                return false;
        } else if (!prenom.equals(other.prenom))
            return false;
        if (salt == null)
        {
            if (other.salt != null)
                return false;
        } else if (!salt.equals(other.salt))
            return false;
        if (search == null)
        {
            if (other.search != null)
                return false;
        } else if (!search.equals(other.search))
            return false;
        if (sexe == null)
        {
            if (other.sexe != null)
                return false;
        } else if (!sexe.equals(other.sexe))
            return false;
        if (tel == null)
        {
            if (other.tel != null)
                return false;
        } else if (!tel.equals(other.tel))
            return false;
        return true;
    }

    public void initFromDto(UserDto dto)
    {
        this.setAlias(dto.getAlias());
        this.setEmail(dto.getEmail());
        this.setId(dto.getId());
        this.setName(dto.getName());
        this.setPassword(dto.getPassword());
        this.setTel(dto.getTel());
        this.setSexe(dto.getSexe());
        this.setAdresse(dto.getAdresse());
        this.setPrenom(dto.getPrenom());
        this.setAnneenaissance(dto.getAnneenaissance());
        this.setNiveauEtude(dto.getNiveauEtude());
        this.setProfession(dto.getProfession());
        this.setFumeur(dto.getFumeur());
    }

    public String getSexe()
    {
        return sexe;
    }

    public void setSexe(String sexe)
    {
        this.sexe = sexe;
    }

    public UserDto convertToDto()
    {
        UserDto dto = new UserDto();
        dto.setAlias(this.getAlias());
        dto.setEmail(this.getEmail());
        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setPassword(this.getPassword());
        dto.setTel(this.getTel());
        dto.setAdresse(this.getAdresse());
        dto.setPrenom(this.getPrenom());
        dto.setAnneenaissance(this.getAnneenaissance());
        dto.setSexe(this.getSexe());
        dto.setNiveauEtude(this.getNiveauEtude());
        dto.setProfession(this.getProfession());
        dto.setFumeur(this.getFumeur());
        return dto;
    }
}
