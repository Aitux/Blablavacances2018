package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/tour")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TourResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.TourResource.class);
    private static TourDao dao = getDbi().open(TourDao.class);


    @Context
    UriInfo uriInfo;

    public TourResource() throws SQLException
    {
        if (!tableExist("Tour"))
        {
            logger.debug("Create Tour table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<Tour> list = dao.all();
        GenericEntity<List<Tour>> entity = new GenericEntity<List<Tour>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert()
    {
        int i = dao.insert();
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{idTour}")
    public Response findByHolidayMaker(@PathParam("idTour") int idTour)
    {
        Tour tour = dao.findByTourId(idTour);
        GenericEntity<Tour> entity = new GenericEntity<Tour>(tour)
        {
        };
        return Response.ok(entity).build();
    }
}
