package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceDuree")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceDureeResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceDureeResource.class);
    private static PreferenceDureeDao dao = getDbi().open(PreferenceDureeDao.class);

    public PreferenceDureeResource() throws SQLException
    {
        if (!tableExist("preferenceDuree"))
        {
            logger.debug("Create table preferenceDuree");
            dao.createPreferenceDuree();
        }
    }

    @POST
    public Response insert(PreferenceDuree preferenceDuree){
        int s =  dao.insert(preferenceDuree);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceDuree> preferenceDureeList = dao.all();
        GenericEntity<List<PreferenceDuree>> entity = new GenericEntity<List<PreferenceDuree>>(preferenceDureeList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/duree/{id}")
    public Response findByIdDuree(@PathParam("id") int id){
        return Response.ok(dao.findByIdDuree(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceDuree preferenceDuree){
        preferenceDuree.setId_user(id_user);
        dao.update(preferenceDuree);
        return Response.ok().build();
    }

}
