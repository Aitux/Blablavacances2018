package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/tourFeatureValue")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TourFeatureValueResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.TourFeatureValueResource.class);
    private static TourFeatureValueDao dao = getDbi().open(TourFeatureValueDao.class);


    @Context
    UriInfo uriInfo;

    public TourFeatureValueResource() throws SQLException
    {
        if (!tableExist("TourFeatureValue"))
        {
            logger.debug("Create TourFeatureValue table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<TourFeatureValue> list = dao.all();
        GenericEntity<List<TourFeatureValue>> entity = new GenericEntity<List<TourFeatureValue>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(TourFeatureValue featureTName)
    {
        int i = dao.insert(featureTName);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(URI.create(uriInfo.getAbsolutePath()+"/"+featureTName)).build();
    }

    @GET
    @Path("/{tourFeatureValue}")
    public Response findByTourFeatureValue(@PathParam("tourFeatureValue") String tourFeatureValue)
    {
        TourFeatureValue list = dao.findByTourFeatureValue(tourFeatureValue);
        GenericEntity<TourFeatureValue> entity = new GenericEntity<TourFeatureValue>(list)
        {
        };
        return Response.ok(entity).build();
    }

}
