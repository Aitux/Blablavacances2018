package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/holidayMakerFeatureValue")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HolidayMakerFeatureValueResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.HolidayMakerFeatureValueResource.class);
    private static HolidayMakerFeatureValueDao dao = getDbi().open(HolidayMakerFeatureValueDao.class);


    @Context
    UriInfo uriInfo;

    public HolidayMakerFeatureValueResource() throws SQLException
    {
        if (!tableExist("HolidayMakerFeatureValue"))
        {
            logger.debug("Create HolidayMakerFeatureValue table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<HolidayMakerFeatureValue> list = dao.all();
        GenericEntity<List<HolidayMakerFeatureValue>> entity = new GenericEntity<List<HolidayMakerFeatureValue>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(HolidayMakerFeatureValue featureHName)
    {
        int i = dao.insert(featureHName);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{holidayMakerFeatureValue}")
    public Response findByFeatureHName(@PathParam("holidayMakerFeatureValue") String holidayMakerFeatureValue)
    {
        HolidayMakerFeatureValue list = dao.findByValue(holidayMakerFeatureValue);
        GenericEntity<HolidayMakerFeatureValue> entity = new GenericEntity<HolidayMakerFeatureValue>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
