package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.HebergementDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Hebergement
{
    private int id_hebergement;
    private String libelle;

    public Hebergement(){
    }

    public Hebergement(String libelle){
        this.libelle = libelle;
    }


    public int getId_hebergement()
    {
        return id_hebergement;
    }

    public void setId_hebergement(int id_hebergement)
    {
        this.id_hebergement = id_hebergement;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(HebergementDto dto){
        this.setId_hebergement(dto.getId_hebergement());
        this.setLibelle(dto.getLibelle());
    }

    public HebergementDto convertToDto(){
        HebergementDto dto = new HebergementDto();
        dto.setId_hebergement(this.getId_hebergement());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
