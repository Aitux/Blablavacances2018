package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceProfession")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceProfessionResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceProfessionResource.class);
    private static PreferenceProfessionDao dao = getDbi().open(PreferenceProfessionDao.class);

    public PreferenceProfessionResource() throws SQLException
    {
        if (!tableExist("preferenceProfession"))
        {
            logger.debug("Create table preferenceProfession");
            dao.createPreferenceProfession();
        }
    }

    @POST
    public Response insert(PreferenceProfession preferenceProfession){
        int s =  dao.insert(preferenceProfession);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceProfession> preferenceProfessionList = dao.all();
        GenericEntity<List<PreferenceProfession>> entity = new GenericEntity<List<PreferenceProfession>>(preferenceProfessionList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/profession/{id}")
    public Response findByIdProfession(@PathParam("id") int id){
        return Response.ok(dao.findByIdProfession(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceProfession preferenceProfession){
        preferenceProfession.setId_user(id_user);
        dao.update(preferenceProfession);
        return Response.ok().build();
    }

}
