package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceExpe")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceExpeResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceExpeResource.class);
    private static PreferenceExpeDao dao = getDbi().open(PreferenceExpeDao.class);

    public PreferenceExpeResource() throws SQLException
    {
        if (!tableExist("preferenceExpe"))
        {
            logger.debug("Create table preferenceExpe");
            dao.createPreferenceExpe();
        }
    }

    @POST
    public Response insert(PreferenceExpe preferenceExpe){
        int s =  dao.insert(preferenceExpe);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceExpe> preferenceExpeList = dao.all();
        GenericEntity<List<PreferenceExpe>> entity = new GenericEntity<List<PreferenceExpe>>(preferenceExpeList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/expe/{id}")
    public Response findByIdExpe(@PathParam("id") int id){
        return Response.ok(dao.findByIdExpe(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceExpe preferenceExpe){
        preferenceExpe.setId_user(id_user);
        dao.update(preferenceExpe);
        return Response.ok().build();
    }

}
