package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceDestination")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceDestinationResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceDestinationResource.class);
    private static PreferenceDestinationDao dao = getDbi().open(PreferenceDestinationDao.class);

    public PreferenceDestinationResource() throws SQLException
    {
        if (!tableExist("preferenceDestination"))
        {
            logger.debug("Create table preferenceDestination");
            dao.createPreferenceDestination();
        }
    }

    @POST
    public Response insert(PreferenceDestination preferenceDestination){
        int s =  dao.insert(preferenceDestination);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceDestination> preferenceDestinationList = dao.all();
        GenericEntity<List<PreferenceDestination>> entity = new GenericEntity<List<PreferenceDestination>>(preferenceDestinationList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/destination/{id}")
    public Response findByIdDestination(@PathParam("id") int id){
        return Response.ok(dao.findByIdDestination(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceDestination preferenceDestination){
        preferenceDestination.setId_user(id_user);
        dao.update(preferenceDestination);
        return Response.ok().build();
    }

}
