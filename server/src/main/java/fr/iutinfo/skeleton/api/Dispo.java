package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DispoDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Dispo
{
    private int id_dispo;
    private String libelle;

    public Dispo(){
    }

    public Dispo(String libelle){
        this.libelle = libelle;
    }


    public int getId_dispo()
    {
        return id_dispo;
    }

    public void setId_dispo(int id_dispo)
    {
        this.id_dispo = id_dispo;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(DispoDto dto){
        this.setId_dispo(dto.getId_dispo());
        this.setLibelle(dto.getLibelle());
    }

    public DispoDto convertToDto(){
        DispoDto dto = new DispoDto();
        dto.setId_dispo(this.getId_dispo());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
