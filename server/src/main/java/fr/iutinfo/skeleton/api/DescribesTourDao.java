package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface DescribesTourDao
{
    @SqlUpdate("create table describesTour (" +
            "idTour integer ON DELETE CASCADE," +
            "featureTName text ON DELETE CASCADE," +
            "tourFeatureValue text ON DELETE CASCADE," +
            "constraint fk_idTour(idTour) references Tour(idTour)," +
            "constraint fk_featureTName(featureTName) references TourFeature(featureTName)," +
            "constraint fk_tourFeatureValue(tourFeatureValue) references TourFeatureValue(tourFeatureValue)," +
            "constraint pk_describesTour primary key(idTour, featureTName, tourFeatureValue))")
    void createTable();

    @SqlUpdate("insert into describesTour(idTour, featureTName, tourFeatureValue) values (:idTour, :featureTName, :tourFeatureValue)")
    int insert(@BindBean() DescribesTour describesTour);

    @SqlQuery("select * from describesTour")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesTour> all();

    @SqlQuery("select * from describesTour where idTour=:idTour")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesTour> findById(@Bind("idTour") int idTour);

    @SqlQuery("select * from describesTour where featureTName=:featureTName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesTour> findByFeatureName(@Bind("featureTName") String featureTName);

    void close();
}
