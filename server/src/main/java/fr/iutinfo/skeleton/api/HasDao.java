package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface HasDao
{
    @SqlUpdate("create table has(" +
            "featureHName text ON DELETE CASCADE," +
            "holidayMakerFeatureValue text ON DELETE CASCADE," +
            "constraint fk_featureHNameHas(featureHName) references HolidayMakerFeature(featureHName)," +
            "constraint fk_holidayMakerFeatureValueHas(holidayMakerFeatureValue) references HolidayMakerFeatureValue(holidayMakerFeatureValue)," +
            "constraint pk_has primary key(featureHName, holidayMakerFeatureValue))")
    void createTable();

    @SqlUpdate("insert into has(featureHName, holidayMakerFeatureValue) values(:featureHName, :holidayMakerFeatureValue")
    int insert(@BindBean() Has has);

    @SqlQuery("select * from has")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Has> all();

    @SqlQuery("select * from has where featureHName = :featureHName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Has> findByFeatureHName(@Bind("featureHName") String featureHName);

    @SqlQuery("select * from has where holidayMakerFeatureValue=:holidayMakerFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Has> findByHolidayMakerFeatureValue(@Bind("holidayMakerFeatureValue") String holidayMakerFeatureValue);

    void close();
}
