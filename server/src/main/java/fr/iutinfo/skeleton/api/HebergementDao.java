package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface HebergementDao
{
    @SqlUpdate("create table hebergement (id_hebergement integer primary key autoincrement, libelle varchar(50))")
    void createHebergementTable();

    @SqlUpdate("insert into hebergement (libelle) values (:libelle)")
    int insert (@BindBean Hebergement hebergement);

    @SqlQuery("select * from hebergement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Hebergement> all();

    @SqlUpdate("update hebergement set libelle = :libelle where id_hebergement = :id_hebergement")
    void update(@BindBean Hebergement hebergement);

    @SqlQuery("select * from hebergement where id_hebergement = :id_hebergement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Hebergement findById(@Bind("id_hebergement") int id_hebergement);

    @SqlQuery("select * from hebergement where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Hebergement findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from hebergement where id_hebergement = :id_hebergement")
    void delete(@Bind("id_hebergement") int id_hebergement);

    @SqlUpdate("drop table if exists hebergement")
    void dropHebergementTable();

    void close();
}
