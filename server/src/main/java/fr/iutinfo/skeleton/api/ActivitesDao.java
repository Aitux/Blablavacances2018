package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface ActivitesDao
{
    @SqlUpdate("create table activite(id_activite integer primary key autoincrement, libelle varchar(50))")
    void createActivitesTable();

    @SqlUpdate("insert into activite (libelle) values (:libelle)")
    int insert (@BindBean Activites activites);

    @SqlQuery("select * from activite")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Activites> all();

    @SqlUpdate("update activite set libelle = :libelle where id_activite = :id_activite")
    void update(@BindBean Activites activites);

    @SqlQuery("select * from activite where id_activite = :id_activite")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Activites findById(@Bind("id_activite") int id_activite);

    @SqlQuery("select * from activite where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Activites findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from activite where id_activite = :id_activite")
    void delete(@Bind("id_activite") int id_activite);

    @SqlUpdate("drop table if exists activite")
    void dropActivitesTable();

    void close();
}
