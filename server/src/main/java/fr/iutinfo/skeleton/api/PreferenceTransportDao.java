package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceTransportDao
{


    @SqlUpdate("create table preferenceTransport(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_transport foreign key(id) references transport(id_transport)," +
            "constraint pk_transport primary key (id_user, id_voyage, id))")
    void createPreferenceTransport();

    @SqlUpdate("insert into preferenceTransport(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceTransport preferenceTransport);

    @SqlQuery("select * from preferenceTransport")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceTransport> all();

    @SqlQuery("select * from preferenceTransport where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransport findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceTransport where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransport findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceTransport where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceTransport findByIdTransport(@Bind("id") int id);

    @SqlUpdate("update preferenceTransport set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceTransport preferenceTransport);

    @SqlUpdate("delete from preferenceTransport where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
