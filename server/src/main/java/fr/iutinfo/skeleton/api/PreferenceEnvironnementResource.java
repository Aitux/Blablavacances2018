package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceEnvironnement")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceEnvironnementResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceEnvironnementResource.class);
    private static PreferenceEnvironnementDao dao = getDbi().open(PreferenceEnvironnementDao.class);

    public PreferenceEnvironnementResource() throws SQLException
    {
        if (!tableExist("preferenceEnvironnement"))
        {
            logger.debug("Create table preferenceEnvironnement");
            dao.createPreferenceEnvironnement();
        }
    }

    @POST
    public Response insert(PreferenceEnvironnement preferenceEnvironnement){
        int s =  dao.insert(preferenceEnvironnement);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceEnvironnement> preferenceEnvironnementList = dao.all();
        GenericEntity<List<PreferenceEnvironnement>> entity = new GenericEntity<List<PreferenceEnvironnement>>(preferenceEnvironnementList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/environnement/{id}")
    public Response findByIdEnvironnement(@PathParam("id") int id){
        return Response.ok(dao.findByIdEnvironnement(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceEnvironnement preferenceEnvironnement){
        preferenceEnvironnement.setId_user(id_user);
        dao.update(preferenceEnvironnement);
        return Response.ok().build();
    }

}
