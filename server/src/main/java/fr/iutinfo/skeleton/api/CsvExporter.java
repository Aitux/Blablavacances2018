package fr.iutinfo.skeleton.api;

import com.opencsv.CSVWriter;
import fr.iutinfo.skeleton.integration.AgeCalculator;
import fr.iutinfo.skeleton.integration.db.dbObject.JobLabel;
import fr.iutinfo.skeleton.integration.db.dbObject.Sex;
import org.skife.jdbi.cglib.core.Local;
import org.supercsv.cellprocessor.ift.CellProcessor;

import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 30/04/18.
 */
public class CsvExporter
{

    private final static String newLine = System.getProperty("line.separator");
    private int cpt;

    private List<Integer> users = new ArrayList<>();

    public static void main(String[] args)
    {
        CsvExporter csvExporter = new CsvExporter();
        csvExporter.buildCsvAlgo();
    }

    /**
     * KISS ? Single Responsibility ? Elle est nulle cette méthode. Mais elle fait le taf.. On la modifiera si on a le temps.
     * Build un csv de la base de données
     * <p>
     * Delimiteur = [;]
     * <p>
     */
    public String buildCsvReadable()
    {
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, ';');
        Connection c = null;
        ArrayList<String> preferenceTableName = new ArrayList<>();
        ArrayList<String> res1 = new ArrayList<>();
        try
        {

            c = getConnection();
            Statement preambule = c.createStatement();
            ResultSet rs0 = preambule.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name LIKE 'preference%'");

            while (rs0.next())
                preferenceTableName.add(rs0.getString("name"));

            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM users INNER JOIN voyage ON users.id = voyage.id_createur;");
            ArrayList<String> colName = new ArrayList<>();

            boolean first = true;

            while (rs.next())
            {
                int id = rs.getInt("id");

                res1.clear();
                res1.add("" + id);
                res1.add("" + rs.getInt("anneenaissance"));
                res1.add("" + rs.getString("sexe"));
                res1.add("" + rs.getInt("niveauEtude"));
                res1.add("" + rs.getString("profession"));
                res1.add("" + rs.getInt("fumeur"));
                res1.add("" + rs.getInt("budget"));
                res1.add("" + rs.getInt("route_seul"));
                res1.add("" + rs.getInt("id_voyage"));
                res1.add("" + rs.getString("nb_co_vac"));
                res1.add("" + rs.getInt("sexe_co_vac"));
                res1.add("" + rs.getString("age_co_vac"));
                res1.add("" + rs.getInt("etude_co_vac"));
                res1.add("" + rs.getInt("fumeur_co_vac"));
                res1.add("" + rs.getString("animal_de_compagnie_co_vac"));
                if (first)
                {
                    colName.add("id_utilisateur");
                    colName.add("annee_naissance");
                    colName.add("sexe");
                    colName.add("niveau_etude");
                    colName.add("profession");
                    colName.add("fumeur");
                    colName.add("budget");
                    colName.add("route_seul");
                    colName.add("id_voyage");
                    colName.add("nb_co_vac");
                    colName.add("sexe_co_vac");
                    colName.add("age_co_vac");
                    colName.add("etude_co_vac");
                    colName.add("fumeur_co_vac");
                    colName.add("animal_de_compagnie_co_vac");
                }
                Statement statement1;
                for (String s : preferenceTableName)
                {
                    statement1 = c.createStatement();
                    ResultSet resultSet = statement1.executeQuery("SELECT * from " + s + " WHERE id_user = " + id);
                    while (resultSet.next())
                    {
                        res1.add("" + resultSet.getInt("id"));
                        res1.add("" + resultSet.getInt("value"));
                        if (first)
                        {
                            colName.add("id_" + s);
                            colName.add("value");
                        }
                    }
                }
                String[] res = new String[res1.size()];
                res = res1.toArray(res);
                if (first)
                {
                    String[] header = new String[colName.size()];
                    header = colName.toArray(header);
                    csvWriter.writeNext(header, false);
                    first = false;
                }
                csvWriter.writeNext(res, false);
            }

            try
            {
                csvWriter.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }

        } catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
        } finally
        {
            close(c);
        }
        return writer.toString();
    }

    public String buildCsvAlgo()
    {
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, ';','\'','\n');
        Connection c = null;

        try
        {
            c = getConnection();
            StringBuilder builder = getFirstRow(c);

            String[][] data = fillData();

            csvWriter.writeNext(builder.toString().split(","),false);
            for (String[] aData : data) csvWriter.writeNext(aData,false);

        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        System.out.println(writer.toString());
        return writer.toString();
    }

    private StringBuilder getFirstRow(Connection c)
    {

        StringBuilder importationTableString = new StringBuilder();
        importationTableString.append("ID,_Anneenaissance,_Age,my_age_less_60,my_age-60_69,my_age-70_74,my_age-75_80,my_age-over80,my_sex,my_studies_less_bac,my_studies_bac,my_studies_bac-1_3,my_studies_over_bac4,");
        try
        {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM profession");
            cpt = 0;
            while (rs.next())
            {
                cpt++;
                importationTableString.append("my_").append(rs.getString("libelle")).append(",");
            }

            importationTableString.append("my_smoking,");
            rs = stmt.executeQuery("SELECT libelle FROM interetSelf");

            importationTableString.append("_Budget,my_budget_less_250,my_budget-250-500,my_budget-500_1000,my_budget-1000_1500,my_budget-over1500,my_ability_to_travel_alone,");
            importationTableString.append("you_1_co_vac,you_2_co_vac,you_3_co_vac,you_4_co_vac,you_5_co_vac,").append("you_age_less_60,you_age-60_69,you_age-70_74,you_age-75_80,you_age-over80,you_F,you_M,you_any_sex,you_studies_less_bac,you_studies_bac,you_studies_bac-1_3,you_studies_over_bac4,you_smoking,");
            while (rs.next())
            {
                importationTableString.append("my_").append(rs.getString("libelle")).append(",");
            }

            rs = stmt.executeQuery("SELECT libelle FROM profession");
            while (rs.next())
                importationTableString.append("you_").append(rs.getString("libelle")).append(",");

            rs = stmt.executeQuery("SELECT libelle FROM interet");
            while (rs.next())
                importationTableString.append("you_").append(rs.getString("libelle")).append(",");

            String[] tmp = {"destination", "environnement", "dispo", "duree", "hebergement", "transportTo;"};

            for (String str :
                    tmp)
            {
                rs = stmt.executeQuery("SELECT libelle FROM " + str);
                while (rs.next())
                    importationTableString.append("trip_").append(rs.getString("libelle")).append(",");
            }
            importationTableString.deleteCharAt(importationTableString.length() - 1);


        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return importationTableString;
    }

    private String[][] fillData()
    {
        List<List<String>> data = new ArrayList<>();
        getUsers(data);
        getInteretSelf(data);
        getPreferenceProfession(data);
        getInteret(data);
        getDestination(data);
        getEnvironnement(data);
        getDispo(data);
        getDuree(data);
        getHebergement(data);
        getTransportTo(data);


        String[][] res = new String[data.size()][data.get(0).size()];
        for (int i = 0; i < res.length; i++)
            res[i] = data.get(i).toArray(new String[0]);

        return res;
    }


    private void getInteretSelf(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceInteretSelf pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getDestination(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceDestination pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getEnvironnement(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceEnvironnement pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getDispo(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceDispo pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getHebergement(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceHebergement pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getDuree(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceDuree pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getInteret(List<List<String>> list){
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceInteret pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getTransportTo(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceTransportTo pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getPreferenceProfession(List<List<String>> list)
    {
        int idx = 0;
        try
        {
            PreparedStatement pstmt = getConnection().prepareStatement("SELECT * from users u INNER JOIN preferenceProfession pd on u.id = pd.id_user WHERE u.id = ?;");
            getPreferenceGeneric(list, idx, pstmt);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    private void getPreferenceGeneric(List<List<String>> list, int idx, PreparedStatement pstmt) throws SQLException
    {
        for (int i :
                users)
        {
            pstmt.setInt(1, i);
            ResultSet rs = pstmt.executeQuery();
            List<String> strings = list.get(idx++);
            while (rs.next())
            {
                int val = rs.getInt("value");
                if (val == 3)
                    strings.add("0");
                else if (val == 2)
                    strings.add("0.5");
                else
                    strings.add("1");
            }
        }
    }

    private void getUsers(List<List<String>> list)
    {
        List<String> strings = new ArrayList<>();
        Connection c = null;
        int size;
        try
        {
            c = getConnection();
            PreparedStatement pstmt = c.prepareStatement("SELECT * FROM users INNER JOIN voyage v on users.id = v.id_createur");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                strings.add(rs.getString("id"));
                users.add(Integer.parseInt(rs.getString("id")));
                int anneenaissance = rs.getInt("anneenaissance");
                LocalDate birthYear = LocalDate.of(anneenaissance, 1, 1);
                int age = AgeCalculator.calculateAge(birthYear, LocalDate.now());
                strings.add("" + anneenaissance);
                strings.add("" + age);
                for (int i = 0; i < 4; i++)
                    strings.add("0");
                if (age < 60)
                    strings.add(3, "1");
                else if (age < 70)
                    strings.add(4, "1");
                else if (age < 75)
                    strings.add(5, "1");
                else if (age < 80)
                    strings.add(6, "1");
                else
                    strings.add(7, "1");


                strings.add("" + Sex.getSexFromString(rs.getString("sexe")).getValue());
                size = strings.size() - 1;
                for (int i = 0; i < 3; i++)
                    strings.add("0");
                strings.add(size + rs.getInt("niveauEtude"), "1");

                String job = rs.getString("profession");

                size = strings.size() - 1;
                for (int i = 0; i < cpt - 1; i++)
                    strings.add("0");

                int val = JobLabel.getJobLabel(job).getValue();
                strings.add(val + size, "1");

                if (rs.getInt("fumeur") == 3)
                    strings.add("0");
                else if (rs.getInt("fumeur") == 2)
                    strings.add("0.5");
                else
                    strings.add("1");

                int budget = rs.getInt("budget");
                strings.add("" + budget);

                size = strings.size() - 1;
                for (int i = 0; i < 4; i++)
                    strings.add("0");
                strings.add(size + budget, "1");
                strings.add("" + rs.getInt("route_seul"));

                // Convert an array string to an integer array
                int[] covacInt = Arrays.stream(rs.getString("nb_co_vac").split(",")).mapToInt(Integer::parseInt).toArray();

                size = strings.size() - 1;
                for (int i = 0; i < 5 - covacInt.length; i++)
                    strings.add("0");

                for (int i : covacInt)
                    strings.add(size + i, "1");
                ;
                int[] ageCovac = Arrays.stream(rs.getString("age_co_vac").split(",")).mapToInt(Integer::parseInt).toArray();

                size = strings.size() - 1;
                for (int i = 0; i < 5 - ageCovac.length; i++)
                    strings.add("0");
                for (int i : ageCovac)
                    strings.add(size + i, "1");

                size = strings.size() - 1;
                for (int i = 0; i < 2; i++)
                    strings.add("0");
                strings.add(size + rs.getInt("sexe_co_vac"), "1");

                int[] etudeCoVac = Arrays.stream(rs.getString("etude_co_vac").split(",")).mapToInt(Integer::parseInt).toArray();

                size = strings.size() - 1;
                for (int i = 0; i < 4 - etudeCoVac.length; i++)
                    strings.add("0");
                for (int i : etudeCoVac)
                    strings.add(size + i, "1");

                int fumeur = rs.getInt("fumeur_co_vac");

                if (fumeur == 1)
                    strings.add("1");
                else if (fumeur == 2)
                    strings.add("0.5");
                else
                    strings.add("0");

                list.add(strings);
                strings = new ArrayList<>();
            }
        } catch (ClassNotFoundException | SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void close(Connection c)
    {
        try
        {
            if (c != null)
                c.close();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws ClassNotFoundException
    {
        Class.forName("org.sqlite.JDBC");

        Connection c = null;
        try
        {
            c = DriverManager.getConnection("jdbc:sqlite:" + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "data.db");
        } catch (SQLException e)
        {

        }
        return c;
    }
}
