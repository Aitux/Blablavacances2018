package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class Tour
{
    private int idTour;

    public Tour(){
    }

    public Tour(int idTour)
    {
        this.idTour = idTour;
    }

    public int getIdTour()
    {
        return idTour;
    }

    public void setIdTour(int idTour)
    {
        this.idTour = idTour;
    }
}
