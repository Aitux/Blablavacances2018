package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface TransportToDao
{
    @SqlUpdate("create table transportTo (id_transportTo integer primary key autoincrement, libelle varchar(50))")
    void createTransportToTable();

    @SqlUpdate("insert into transportTo (libelle) values (:libelle)")
    int insert (@BindBean TransportTo transportTo);

    @SqlQuery("select * from transportTo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<TransportTo> all();

    @SqlUpdate("update transportTo set libelle = :libelle where id_transportTo = :id_transportTo")
    void update(@BindBean TransportTo transportTo);

    @SqlQuery("select * from transportTo where id_transportTo = :id_transportTo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    TransportTo findById(@Bind("id_transportTo") int id_transportTo);

    @SqlQuery("select * from transportTo where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    TransportTo findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from transportTo where id_transportTo = :id_transportTo")
    void delete(@Bind("id_transportTo") int id_transportTo);

    @SqlUpdate("drop table if exists transportTo")
    void dropTransportToTable();

    void close();
}
