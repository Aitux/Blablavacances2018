package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DureeDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Duree
{
    private int id_duree;
    private String libelle;

    public Duree(){
    }

    public Duree(String libelle){
        this.libelle = libelle;
    }


    public int getId_duree()
    {
        return id_duree;
    }

    public void setId_duree(int id_duree)
    {
        this.id_duree = id_duree;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(DureeDto dto){
        this.setId_duree(dto.getId_duree());
        this.setLibelle(dto.getLibelle());
    }

    public DureeDto convertToDto(){
        DureeDto dto = new DureeDto();
        dto.setId_duree(this.getId_duree());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
