package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 07/05/18.
 */
public class Participe
{

    final static Logger logger = LoggerFactory.getLogger(Participe.class);

    private int id_user;
    private int id_voyage;

    public Participe(){

    }

    public Participe(int id_user, int id_voyage)
    {
        this.id_user = id_user;
        this.id_voyage = id_voyage;
    }

    public int getId_user()
    {
        return id_user;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }

    public int getId_voyage()
    {
        return id_voyage;
    }

    public void setId_voyage(int id_voyage)
    {
        this.id_voyage = id_voyage;
    }
}
