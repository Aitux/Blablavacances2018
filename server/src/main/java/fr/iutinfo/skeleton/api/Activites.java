package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ActivitesDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Activites
{
    private int id_activite;
    private String libelle;

    public Activites(){
    }

    public Activites(String libelle){
        this.libelle = libelle;
    }


    public int getId_activite()
    {
        return id_activite;
    }

    public void setId_activite(int id_activite)
    {
        this.id_activite = id_activite;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(ActivitesDto dto){
        this.setId_activite(dto.getId_activites());
        this.setLibelle(dto.getLibelle());
    }

    public ActivitesDto convertToDto(){
        ActivitesDto dto = new ActivitesDto();
        dto.setId_activites(this.getId_activite());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
