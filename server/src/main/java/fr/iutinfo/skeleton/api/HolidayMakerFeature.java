package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class HolidayMakerFeature
{
    private String featureHName;

    public HolidayMakerFeature(String featureHName)
    {
        this.featureHName = featureHName;
    }

    public HolidayMakerFeature(){
    }

    public String getFeatureHName()
    {
        return featureHName;
    }

    public void setFeatureHName(String featureHName)
    {
        this.featureHName = featureHName;
    }
}
