package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceInteret")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceInteretResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceInteretResource.class);
    private static PreferenceInteretDao dao = getDbi().open(PreferenceInteretDao.class);

    public PreferenceInteretResource() throws SQLException
    {
        if (!tableExist("preferenceInteret"))
        {
            logger.debug("Create table preferenceInteret");
            dao.createPreferenceInteret();
        }
    }

    @POST
    public Response insert(PreferenceInteret preferenceInteret){
        int s =  dao.insert(preferenceInteret);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceInteret> preferenceInteretList = dao.all();
        GenericEntity<List<PreferenceInteret>> entity = new GenericEntity<List<PreferenceInteret>>(preferenceInteretList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/interet/{id}")
    public Response findByIdInteret(@PathParam("id") int id){
        return Response.ok(dao.findByIdInteret(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceInteret preferenceInteret){
        preferenceInteret.setId_user(id_user);
        dao.update(preferenceInteret);
        return Response.ok().build();
    }

}
