package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceDestinationDao
{


    @SqlUpdate("create table preferenceDestination(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_destination foreign key(id) references destination(id_destination)," +
            "constraint pk_destination primary key (id_user, id_voyage, id))")
    void createPreferenceDestination();

    @SqlUpdate("insert into preferenceDestination(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceDestination preferenceDestination);

    @SqlQuery("select * from preferenceDestination")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceDestination> all();

    @SqlQuery("select * from preferenceDestination where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDestination findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceDestination where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDestination findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceDestination where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDestination findByIdDestination(@Bind("id") int id);

    @SqlUpdate("update preferenceDestination set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceDestination preferenceDestination);

    @SqlUpdate("delete from preferenceDestination where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
