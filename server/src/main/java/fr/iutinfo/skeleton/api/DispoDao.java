package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface DispoDao
{
    @SqlUpdate("create table dispo (id_dispo integer primary key autoincrement, libelle varchar(50))")
    void createDispoTable();

    @SqlUpdate("insert into dispo (libelle) values (:libelle)")
    int insert (@BindBean Dispo dispo);

    @SqlQuery("select * from dispo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Dispo> all();

    @SqlUpdate("update dispo set libelle = :libelle where id_dispo = :id_dispo")
    void update(@BindBean Dispo dispo);

    @SqlQuery("select * from dispo where id_dispo = :id_dispo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Dispo findById(@Bind("id_dispo") int id_dispo);

    @SqlQuery("select * from dispo where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Dispo findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from dispo where id_dispo = :id_dispo")
    void delete(@Bind("id_dispo") int id_dispo);

    @SqlUpdate("drop table if exists dispo")
    void dropDispoTable();

    void close();
}
