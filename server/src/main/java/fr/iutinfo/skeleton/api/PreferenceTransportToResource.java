package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceTransportTo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceTransportToResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceTransportToResource.class);
    private static PreferenceTransportToDao dao = getDbi().open(PreferenceTransportToDao.class);

    public PreferenceTransportToResource() throws SQLException
    {
        if (!tableExist("preferenceTransportTo"))
        {
            logger.debug("Create table preferenceTransportTo");
            dao.createPreferenceTransportTo();
        }
    }

    @POST
    public Response insert(PreferenceTransportTo preferenceTransportTo){
        int s =  dao.insert(preferenceTransportTo);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceTransportTo> preferenceTransportToList = dao.all();
        GenericEntity<List<PreferenceTransportTo>> entity = new GenericEntity<List<PreferenceTransportTo>>(preferenceTransportToList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/transportTo/{id}")
    public Response findByIdTransportTo(@PathParam("id") int id){
        return Response.ok(dao.findByIdTransportTo(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceTransportTo preferenceTransportTo){
        preferenceTransportTo.setId_user(id_user);
        dao.update(preferenceTransportTo);
        return Response.ok().build();
    }

}
