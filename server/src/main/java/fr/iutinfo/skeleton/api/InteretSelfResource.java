package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.InteretSelfDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/interetSelf")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class InteretSelfResource
{
    final static Logger logger = LoggerFactory.getLogger(InteretSelfResource.class);
    private static InteretSelfDao dao = getDbi().open(InteretSelfDao.class);

    @Context
    public UriInfo uriInfo;

    public InteretSelfResource() throws SQLException
    {
        if (!tableExist("interetSelf"))
        {
            logger.debug("Create table interetSelf");
            dao.createInteretSelfTable();
        }
    }

    @GET
    public List<InteretSelfDto> getAllInteretSelf(){
        List<InteretSelf> interetSelfList;
        interetSelfList = dao.all();
        return interetSelfList.stream().map(InteretSelf::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(InteretSelf interetSelf){
        if(dao.all().contains(interetSelf)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(interetSelf);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(interetSelf.getLibelle()).getId_interetSelf()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_interetSelf}")
    public Response findById(@PathParam("id_interetSelf") int id){
        InteretSelf a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        InteretSelf a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

