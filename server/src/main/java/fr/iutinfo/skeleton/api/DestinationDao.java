package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface DestinationDao
{
    @SqlUpdate("create table destination (id_destination integer primary key autoincrement, libelle varchar(50))")
    void createDestinationTable();

    @SqlUpdate("insert into destination (libelle) values (:libelle)")
    int insert (@BindBean Destination destination);

    @SqlQuery("select * from destination")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Destination> all();

    @SqlUpdate("update destination set libelle = :libelle where id_destination = :id_destination")
    void update(@BindBean Destination destination);

    @SqlQuery("select * from destination where id_destination = :id_destination")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Destination findById(@Bind("id_destination") int id_destination);

    @SqlQuery("select * from destination where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Destination findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from destination where id_destination = :id_destination")
    void delete(@Bind("id_destination") int id_destination);

    @SqlUpdate("drop table if exists destination")
    void dropDestinationTable();

    void close();
}
