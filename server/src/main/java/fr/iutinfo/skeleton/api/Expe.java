package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ExpeDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Expe
{
    private int id_expe;
    private String libelle;

    public Expe(){
    }

    public Expe(String libelle){
        this.libelle = libelle;
    }


    public int getId_expe()
    {
        return id_expe;
    }

    public void setId_expe(int id_expe)
    {
        this.id_expe = id_expe;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(ExpeDto dto){
        this.setId_expe(dto.getId_expe());
        this.setLibelle(dto.getLibelle());
    }

    public ExpeDto convertToDto(){
        ExpeDto dto = new ExpeDto();
        dto.setId_expe(this.getId_expe());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
