package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DispoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/dispo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DispoResource
{
    final static Logger logger = LoggerFactory.getLogger(DispoResource.class);
    private static DispoDao dao = getDbi().open(DispoDao.class);

    @Context
    public UriInfo uriInfo;

    public DispoResource() throws SQLException
    {
        if (!tableExist("dispo"))
        {
            logger.debug("Create table dispo");
            dao.createDispoTable();
        }
    }

    @GET
    public List<DispoDto> getAllDispo(){
        List<Dispo> dispoList;
        dispoList = dao.all();
        return dispoList.stream().map(Dispo::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Dispo dispo){
        if(dao.all().contains(dispo)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(dispo);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(dispo.getLibelle()).getId_dispo()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_dispo}")
    public Response findById(@PathParam("id_dispo") int id){
        Dispo a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Dispo a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

