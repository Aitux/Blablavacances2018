package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface EnvironnementDao
{
    @SqlUpdate("create table environnement (id_environnement integer primary key autoincrement, libelle varchar(50))")
    void createEnvironnementTable();

    @SqlUpdate("insert into environnement (libelle) values (:libelle)")
    int insert (@BindBean Environnement environnement);

    @SqlQuery("select * from environnement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Environnement> all();

    @SqlUpdate("update environnement set libelle = :libelle where id_environnement = :id_environnement")
    void update(@BindBean Environnement environnement);

    @SqlQuery("select * from environnement where id_environnement = :id_environnement")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Environnement findById(@Bind("id_environnement") int id_environnement);

    @SqlQuery("select * from environnement where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Environnement findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from environnement where id_environnement = :id_environnement")
    void delete(@Bind("id_environnement") int id_environnement);

    @SqlUpdate("drop table if exists environnement")
    void dropEnvironnementTable();

    void close();
}
