package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/preferenceHebergement")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PreferenceHebergementResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(PreferenceHebergementResource.class);
    private static PreferenceHebergementDao dao = getDbi().open(PreferenceHebergementDao.class);

    public PreferenceHebergementResource() throws SQLException
    {
        if (!tableExist("preferenceHebergement"))
        {
            logger.debug("Create table preferenceHebergement");
            dao.createPreferenceHebergement();
        }
    }

    @POST
    public Response insert(PreferenceHebergement preferenceHebergement){
        int s =  dao.insert(preferenceHebergement);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<PreferenceHebergement> preferenceHebergementList = dao.all();
        GenericEntity<List<PreferenceHebergement>> entity = new GenericEntity<List<PreferenceHebergement>>(preferenceHebergementList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }

    @GET
    @Path("/voyage/{id_voyage}")
    public Response findByIdVoyage(@PathParam("id_voyage") int id_voyage){
        return Response.ok(dao.findByIdVoyage(id_voyage)).build();
    }

    @GET
    @Path("/hebergement/{id}")
    public Response findByIdHebergement(@PathParam("id") int id){
        return Response.ok(dao.findByIdHebergement(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, PreferenceHebergement preferenceHebergement){
        preferenceHebergement.setId_user(id_user);
        dao.update(preferenceHebergement);
        return Response.ok().build();
    }

}
