package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteDataSource;

import javax.inject.Singleton;
import javax.swing.filechooser.FileSystemView;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

@Singleton
public class BDDFactory
{
    final static Logger logger = LoggerFactory.getLogger(BDDFactory.class);
    private static DBI dbi = null;

    public static DBI getDbi()
    {
        if (dbi == null)
        {
            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl("jdbc:sqlite:" + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "data.db");
            dbi = new DBI(ds);
            logger.debug("user.dir : " + System.getProperty("user.dir"));
            logger.debug("database directory: " + FileSystemView.getFileSystemView().getHomeDirectory() + System.getProperty("file.separator") + "data.db");
        }
        return dbi;
    }

    static boolean tableExist(String tableName) throws SQLException
    {
        DatabaseMetaData dbm = getDbi().open().getConnection().getMetaData();
        ResultSet tables = dbm.getTables(null, null, tableName, null);
        boolean exist = tables.next();
        tables.close();
        return exist;
    }
}