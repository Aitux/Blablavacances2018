package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;


/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
@Path("/userInteretSelf")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserInteretSelfResource
{
    @Context
    public UriInfo uriInfo;

    final static Logger logger = LoggerFactory.getLogger(UserInteretSelfResource.class);
    private static UserInteretSelfDao dao = getDbi().open(UserInteretSelfDao.class);

    public UserInteretSelfResource() throws SQLException
    {
        if (!tableExist("preferenceInteretSelf"))
        {
            logger.debug("Create table userInteretSelf");
            dao.createUserInteretSelf();
        }
    }

    @POST
    public Response insert(UserInteretSelf userInteretSelf){
        int s =  dao.insert(userInteretSelf);
        if(s == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    public Response all(){
        List<UserInteretSelf> userInteretSelfList = dao.all();
        GenericEntity<List<UserInteretSelf>> entity = new GenericEntity<List<UserInteretSelf>>(userInteretSelfList) {};
        return Response.ok(entity).build();
    }

    @GET
    @Path("/user/{id_user}")
    public Response findByIdUser(@PathParam("id_user") int id_user){
        return Response.ok(dao.findByIdUser(id_user)).build();
    }


    @GET
    @Path("/interet/{id}")
    public Response findByIdInteret(@PathParam("id") int id){
        return Response.ok(dao.findByIdInteret(id)).build();
    }

    @PUT
    @Path("/update/{id_user}")
    public Response update(@PathParam("id_user") int id_user, UserInteretSelf userInteretSelf){
        userInteretSelf.setId_user(id_user);
        dao.update(userInteretSelf);
        return Response.ok().build();
    }

}
