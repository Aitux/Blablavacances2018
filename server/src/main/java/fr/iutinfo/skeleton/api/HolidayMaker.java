package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class HolidayMaker
{
    private int idHolidayMaker;

    public HolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }

    public HolidayMaker(){

    }

    public int getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }
}
