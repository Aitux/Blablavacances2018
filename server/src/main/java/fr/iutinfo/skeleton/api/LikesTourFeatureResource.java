package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/likesTourFeature")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LikesTourFeatureResource
{
    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.LikesTourFeatureResource.class);
    private static LikesTourFeatureDao dao = getDbi().open(LikesTourFeatureDao.class);


    @Context
    UriInfo uriInfo;

    public LikesTourFeatureResource() throws SQLException
    {
        if (!tableExist("likesTourFeature"))
        {
            logger.debug("Create LikesTourFeature table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<LikesTourFeature> list = dao.all();
        GenericEntity<List<LikesTourFeature>> entity = new GenericEntity<List<LikesTourFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(LikesTourFeature likesTourFeature)
    {
        int i = dao.insert(likesTourFeature);
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{idHolidayMaker}")
    public Response findByHolidayMaker(@PathParam("idHolidayMaker") int idHolidayMaker)
    {
        List<LikesTourFeature> list = dao.findByHolidayMakerId(idHolidayMaker);
        GenericEntity<List<LikesTourFeature>> entity = new GenericEntity<List<LikesTourFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/feature/{featureTName}")
    public Response findByFeatureTName(@PathParam("featureTName") String featureTName)
    {
        List<LikesTourFeature> list = dao.findByFeatureTName(featureTName);
        GenericEntity<List<LikesTourFeature>> entity = new GenericEntity<List<LikesTourFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/tourFeatureValue/{tourFeatureValue}")
    public Response findByTourFeatureValue(@PathParam("tourFeatureValue") String tourFeatureValue){
        List<LikesTourFeature> list = dao.findByTourFeatureValue(tourFeatureValue);
        GenericEntity<List<LikesTourFeature>> entity = new GenericEntity<List<LikesTourFeature>>(list)
        {
        };
        return Response.ok(entity).build();
    }
}
