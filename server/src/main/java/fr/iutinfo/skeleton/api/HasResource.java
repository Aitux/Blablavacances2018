package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/has")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HasResource
{

    final static Logger logger = LoggerFactory.getLogger(HasResource.class);
    private static HasDao dao = getDbi().open(HasDao.class);


    @Context
    UriInfo uriInfo;

    public HasResource() throws SQLException
    {
        if (!tableExist("has"))
        {
            logger.debug("Create has table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<Has> list = dao.all();
        GenericEntity<List<Has>> entity = new GenericEntity<List<Has>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(Has has){
        int i = dao.insert(has);
        if(i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/feature/{featureHName}")
    public Response findByFeatureHName(@PathParam("featureHName") String featureHName){
        List<Has> list = dao.findByFeatureHName(featureHName);
        GenericEntity<List<Has>> entity = new GenericEntity<List<Has>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/{holidayMakerFeatureValue}")
    public Response findByHolidayMakerFeatureValue(@PathParam("holidayMakerFeatureValue") String holidayMakerFeatureValue)
    {
        List<Has> list = dao.findByHolidayMakerFeatureValue(holidayMakerFeatureValue);
        GenericEntity<List<Has>> entity = new GenericEntity<List<Has>>(list)
        {
        };
        return Response.ok(entity).build();
    }


}
