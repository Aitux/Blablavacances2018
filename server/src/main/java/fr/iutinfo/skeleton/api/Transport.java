package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.TransportDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Transport
{
    private int id_transport;
    private String libelle;

    public Transport(){
    }

    public Transport(String libelle){
        this.libelle = libelle;
    }


    public int getId_transport()
    {
        return id_transport;
    }

    public void setId_transport(int id_transport)
    {
        this.id_transport = id_transport;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(TransportDto dto){
        this.setId_transport(dto.getId_transport());
        this.setLibelle(dto.getLibelle());
    }

    public TransportDto convertToDto(){
        TransportDto dto = new TransportDto();
        dto.setId_transport(this.getId_transport());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
