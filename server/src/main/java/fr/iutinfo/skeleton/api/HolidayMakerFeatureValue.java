package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class HolidayMakerFeatureValue
{
    private String holidayMakerFeatureValue;

    public HolidayMakerFeatureValue(String holidayMakerFeatureValue)
    {
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

    public HolidayMakerFeatureValue(){
    }

    public String getHolidayMakerFeatureValue()
    {
        return holidayMakerFeatureValue;
    }

    public void setHolidayMakerFeatureValue(String holidayMakerFeatureValue)
    {
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

}
