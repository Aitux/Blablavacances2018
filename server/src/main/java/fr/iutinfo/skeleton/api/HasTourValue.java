package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class HasTourValue
{
    private String tourFeatureValue;
    private String featureTName;

    public HasTourValue(String tourFeatureValue, String featureTName)
    {
        this.tourFeatureValue = tourFeatureValue;
        this.featureTName = featureTName;
    }

    public HasTourValue(){}

    public String getTourFeatureValue()
    {
        return tourFeatureValue;
    }

    public void setTourFeatureValue(String tourFeatureValue)
    {
        this.tourFeatureValue = tourFeatureValue;
    }

    public String getFeatureTName()
    {
        return featureTName;
    }

    public void setFeatureTName(String featureTName)
    {
        this.featureTName = featureTName;
    }
}
