package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface InteretDao
{
    @SqlUpdate("create table interet (id_interet integer primary key autoincrement, libelle varchar(50))")
    void createInteretTable();

    @SqlUpdate("insert into interet (libelle) values (:libelle)")
    int insert (@BindBean Interet interet);

    @SqlQuery("select * from interet")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Interet> all();

    @SqlUpdate("update interet set libelle = :libelle where id_interet = :id_interet")
    void update(@BindBean Interet interet);

    @SqlQuery("select * from interet where id_interet = :id_interet")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Interet findById(@Bind("id_interet") int id_interet);

    @SqlQuery("select * from interet where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Interet findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from interet where id_interet = :id_interet")
    void delete(@Bind("id_interet") int id_interet);

    @SqlUpdate("drop table if exists interet")
    void dropInteretTable();

    void close();
}
