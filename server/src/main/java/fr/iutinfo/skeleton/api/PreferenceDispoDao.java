package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceDispoDao
{


    @SqlUpdate("create table preferenceDispo(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_dispo foreign key(id) references dispo(id_dispo)," +
            "constraint pk_dispo primary key (id_user, id_voyage, id))")
    void createPreferenceDispo();

    @SqlUpdate("insert into preferenceDispo(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceDispo preferenceDispo);

    @SqlQuery("select * from preferenceDispo")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceDispo> all();

    @SqlQuery("select * from preferenceDispo where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDispo findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceDispo where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDispo findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceDispo where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceDispo findByIdDispo(@Bind("id") int id);

    @SqlUpdate("update preferenceDispo set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceDispo preferenceDispo);

    @SqlUpdate("delete from preferenceDispo where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
