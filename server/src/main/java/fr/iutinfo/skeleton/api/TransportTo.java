package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.TransportToDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class TransportTo
{
    private int id_transportTo;
    private String libelle;

    public TransportTo(){
    }

    public TransportTo(String libelle){
        this.libelle = libelle;
    }


    public int getId_transportTo()
    {
        return id_transportTo;
    }

    public void setId_transportTo(int id_transportTo)
    {
        this.id_transportTo = id_transportTo;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(TransportToDto dto){
        this.setId_transportTo(dto.getId_transportTo());
        this.setLibelle(dto.getLibelle());
    }

    public TransportToDto convertToDto(){
        TransportToDto dto = new TransportToDto();
        dto.setId_transportTo(this.getId_transportTo());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
