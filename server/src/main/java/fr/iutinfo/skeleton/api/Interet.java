package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.InteretDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
@XmlRootElement
public class Interet
{
    private int id_interet;
    private String libelle;

    public Interet(){
    }

    public Interet(String libelle){
        this.libelle = libelle;
    }


    public int getId_interet()
    {
        return id_interet;
    }

    public void setId_interet(int id_interet)
    {
        this.id_interet = id_interet;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(InteretDto dto){
        this.setId_interet(dto.getId_interet());
        this.setLibelle(dto.getLibelle());
    }

    public InteretDto convertToDto(){
        InteretDto dto = new InteretDto();
        dto.setId_interet(this.getId_interet());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
