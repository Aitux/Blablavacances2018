package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceActivitesDao
{


    @SqlUpdate("create table preferenceActivite(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_activite foreign key(id) references activite(id_activite)," +
            "constraint pk_activite primary key (id_user, id_voyage, id))")
    void createPreferenceActivite();

    @SqlUpdate("insert into preferenceActivite(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceActivites preferenceActivites);

    @SqlQuery("select * from preferenceActivite")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceActivites> all();

    @SqlQuery("select * from preferenceActivite where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceActivites findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceActivite where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceActivites findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceActivite where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceActivites findByIdActivite(@Bind("id") int id);

    @SqlUpdate("update preferenceActivite set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceActivites preferenceActivites);

    @SqlUpdate("delete from preferenceActivite where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
