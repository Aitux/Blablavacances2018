package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface ProfessionDao
{
    @SqlUpdate("create table profession (id_profession integer primary key autoincrement, libelle varchar(50))")
    void createProfessionTable();

    @SqlUpdate("insert into profession (libelle) values (:libelle)")
    int insert (@BindBean Profession profession);

    @SqlQuery("select * from profession")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Profession> all();

    @SqlUpdate("update profession set libelle = :libelle where id_profession = :id_profession")
    void update(@BindBean Profession profession);

    @SqlQuery("select * from profession where id_profession = :id_profession")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Profession findById(@Bind("id_profession") int id_profession);

    @SqlQuery("select * from profession where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Profession findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from profession where id_profession = :id_profession")
    void delete(@Bind("id_profession") int id_profession);

    @SqlUpdate("drop table if exists profession")
    void dropProfessionTable();

    void close();
}
