package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.InteretSelfDto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 26/04/18.
 */
@XmlRootElement
public class InteretSelf
{
    private int id_interetSelf;
    private String libelle;

    public InteretSelf(){
    }

    public InteretSelf(String libelle){
        this.libelle = libelle;
    }


    public int getId_interetSelf()
    {
        return id_interetSelf;
    }

    public void setId_interetSelf(int id_interetSelf)
    {
        this.id_interetSelf = id_interetSelf;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void initFromDto(InteretSelfDto dto){
        this.setId_interetSelf(dto.getId_interetSelf());
        this.setLibelle(dto.getLibelle());
    }

    public InteretSelfDto convertToDto(){
        InteretSelfDto dto = new InteretSelfDto();
        dto.setId_interetSelf(this.getId_interetSelf());
        dto.setLibelle(this.getLibelle());
        return dto;
    }
}
