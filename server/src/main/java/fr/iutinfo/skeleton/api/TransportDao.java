package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface TransportDao
{
    @SqlUpdate("create table transport (id_transport integer primary key autoincrement, libelle varchar(50))")
    void createTransportTable();

    @SqlUpdate("insert into transport (libelle) values (:libelle)")
    int insert (@BindBean Transport transport);

    @SqlQuery("select * from transport")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Transport> all();

    @SqlUpdate("update transport set libelle = :libelle where id_transport = :id_transport")
    void update(@BindBean Transport transport);

    @SqlQuery("select * from transport where id_transport = :id_transport")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Transport findById(@Bind("id_transport") int id_transport);

    @SqlQuery("select * from transport where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Transport findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from transport where id_transport = :id_transport")
    void delete(@Bind("id_transport") int id_transport);

    @SqlUpdate("drop table if exists transport")
    void dropTransportTable();

    void close();
}
