package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ProfessionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/profession")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfessionResource
{
    final static Logger logger = LoggerFactory.getLogger(ProfessionResource.class);
    private static ProfessionDao dao = getDbi().open(ProfessionDao.class);

    @Context
    public UriInfo uriInfo;

    public ProfessionResource() throws SQLException
    {
        if (!tableExist("profession"))
        {
            logger.debug("Create table profession");
            dao.createProfessionTable();
        }
    }

    @GET
    public List<ProfessionDto> getAllProfession(){
        List<Profession> professionList;
        professionList = dao.all();
        return professionList.stream().map(Profession::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Profession profession){
        if(dao.all().contains(profession)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(profession);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(profession.getLibelle()).getId_profession()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_profession}")
    public Response findById(@PathParam("id_profession") int id){
        Profession a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Profession a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

