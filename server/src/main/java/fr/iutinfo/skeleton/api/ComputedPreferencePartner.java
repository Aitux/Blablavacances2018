package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class ComputedPreferencePartner
{
    private int score;
    private int idHolidayMaker1;
    private int idHolidayMaker2;

    public ComputedPreferencePartner(int score, int idHolidayMaker1, int idHolidayMaker2)
    {
        this.score = score;
        this.idHolidayMaker1 = idHolidayMaker1;
        this.idHolidayMaker2 = idHolidayMaker2;
    }

    public ComputedPreferencePartner(){
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    public int getIdHolidayMaker1()
    {
        return idHolidayMaker1;
    }

    public void setIdHolidayMaker1(int idHolidayMaker1)
    {
        this.idHolidayMaker1 = idHolidayMaker1;
    }

    public int getIdHolidayMaker2()
    {
        return idHolidayMaker2;
    }

    public void setIdHolidayMaker2(int idHolidayMaker2)
    {
        this.idHolidayMaker2 = idHolidayMaker2;
    }
}
