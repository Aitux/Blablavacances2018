package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public interface PreferenceInteretDao
{


    @SqlUpdate("create table preferenceInteret(id_user integer, id_voyage integer, id integer, value integer," +
            "constraint fk_user foreign key(id_user) references users(id)," +
            "constraint fk_voyage foreign key(id_voyage) references voyage(id_voyage)," +
            "constraint fk_interet foreign key(id) references interet(id_interet)," +
            "constraint pk_interet primary key (id_user, id_voyage, id))")
    void createPreferenceInteret();

    @SqlUpdate("insert into preferenceInteret(id_user, id_voyage, id, value) values (:id_user, :id_voyage, :id, :value)")
    int insert(@BindBean() PreferenceInteret preferenceInteret);

    @SqlQuery("select * from preferenceInteret")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<PreferenceInteret> all();

    @SqlQuery("select * from preferenceInteret where id_user = :id_user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceInteret findByIdUser(@Bind("id_user") int id_user);

    @SqlQuery("select * from preferenceInteret where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceInteret findByIdVoyage(@Bind("id_voyage") int id_voyage);

    @SqlQuery("select * from preferenceInteret where id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    PreferenceInteret findByIdInteret(@Bind("id") int id);

    @SqlUpdate("update preferenceInteret set value = :value where id_user = :id_user")
    void update(@BindBean() PreferenceInteret preferenceInteret);

    @SqlUpdate("delete from preferenceInteret where id_user = :id_user")
    void delete(@Bind("id_user") int id_user);

    void close();
}
