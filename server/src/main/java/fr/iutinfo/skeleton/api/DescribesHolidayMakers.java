package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class DescribesHolidayMakers
{

    private float idHolidayMaker;
    private String featureHName;
    private String holidayMakerFeatureValue;

    public DescribesHolidayMakers(int idHolidayMaker, String featureHName, String holidayMakerFeatureValue)
    {
        this.idHolidayMaker = idHolidayMaker;
        this.featureHName = featureHName;
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

    public DescribesHolidayMakers(){
    }

    public String getFeatureHName()
    {
        return featureHName;
    }

    public void setFeatureHName(String featureHName)
    {
        this.featureHName = featureHName;
    }

    public String getHolidayMakerFeatureValue()
    {
        return holidayMakerFeatureValue;
    }

    public void setHolidayMakerFeatureValue(String holidayMakerFeatureValue)
    {
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

    public float getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(float idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }
}
