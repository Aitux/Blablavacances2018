package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class Has
{
    private String featureHName;
    private String holidayMakerFeatureValue;

    public Has(String featureHName, String holidayMakerFeatureValue)
    {
        this.featureHName = featureHName;
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }

    public Has(){}

    public String getFeatureHName()
    {
        return featureHName;
    }

    public void setFeatureHName(String featureHName)
    {
        this.featureHName = featureHName;
    }

    public String getHolidayMakerFeatureValue()
    {
        return holidayMakerFeatureValue;
    }

    public void setHolidayMakerFeatureValue(String holidayMakerFeatureValue)
    {
        this.holidayMakerFeatureValue = holidayMakerFeatureValue;
    }


}
