package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.DestinationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/destination")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DestinationResource
{
    final static Logger logger = LoggerFactory.getLogger(DestinationResource.class);
    private static DestinationDao dao = getDbi().open(DestinationDao.class);

    @Context
    public UriInfo uriInfo;

    public DestinationResource() throws SQLException
    {
        if (!tableExist("destination"))
        {
            logger.debug("Create table destination");
            dao.createDestinationTable();
        }
    }

    @GET
    public List<DestinationDto> getAllDestination(){
        List<Destination> destinationList;
        destinationList = dao.all();
        return destinationList.stream().map(Destination::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Destination destination){
        if(dao.all().contains(destination)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(destination);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(destination.getLibelle()).getId_destination()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_destination}")
    public Response findById(@PathParam("id_destination") int id){
        Destination a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Destination a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

