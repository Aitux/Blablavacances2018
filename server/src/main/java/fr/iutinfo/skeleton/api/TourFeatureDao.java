package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface TourFeatureDao
{
    @SqlUpdate("create table TourFeature(" +
            "featureTName text PRIMARY KEY" +
            ")")
    void createTable();

    @SqlUpdate("insert into TourFeature(featureTName) values(:featureTName)")
    int insert(@BindBean() TourFeature tourFeature);

    @SqlQuery("select * from TourFeature")
    List<TourFeature> all();

    @SqlQuery("select * from TourFeature where featureTName = :featureTName")
    TourFeature findByFeatureTName(@Bind("featureTName") String featureTName);
}
