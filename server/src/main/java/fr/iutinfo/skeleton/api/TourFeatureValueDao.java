package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface TourFeatureValueDao
{
    @SqlUpdate("create table TourFeatureValue(" +
            "tourFeatureValue text PRIMARY KEY" +
            ")")
    void createTable();

    @SqlUpdate("insert into TourFeatureValue(tourFeatureValue) values(:tourFeatureValue)")
    int insert(@BindBean() TourFeatureValue tourFeatureValue);

    @SqlQuery("select * from TourFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<TourFeatureValue> all();

    @SqlQuery("select * from TourFeatureValue where tourFeatureValue=:tourFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    TourFeatureValue findByTourFeatureValue(@Bind("tourFeatureValue") String tourFeatureValue);
}
