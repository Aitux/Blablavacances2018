package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.TransportDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/transport")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransportResource
{
    final static Logger logger = LoggerFactory.getLogger(TransportResource.class);
    private static TransportDao dao = getDbi().open(TransportDao.class);

    @Context
    public UriInfo uriInfo;

    public TransportResource() throws SQLException
    {
        if (!tableExist("transport"))
        {
            logger.debug("Create table transport");
            dao.createTransportTable();
        }
    }

    @GET
    public List<TransportDto> getAllTransport(){
        List<Transport> transportList;
        transportList = dao.all();
        return transportList.stream().map(Transport::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Transport transport){
        if(dao.all().contains(transport)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(transport);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(transport.getLibelle()).getId_transport()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_transport}")
    public Response findById(@PathParam("id_transport") int id){
        Transport a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Transport a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

