package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.EnvironnementDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/environnement")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EnvironnementResource
{
    final static Logger logger = LoggerFactory.getLogger(EnvironnementResource.class);
    private static EnvironnementDao dao = getDbi().open(EnvironnementDao.class);

    @Context
    public UriInfo uriInfo;

    public EnvironnementResource() throws SQLException
    {
        if (!tableExist("environnement"))
        {
            logger.debug("Create table environnement");
            dao.createEnvironnementTable();
        }
    }

    @GET
    public List<EnvironnementDto> getAllEnvironnement(){
        List<Environnement> environnementList;
        environnementList = dao.all();
        return environnementList.stream().map(Environnement::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(Environnement environnement){
        if(dao.all().contains(environnement)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(environnement);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(environnement.getLibelle()).getId_environnement()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_environnement}")
    public Response findById(@PathParam("id_environnement") int id){
        Environnement a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        Environnement a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

