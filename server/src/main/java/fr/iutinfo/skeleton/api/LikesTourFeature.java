package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class LikesTourFeature
{
    private int attractionTourFeature;
    private String featureTName;
    private String tourFeatureValue;
    private int idHolidayMaker;

    public LikesTourFeature(int attractionTourFeature, String featureTName, String tourFeatureValue, int idHolidayMaker)
    {
        this.attractionTourFeature = attractionTourFeature;
        this.featureTName = featureTName;
        this.tourFeatureValue = tourFeatureValue;
        this.idHolidayMaker = idHolidayMaker;
    }

    public LikesTourFeature(){}

    public int getAttractionTourFeature()
    {
        return attractionTourFeature;
    }

    public void setAttractionTourFeature(int attractionTourFeature)
    {
        this.attractionTourFeature = attractionTourFeature;
    }

    public String getFeatureTName()
    {
        return featureTName;
    }

    public void setFeatureTName(String featureTName)
    {
        this.featureTName = featureTName;
    }

    public String getTourFeatureValue()
    {
        return tourFeatureValue;
    }

    public void setTourFeatureValue(String tourFeatureValue)
    {
        this.tourFeatureValue = tourFeatureValue;
    }

    public int getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }
}
