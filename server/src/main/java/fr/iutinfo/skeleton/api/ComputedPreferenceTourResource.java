package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/computedPreferenceTour")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ComputedPreferenceTourResource
{
    final static Logger logger = LoggerFactory.getLogger(ComputedPreferenceTourResource.class);
    private static ComputedPreferenceTourDao dao = getDbi().open(ComputedPreferenceTourDao.class);

    @Context
    UriInfo uriInfo;

    public ComputedPreferenceTourResource() throws SQLException
    {
        if (!tableExist("computedPreferenceTour"))
        {
            logger.debug("Create table computedPreferenceTour");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<ComputedPreferenceTour> list = dao.all();
        GenericEntity<List<ComputedPreferenceTour>> entity = new GenericEntity<List<ComputedPreferenceTour>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @GET
    @Path("/{idHolidayMaker1}/{idTour}")
    public Response findById(@PathParam("idHolidayMaker1") int idHolidayMaker1, @PathParam("idTour") int idTour)
    {
        List<ComputedPreferenceTour> list = dao.findById(idHolidayMaker1, idTour);
        GenericEntity<List<ComputedPreferenceTour>> entity = new GenericEntity<List<ComputedPreferenceTour>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert(ComputedPreferenceTour computedPreferenceTour){
        int i = dao.insert(computedPreferenceTour);
        if(i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @PUT
    public Response update(ComputedPreferenceTour computedPreferenceTour){
        int i = dao.update(computedPreferenceTour);
        if(i == 0)
            return Response.notModified().build();
        else
            return Response.ok(uriInfo.getAbsolutePath()).build();
    }

}
