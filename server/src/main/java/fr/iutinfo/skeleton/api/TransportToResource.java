package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.TransportToDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */

@Path("/transportTo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransportToResource
{
    final static Logger logger = LoggerFactory.getLogger(TransportToResource.class);
    private static TransportToDao dao = getDbi().open(TransportToDao.class);

    @Context
    public UriInfo uriInfo;

    public TransportToResource() throws SQLException
    {
        if (!tableExist("transportTo"))
        {
            logger.debug("Create table transportTo");
            dao.createTransportToTable();
        }
    }

    @GET
    public List<TransportToDto> getAllTransportTo(){
        List<TransportTo> transportToList;
        transportToList = dao.all();
        return transportToList.stream().map(TransportTo::convertToDto).collect(Collectors.toList());
    }

    @POST
    public Response insert(TransportTo transportTo){
        if(dao.all().contains(transportTo)){
            return Response.status(Response.Status.CONFLICT).build();
        }else{
            dao.insert(transportTo);
            URI instanceURI = uriInfo.getAbsolutePathBuilder().path("" + dao.findByLibelle(transportTo.getLibelle()).getId_transportTo()).build();
            return Response.created(instanceURI).build();
        }
    }

    @GET
    @Path("/id/{id_transportTo}")
    public Response findById(@PathParam("id_transportTo") int id){
        TransportTo a = dao.findById(id);
        if (a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

    @GET
    @Path("/libelle/{libelle}")
    public Response findByLibelle(@PathParam("libelle") String libelle){
        TransportTo a = dao.findByLibelle(libelle);
        if(a == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        else
            return Response.ok(a).build();
    }

}

