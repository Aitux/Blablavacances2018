package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface HasTourValueDao
{
    @SqlUpdate("create table hasTourValue(" +
            "tourFeatureValue text ON DELETE CASCADE," +
            "featureTName text ON DELETE CASCADE," +
            "constraint fk_tourFeatureValueHasTourValue(tourFeatureValue) references TourFeature(featureTName)," +
            "constraint fk_featureTNameHasTourValue(featureTName) references TourFeatureValue(tourFeatureValue)," +
            "constraint pk_hasTourValue primary key(tourFeatureValue, featureTName))")
    void createTable();

    @SqlUpdate("insert into hasTourValue(tourFeatureValue, featureTName) values (:tourFeatureValue, :featureTName)")
    int insert (@BindBean() HasTourValue hasTourValue);

    @SqlQuery("select * from hasTourValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HasTourValue> all();

    @SqlQuery("select * from hasTourValue where tourFeatureValue = :tourFeatureValue")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HasTourValue> findByTourFeatureValue(@Bind("tourFeatureValue") String tourFeatureValue);

    @SqlQuery("select * from hasTourValue where featureTName = :featureTName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<HasTourValue> findByFeatureTName(@Bind("featureTName") String featureTName);

    void close();
}
