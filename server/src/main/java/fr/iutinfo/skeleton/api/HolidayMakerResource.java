package fr.iutinfo.skeleton.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.sql.SQLException;
import java.util.List;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
@Path("/holidayMaker")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HolidayMakerResource
{

    final static Logger logger = LoggerFactory.getLogger(fr.iutinfo.skeleton.api.HolidayMakerResource.class);
    private static HolidayMakerDao dao = getDbi().open(HolidayMakerDao.class);


    @Context
    UriInfo uriInfo;

    public HolidayMakerResource() throws SQLException
    {
        if (!tableExist("HolidayMaker"))
        {
            logger.debug("Create HolidayMaker table");
            dao.createTable();
        }
    }

    @GET
    public Response all()
    {
        List<HolidayMaker> list = dao.all();
        GenericEntity<List<HolidayMaker>> entity = new GenericEntity<List<HolidayMaker>>(list)
        {
        };
        return Response.ok(entity).build();
    }

    @POST
    public Response insert()
    {
        int i = dao.insert();
        if (i == 0)
            return Response.notModified().build();
        else
            return Response.created(uriInfo.getAbsolutePath()).build();
    }

    @GET
    @Path("/{idHolidayMaker}")
    public Response findById(@PathParam("idHolidayMaker") int idHolidayMaker)
    {
        HolidayMaker holidayMaker = dao.findById(idHolidayMaker);
        GenericEntity<HolidayMaker> entity = new GenericEntity<HolidayMaker>(holidayMaker)
        {
        };
        return Response.ok(entity).build();
    }
}
