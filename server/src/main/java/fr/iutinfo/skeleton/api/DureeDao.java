package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 18/04/18.
 */
public interface DureeDao
{
    @SqlUpdate("create table duree (id_duree integer primary key autoincrement, libelle varchar(50))")
    void createDureeTable();

    @SqlUpdate("insert into duree (libelle) values (:libelle)")
    int insert (@BindBean Duree duree);

    @SqlQuery("select * from duree")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Duree> all();

    @SqlUpdate("update duree set libelle = :libelle where id_duree = :id_duree")
    void update(@BindBean Duree duree);

    @SqlQuery("select * from duree where id_duree = :id_duree")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Duree findById(@Bind("id_duree") int id_duree);

    @SqlQuery("select * from duree where libelle = :libelle")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Duree findByLibelle(@Bind("libelle") String libelle);

    @SqlUpdate("delete from duree where id_duree = :id_duree")
    void delete(@Bind("id_duree") int id_duree);

    @SqlUpdate("drop table if exists duree")
    void dropDureeTable();

    void close();
}
