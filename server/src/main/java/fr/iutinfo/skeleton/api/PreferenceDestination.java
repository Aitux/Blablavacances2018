package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 23/04/18.
 */
public class PreferenceDestination
{
    private int id_user;
    private int id_voyage;
    private int id;
    private int value;

    public PreferenceDestination(){
    }

    public PreferenceDestination(int id_user, int id_voyage, int id, int value)
    {
        this.id_user = id_user;
        this.id_voyage = id_voyage;
        this.id = id;
        this.value = value;
    }



    public int getId_user()
    {
        return id_user;
    }

    public void setId_user(int id_user)
    {
        this.id_user = id_user;
    }

    public int getId_voyage()
    {
        return id_voyage;
    }

    @Override
    public String toString()
    {
        return "PreferenceDestination{" +
                "id_user=" + id_user +
                ", id_voyage=" + id_voyage +
                ", id=" + id +
                ", value=" + value +
                '}';
    }

    public void setId_voyage(int id_voyage)
    {
        this.id_voyage = id_voyage;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }


}
