package fr.iutinfo.skeleton.api;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public class DescribesTour
{
    private String featureTName;
    private int idTour;
    private int idHolidayMaker;

    public DescribesTour(String featureTName, int idTour, int idHolidayMaker)
    {
        this.idTour = idTour;
        this.idHolidayMaker = idHolidayMaker;
        this.featureTName = featureTName;
    }

    public DescribesTour(){}

    public String getFeatureTName()
    {
        return featureTName;
    }

    public void setFeatureTName(String featureTName)
    {
        this.featureTName = featureTName;
    }

    public int getIdTour()
    {
        return idTour;
    }

    public void setIdTour(int idTour)
    {
        this.idTour = idTour;
    }

    public int getIdHolidayMaker()
    {
        return idHolidayMaker;
    }

    public void setIdHolidayMaker(int idHolidayMaker)
    {
        this.idHolidayMaker = idHolidayMaker;
    }
}
