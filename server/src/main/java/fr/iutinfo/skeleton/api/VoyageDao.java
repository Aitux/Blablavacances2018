package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface VoyageDao {

    @SqlUpdate("create table voyage (id_voyage integer primary key autoincrement, id_createur integer ,budget integer," +
            " route_seul integer, nb_co_vac varchar(15), description varchar(500)," +
            " sexe_co_vac integer, age_co_vac varchar(15), etude_co_vac varchar(15), fumeur_co_vac integer, animal_de_compagnie_co_vac integer, " +
            "Constraint FK_voyage_user_id foreign key(id_createur) references users(id))")
    void createTableVoyage();

    @SqlUpdate("insert into voyage (id_createur, budget, route_seul, nb_co_vac, sexe_co_vac, age_co_vac, etude_co_vac, fumeur_co_vac, animal_de_compagnie_co_vac, description)" +
            " values (:id_createur, :budget, :route_seul, :nb_co_vac, :sexe_co_vac, :age_co_vac, :etude_co_vac, :fumeur_co_vac, :animal_de_compagnie_co_vac, :description)")
    @GetGeneratedKeys
    int insert(@BindBean() Voyage voyage);

    @SqlQuery("select * from voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Voyage> all();

    @SqlQuery("select * from voyage where id_voyage = :id_voyage")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Voyage findById(@Bind("id_voyage") int id_voyage);

    @SqlUpdate("delete from voyage where id_voyage = :id_voyage")
    int delete(@Bind("id_voyage") int id_voyage);

    @SqlUpdate("drop table if exists voyage")
    void dropVoyageTable();

    @SqlQuery("select * from voyage where id_createur = :id_createur")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Voyage> findByCreateur(@Bind("id_createur") int id_createur);

    @SqlUpdate("update voyage set budget=:budget, route_seul=:route_seul, nb_co_vac=:nb_co_vac, etude_co_vac=:etude_co_vac, fumeur_co_vac=:fumeur_co_vac, animal_de_compagnie_co_vac=:animal_de_compagnie_co_vac, description=:description where id_voyage = :id_voyage")
    int update(@BindBean() Voyage voyage);

    void close();

}
