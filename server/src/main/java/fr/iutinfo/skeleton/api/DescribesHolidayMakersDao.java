package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

/**
 * Project : agile-skeleton
 * Original Package : fr.iutinfo.skeleton.api
 * Created by aitux on 11/05/18.
 */
public interface DescribesHolidayMakersDao
{
    @SqlUpdate("create table describesHolidayMakers (" +
            "idHolidayMaker integer ON DELETE CASCADE," +
            "featureHName text ON DELETE CASCADE," +
            "holidayMakerFeatureValue text ON DELETE CASCADE," +
            "constraint fk_idHolidayMaker(idHolidayMaker) references HolidayMaker(idHolidayMaker)," +
            "constraint fk_featureHName(featureHName) references HolidayMakerFeature(featureHName)," +
            "constraint fk_holidayMakerFeatureValue(holidayMakerFeatureValue) references HolidayMakerFeatureValue(holidayMakerFeatureValue)," +
            "constraint pk_describesHolidayMakers primary key(idHolidayMaker, featureHName, holidayMakerFeatureValue))")
    void createTable();

    @SqlUpdate("insert into describesHolidayMakers(idHolidayMaker, featureHName, holidayMakerFeatureValue) values(:idHolidayMaker, :featureHName, :holidayMakerFeatureValue)")
    int insert(@BindBean() DescribesHolidayMakers describesHolidayMakers);

    @SqlQuery("select * from describesHolidayMakers")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesHolidayMakers> all();

    @SqlQuery("select * from describesHolidayMakers where idHolidayMaker=:idHolidayMaker")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesHolidayMakers> findById(@Bind("idHolidayMaker") int idHolidayMaker);

    @SqlQuery("select * from describesHolidayMakers where featureHName=:featureHName")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<DescribesHolidayMakers> findByFeatureName(@Bind("featureHName") String featureHName);

    void close();
}
