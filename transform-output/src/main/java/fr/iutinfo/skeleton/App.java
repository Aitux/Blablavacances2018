package fr.iutinfo.skeleton;

import com.opencsv.CSVReader;

import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.util.ArrayList;
import java.util.List;

public class App
{
    public static void main(String[] args) throws InvalidAlgorithmParameterException
    {

        if (args.length != 3)
            throw new InvalidAlgorithmParameterException("\nUsage: java transformOutput <affinity.csv> <interest.csv> <output.txt>");

        String inputAffinity = args[0];
        String inputInterest = args[1];
        String output = args[2];

        int capacity = 4;

        List<Integer> idList = new ArrayList<>();
        List<Integer> idTripList = new ArrayList<>();

        try
        {
            BufferedReader affinity = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputAffinity))));
            BufferedReader interest = new BufferedReader(new InputStreamReader(new FileInputStream(new File(inputInterest))));
            BufferedWriter out = new BufferedWriter(new FileWriter(output));
            StringBuilder builder = new StringBuilder();
            StringBuilder builder1 = new StringBuilder();

            String s;

            while ((s = interest.readLine()) != null)
                builder.append(s).append('\n');

            while ((s = affinity.readLine()) != null)
                builder1.append(s).append('\n');

            CSVReader reader = new CSVReader(new StringReader(builder.toString()));
            int tmp = -1;
            List<Integer> tmpTrip = new ArrayList<>();
            for (String[] x : reader.readAll())
            {
                if (Integer.parseInt(x[0]) != tmp)
                {
                    idList.add(Integer.parseInt(x[0]));
                    tmp = Integer.parseInt(x[0]);
                }
                if (!tmpTrip.contains(Integer.parseInt(x[1])))
                {
                    idTripList.add(Integer.parseInt(x[1]));
                    tmpTrip.add(Integer.parseInt(x[1]));
                }
            }
            out.write("m: " + idList.size() + "\n");
            out.write("n: " + idTripList.size() + "\n");
            out.write("activities: ");

            for (Integer integer : idTripList) out.write(" a" + integer + "(" + capacity + ")");
            out.write("\nindividuals: ");
            for (Integer integer : idList) out.write("i" + integer + " ");
            out.write("\n");
            out.flush();

            reader = new CSVReader(new StringReader(builder.toString()));
            for (String[] x : reader.readAll())
            {
                int src = Integer.parseInt(x[0]);
                int target = Integer.parseInt(x[1]);
                double score = (Double.parseDouble(x[2]) - 0.5) * 2;
                out.write("i" + src + ": a" + target + " " + score + "\n");
            }

            reader = new CSVReader(new StringReader(builder1.toString()));
            for (String[] x : reader.readAll())
            {
                int src = Integer.parseInt(x[0]);
                int target = Integer.parseInt(x[1]);
                double score = (Double.valueOf(x[2]) - 0.5) * 2;
                out.write("i" + src + ": a" + target + " " + score + "\n");
            }
            out.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
