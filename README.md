# PartEnS

## Quick start

To build and run from source:

```bash
$ mvn clean install
$ cd server
$ mvn jetty:run
```

**In the case** some tests would fail, you still can build and run using:

```bash
$ mvn clean install -DskipTests
$ cd server
$ mvn jetty:run
```
 
**To access a resource you need to use an HTTP request on** `http://localhost:8080/v1` **+ the required resource.**

## Resources
 
 What's coming next is true for:
 
 * Activites `/activite`
 * Destination `/destination`
 * Dispo `/dispo`
 * Duree `/duree`
 * Environnement `/environnement`
 * Expe `/expe`
 * Hebergement `/hebergement`
 * Interet `/interet`
 * Profession `/profession`
 * Transport `/transport`
 * TransportTo `/transportTo`
 
 
 Resources allows the users to interact with the RESTful service through method such as `GET`, `POST`, `PUT` and `DELETE`
 All those resource are made of 3 classes (*i.e.* x, xDao and xResource), they match with every category of the quiz.
 
 * Classes that aren't suffixed, are concrete representation of the database.
 * Classes ending with Dao enable the server to interact with the database.
 * Classes ending with Resource enable the user to interact with the server.
 
 **You can interact with those resources thanks to HTTP request.** 
 *All values are merely indicative.*
 
 For example with "Destination"
 
 * `GET` all destinations
```
 $ curl -X GET -i http://localhost:8080/v1/destination
```
 * `GET` destination by id
```
 $ curl -X GET -i http://localhost:8080/v1/destination/id/5
```
 * `GET` destination by name
```
  $ curl -X GET -i http://localhost:8080/v1/destination/libelle/PACA
 ```
 * `POST` to create a new destination (even though you shouldn't create new features since they are automatically generated from what's in the quiz)
```
 $ curl -X POST -i http://localhost:8080/v1/destination --data '{
   "libelle":"Corse"
   }'
```

## Preference Resources

What's coming next is true for:
 
 * PreferenceActivites `/preferenceActivite`
 * PreferenceDestination `/preferenceDestination`
 * PreferenceDispo `/preferenceDispo`
 * PreferenceDuree `/preferenceDuree`
 * PreferenceEnvironnement `/preferenceSelectEnv`
 * PreferenceExpe `/preferenceExpe`
 * PreferenceHebergement `/preferenceHebergement`
 * PreferenceInteret `/preferenceInteret`
 * PreferenceProfession `/preferenceProfession`
 * PreferenceTransport `/preferenceTransport`
 * PreferenceTransportTo `/preferenceTransportTo`

Classes starting with "preference" are just basic association of a resource, a trip, a user and a value (from 1 to 3) describing what the user think about that:
* 1 - The user is interested by this.
* 2 - The user doesn't really care.
* 3 - The user doesn't want this.

 **You can interact with those resources thanks to HTTP request.** 
 *All values are merely indicative.*
 
 For example with "PreferenceDestination"
  
 * `GET` all destination preferences
 ```
 $ curl -X GET -i http://localhost:8080/v1/preferenceDestination
 ```
 * `GET` destination preferences by user id
 
 ```
 $ curl -X GET -i http://localhost:8080/v1/preferenceDestination/user/1
 ```
 * `GET` destination preferences by trip id
 ```
  $ curl -X GET -i http://localhost:8080/v1/preferenceDestination/voyage/2
  ```
  
 * `GET` destination preferences by destination id (*cf.* Resources)
 ```
    $ curl -X GET -i http://localhost:8080/v1/preferenceDestination/destination/2
 ```
  
 * `POST` to create a new destination preference
 ```
    $ curl -X POST -i http://localhost:8080/v1/preferenceDestination --data '{
        "id_user":"2",
        "id_voyage":"5",
        "id":"1",
        "value":"2"
    }'
 ```

* `PUT` to update an existing destination preference
```
 $ curl -X PUT -i http://localhost:8080/v1/preferenceDestination/update/5 --data '{
        "value":"3"
    }'
```

## Trip Information

Trip information are contained into Voyage classes (Voyage, VoyageDao, VoyageResource).
 
**You can interact with those resources thanks to HTTP request.** 
*All values are merely indicative*


* `GET` all trips
```
 $ curl -X GET -i http://localhost:8080/v1/voyage
```
* `GET` by owner id (the user that created the project)
```
 $ curl -X GET -i http://localhost:8080/v1/voyage/3
```

* `GET` if you need to know if a user reached the maximum amount of project (2018/05: at the moment this limit is set to 1).
```
 $ curl -X GET -i http://localhost:8080/v1/voyage/limite/5
```
 * `GET` destination by trip id
```
  $ curl -X GET -i http://localhost:8080/v1/voyage/id/1
 ```
 * `POST` to create a new trip
```
 $ curl -X POST -i http://localhost:8080/v1/voyage --data '{
   "id_createur":"2",
    "budget": "1",
    "route_seul": "1",
    "nb_co_vac": "1,2,3",
    "sexe_co_vac": "2",
    "age_co_vac": "2,3",
    "etude_co_vac": "1,2,3",
    "fumeur_co_vac": "1",
    "animal_de_compagnie_co_vac": "3",
    "description": "My description"
   }'
```

* `PUT` to update an existing trip
```
$ curl -X PUT -i http://localhost:8080/v1/voyage/{trip id} --data '{
   "id_createur":"2",
   "budget": "1",
   "route_seul": "1",
   "nb_co_vac": "1,2,3",
   "sexe_co_vac": "2",
   "age_co_vac": "2,3",
   "etude_co_vac": "1,2,3",
   "fumeur_co_vac": "1",
   "animal_de_compagnie_co_vac": "3",
   "description": "My description"
   }'
```

* `DELETE` to delete an existing trip
```
$ curl -X DELETE -i http://localhost:8080/v1/voyage/{trip id}
```

Name | Description 
--- | --- 
**id_voyage** | The id of the trip.
**id_createur** | The id of the user that created the trip.
**budget** | An integer between 1 and 5. `1: <250€`, `2: 250-500€`, `3: 500-1000€`, `4: 1000-1500€`, `5: >1500€`
**route_seul** | A binary digit. `0: Can't travel alone`, `1: Can travel alone`
**nb_co_vac** | A string containing the number of other vacationers that the user want to travel with. `From 1 to 5`. Multiple values are possible. 
**description** | Describe the trip created by a user.
**sexe_co_vac** | `1: A women-only group`,`A mixed group`,`A men-only group` 
**age_co_vac** | A string containing the age of other vacationers that the user want to travel with. `1: 50-59yo`, `2: 60-69yo`, `3: 70-74yo`, `4: 75-79yo`, `5: 80+`. Multiple values are possible.
**etude_co_vac** | A string containing the level of study of other vacationers that the user want to travel with. `1: Primaire/Brevet/BEPC`, `2: BAC`,`3: BAC +1 à +3`,`4: BAC +4 ou plus`. Multiple values are possible.
**fumeur_co_vac** | An integer between 1 and 3. `1: The user agree to travel with a smoker`, `2: The user agree to travel with a smoker if this one, smoke outdoor`, `3: The user doesn't want to travel with a smoker`
**animal_de_compagnie_co_vac** | An integer between 1 and 3, indicating if the user wants to travel with a pet. `1: Yes`, `2: It is possible`, `3: No`

## User information
 
 User information are contained into User classes (User, UserDao, UserResource).
 
 **You can interact with those resources thanks to HTTP request.** 
 *All values are merely indicative*
 
 
 * `GET` all users
 ```
  $ curl -X GET -i http://localhost:8080/v1/user
 ```
 * `GET` user by name
 ```
  $ curl -X GET -i http://localhost:8080/v1/user/John
 ```
 
 * `GET` 
 ```
  $ curl -X GET -i http://localhost:8080/v1/voyage/limite/5
 ```
  * `GET` destination by trip id
 ```
   $ curl -X GET -i http://localhost:8080/v1/voyage/id/1
  ```
  * `POST` to create a new trip
 ```
  $ curl -X POST -i http://localhost:8080/v1/voyage --data '{
    "id_createur":"2",
     "budget": "1",
     "route_seul": "1",
     "nb_co_vac": "1,2,3",
     "sexe_co_vac": "2",
     "age_co_vac": "2,3",
     "etude_co_vac": "1,2,3",
     "fumeur_co_vac": "1",
     "animal_de_compagnie_co_vac": "3",
     "description": "My description"
    }'
 ```
 
 * `PUT` to update an existing trip
 ```
 $ curl -X PUT -i http://localhost:8080/v1/voyage/{trip id} --data '{
    "id_createur":"2",
    "budget": "1",
    "route_seul": "1",
    "nb_co_vac": "1,2,3",
    "sexe_co_vac": "2",
    "age_co_vac": "2,3",
    "etude_co_vac": "1,2,3",
    "fumeur_co_vac": "1",
    "animal_de_compagnie_co_vac": "3",
    "description": "My description"
    }'
 ```
 
 
 Name | Description 
 --- | --- 
 **id** | The id of the user.
 **name** | Its last name.
 **prenom** | Its first name.
 **alias** | Its alias.
 **email** | Its email.
 **adresse** | Its address.
 **anneenaissance** | Its birth year.
 **sexe** | A string containing its sex, `Homme`, `Femme`, `Autre`.
 **niveauEtude** | An integer from 1 to 4 `1: Primaire/Brevet/BEPC`, `2: BAC`,`3: BAC +1 à +3`,`4: BAC +4 ou plus`. Multiple values are possible.
 **profession** | A string containing the former profession of the user.
 
### Possible value for "profession"
 
 * Agriculture
 * Art - Culture - Audiovisuel
 * Artisanat d'art
 * BTP Architecture
 * Commerce
 * Défense publique - Sécurité
 * Droit - Sciences politiques
 * Economie - Gestion - Finance
 * Hôtellerie - restauration - tourisme
 * Industrie
 * Information - communication
 * Lettres - Langues - Sciences humaines
 * Maintenances - Entretien
 * Santé - Social - Soins esthétiques
 * Sciences - Informatiques
 * Sport
 * Transport
 * Enseignement
 * Administration - secrétariat
 * Sans profession

## UI
 
 At the moment each user can only create a single project at a time. So there is no need to have a detailed dashboard with a all the information about the trip.
 
 When the user log on, he can see if he already created a project thanks to a message displayed in place of the dashboard. He also can easily access to the main features, at the top of the screen in the nav bar.
 
 ![dashboard picture](images/main.png)
 
 When the user access to "Créer un projet de vacances", he can answer to the questions that are asked to create a new project.
 
 ![quiz picture](images/quiz.png)
 
## CSV
 
 The csv document that can be downloaded from `http://localhost:8080/v1/csv` is a semi-colon separated values.
 
 ![csv sample picture](images/csv.png)
 
 He is composed with the useful values of User, Voyage and all the preferences for each user.


If you need to get the exact name of a preference or more information about the user or the trip you can get all those information from the server by using HTTP request (*cf.* Resources, Preference Resources, Trip Information, User Information).
 

# Note

During my internship I had to update the computer that was lent by UVHC, and I created an account to access the super-user mode of the freshly installed Linux Shell (on Windows). 
You can find the required information to log in as super-user:

`password: root`

```
$ su -s
OR
$ sudo [command] 
```
